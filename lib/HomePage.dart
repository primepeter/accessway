import 'dart:io' as io;
import 'dart:async';
import 'dart:io';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/pages/LandingPage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:simple_icons/simple_icons.dart';

import 'CallPageTest.dart';
import 'CreatePost.dart';
import 'auth/WelcomePage.dart';
import 'basemodel.dart';
import 'main.dart';
import 'pages/NewsPage.dart';
import 'pages/ProfilePage.dart';
import 'pages/StatsPage.dart';
import 'sheets/FriendsSheet.dart';
import 'sheets/NotificationSheet.dart';
import 'sheets/ProfileSheet.dart';

const double kTopHeight = 90;
final offerReloadController = StreamController<bool>.broadcast();
final reloadController = StreamController<int>.broadcast();

bool showNotifyDot = false;

bool friendBusy = true;
List<BaseModel> friendList = [];
BaseModel callModel;
bool callBusy = true;
bool showCallScreen = false;

List<StreamSubscription> subs = [];

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentPage = 0;
  final pageController = PageController();
  bool isBusy = true;
  bool setUp = false;

  List get pageItems => [
        {"icon": "assets/tabs/home.png", "title": "Bookings"},
        {"icon": "assets/tabs/category.png", "title": "SC Safe"},
        {"icon": "assets/tabs/badge.png", "title": "Rewards"},
        {"icon": "assets/tabs/user.png", "title": "Profile"}
      ];

  @override
  initState() {
    super.initState();
    var reload = reloadController.stream.listen((p) {
      if (mounted) setState(() {});
    });

    subs.add(reload);
    Future.delayed(Duration(seconds: 1), () {
      loadSettings();
      //createUserListener();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var sub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
        if (banned.contains(userModel.getObjectId()) ||
            banned.contains(userModel.getString(DEVICE_ID)) ||
            banned.contains(userModel.getEmail())) {
          io.exit(0);
        }
        setState(() {});
      }
    });
    subs.add(sub);
  }

  void createUserListener() async {
    print("createUserListener");
    var sub = FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(userModel.getObjectId())
        .snapshots()
        .listen((shot) async {
      if (!shot.exists) {
        Future.delayed(Duration(milliseconds: 500), () {
          pushAndResult(context, WelcomePage(), clear: true, fade: true);
        });
        return;
      }

      if (shot != null) {
        userModel = BaseModel(doc: shot);
        isAdmin = userModel.getBoolean(IS_ADMIN);
        for (String s in userModel.getList(BLOCKED)) {
          if (!blockedIds.contains(s)) blockedIds.add(s);
        }
        if (!setUp) {
          //load start up code here.....
          loadFCM();
          loadFriends();
          loadCalls();
          setUp = true;
        }
        if (mounted) setState(() {});
      }
    });
    subs.add(sub);
  }

  loadFCM() async {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        print("onLaunch: ${message.data}");
        handleMessage(message.data);
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      print("onMessage: ${message.data}");
      showNotifyDot = true;
      setState(() {});
      showFCM(message);
      String type = message.data["type"];
      String offerId = message.data["offer_id"];
      String bidId = message.data["bid_id"];
      String status = message.data["status"];
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {});

    if (userModel.isAdminItem()) {
      FirebaseMessaging.instance.subscribeToTopic('admin');
    }
    await FirebaseMessaging.instance.subscribeToTopic('all');
    await FirebaseMessaging.instance.subscribeToTopic(userModel.getObjectId());
  }

  showFCM(RemoteMessage message) {
    RemoteNotification notification = message.notification;

    final iosInit = IOSNotificationDetails(sound: 'slow_spring_board.aiff');

    final androidInit = AndroidNotificationDetails(
      channel.id,
      channel.name,
      channel.description,
      // TODO add a proper drawable resource to android, for now using
      //      one that already exists in example app.
      icon: 'ic_notify',
    );

    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(android: androidInit, iOS: iosInit));
  }

  handleMessage(Map message) {
    if (null == message || message.isEmpty) return;
    String itemId = message[ITEM_ID];
    String itemDB = message[ITEM_DB];
  }

  loadFriends() async {
    var sub = FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(FRIEND_LIST, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((value) {
      for (var changes in value.docChanges) {
        final bm = BaseModel(doc: changes.doc);
        if (bm.myItem()) continue;

        if (changes.type == DocumentChangeType.removed) {
          friendList.removeWhere((e) => e.getObjectId() == bm.getObjectId());
          continue;
        }
        if (changes.type == DocumentChangeType.modified &&
            !bm.getList(FRIEND_LIST).contains(userModel.getObjectId())) {
          // if (!bm.getList(FRIEND_LIST).contains(userModel.getObjectId())) {
          //   friendList.removeWhere((e) => e.getObjectId() == bm.getObjectId());
          //   continue;
          // }
          friendList.removeWhere((e) => e.getObjectId() == bm.getObjectId());

          continue;
        }

        int p =
            friendList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
        if (p == -1) {
          friendList.add(bm);
        } else {
          friendList[p] = bm;
        }
      }
      friendBusy = false;
      setState(() {});
    });
    subs.add(sub);
  }

  loadCalls() async {
    var sub = FirebaseFirestore.instance
        .collection(CALL_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .limit(1)
        .snapshots()
        .listen((value) {
      if (null == value || value.size == 0) return;
      final call = BaseModel(doc: value.docs.first);
      // if (call.myItem()) {
      //   return;
      // }
      callModel = call;
      callBusy = false;
      showCallScreen = true;
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: black,
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 25),
            child: PageView(
              controller: pageController,
              physics: NeverScrollableScrollPhysics(),
              onPageChanged: (p) {
                currentPage = p;
                setState(() {});
              },
              children: [
                LandingPage(),
                NewsPage(),
                StatsPage(),
                ProfilePage(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Material(
                elevation: 5,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30)),
                child: Container(
                  // height: 120,
                  padding: EdgeInsets.fromLTRB(10, 40, 10, 10),
                  // alignment: Alignment.bottomCenter,
                  child: Row(children: [
                    TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                            shape: CircleBorder(), minimumSize: Size(40, 40)),
                        child: imageItem("assets/tabs/camera.png", 25, black)),
                    Expanded(
                      child: Container(
                        height: 40,
                        alignment: Alignment.center,
                        child: Text(
                          "AccessWay",
                          style: textStyle(
                            true,
                            20,
                            black,
                          ),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                            shape: CircleBorder(), minimumSize: Size(40, 40)),
                        child: imageItem("assets/tabs/send.png", 20, black))
                  ]),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 60,
              decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(color: blueColor.withOpacity(.1))),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: Row(children: [
                bottomTab(0),
                bottomTab(1),
                Container(
                  width: 100,
                ),
                bottomTab(2),
                bottomTab(3),
              ]),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.fromLTRB(10, 10, 10, 20),
              height: 80,
              child: GestureDetector(
                onLongPress: () {
                  if (!isAdmin) return;
                },
                onTap: () {
                  pushSheet(context, CreatePost());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Material(
                      // shape: RoundedRectangleBorder(
                      //     borderRadius: BorderRadius.circular(15),
                      //     side: BorderSide(color: white, width: 2)),

                      shape: CircleBorder(
                          side: BorderSide(
                              color: blueColor.withOpacity(.1), width: 2)),

                      elevation: 14,
                      color: blueColor,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: imageItem("assets/tabs/plus.png", 25, white),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Color get blueColor => Color(0xff023B7E);

  bottomTab(int p) {
    String title = pageItems[p]["title"];
    var icon = pageItems[p]["icon"];
    var icon1 = pageItems[p]["icon"];
    bool selected = currentPage == p;

    return Flexible(
      fit: FlexFit.tight,
      child: Center(
        child: GestureDetector(
          onTap: () {
            pageController.jumpToPage(
                p); //duration: Duration(milliseconds: 400),curve: Curves.ease);
          },
          child: AnimatedOpacity(
            duration: Duration(milliseconds: 400),
            opacity: 1, //selected?(1):(.4),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              decoration: BoxDecoration(),
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    imageItem(
                        selected ? icon1 : icon, selected ? 18 : 25, blueColor),
                    // Text(
                    //   title,
                    //   style: textStyle(false, selected ? 12 : 11,
                    //       white.withOpacity(selected ? 1 : (.4))),
                    //   maxLines: 1,
                    //   textAlign: TextAlign.center,
                    // ),
                    if (selected)
                      Container(
                        width: 20,
                        height: 4,
                        margin: EdgeInsets.only(top: 5),
                        decoration: BoxDecoration(
                            color: orange0,
                            borderRadius: BorderRadius.circular(10)
                            // shape: BoxShape.circle
                            ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

onShare(BuildContext context, String path, String text) async {
  final RenderBox box = context.findRenderObject() as RenderBox;

  if ((null != path && path.isNotEmpty) && (null != text && text.isNotEmpty)) {
    final newFile = await createFileFromAsset(path, "shareMe.png");
    await Share.shareFiles([newFile.path],
        text: text,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    return;
  }

  await Share.share(text,
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
}

Future<File> createFileFromAsset(String path, String name) async {
  final ByteData data = await rootBundle.load(path);
  Directory tempDir = await getTemporaryDirectory();
  File tempFile = File('${tempDir.path}/$name');
  await tempFile.writeAsBytes(data.buffer.asUint8List(), flush: true);
  return tempFile;
}
