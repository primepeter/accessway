

import 'package:Loopin/AppEngine.dart';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class ContactUs extends StatefulWidget {
   @override
   _ContactUstate createState() => _ContactUstate();
 }

 class _ContactUstate extends State<ContactUs> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

   @override
   Widget build(BuildContext context) {
     return SafeArea(
       child: Scaffold(
         body:page(),backgroundColor: transparent,
       ),
     );
   }

  ScrollController mainScroll=ScrollController();
  page() {

    return pageChild();
  }

   pageChild(){
     return Column(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: <Widget>[
         addSpace(10),
         Row(
           children: <Widget>[
             InkWell(
                 onTap: () {
                   Navigator.pop(context);
                 },
                 child: Container(
                   width: 50,
                   height: 50,
                   child: Center(
                       child: Icon(
                         Icons.keyboard_backspace,
                         color: getBlackColor(),
                         size: 30,
                       )),
                 )),
//             Flexible(flex:1,fit:FlexFit.tight,child: Text("Contact Us",style: textStyle(true, 25, getBlackColor()),)),
             addSpaceWidth(20),
           ],
         ),
         addSpace(10),

         Expanded(child:
                     Padding(
                       padding: const EdgeInsets.all(20.0),
                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
                         mainAxisSize: MainAxisSize.min,
                         children: [
                           Image.asset(ic_plain,width: 60,height:60,),
                           Text("Do you need help?",
                               style: textStyle(true, 25, getBlackColor()),textAlign: TextAlign.center,),
                           GestureDetector(
                               child: Text("Our team is ready to assist you",textAlign: TextAlign.center,
                               style: textStyle(true, 13, getBlackColor().withOpacity(.5)))),
                           addSpace(20),
                           GestureDetector(
                             onTap: (){
                               // clickChatSupport(context);
                             },
                             child: Card(
//                             margin: EdgeInsets.all(10),
                               elevation: 5,clipBehavior: Clip.antiAlias,shadowColor: getBlackColor().withOpacity(.1),
                               color: getWhiteColor(),
                               shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.all(Radius.circular(5)),
                                   side: BorderSide(color: getBlackColor().withOpacity(.1),width: .5)),
                               child:Padding(
                                 padding: const EdgeInsets.all(15),
                                 child: Column(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     Row(
                                       children: [
                                         Flexible(fit: FlexFit.tight,child: Text("Chat us",style: textStyle(false, 25, getBlackColor()),)),
                                         addSpaceWidth(10),
                                         Icon(Icons.chat)
                                       ],
                                     ),

                                   ],
                                 ),
                               ),
                             ),
                           ),
                           addSpace(10),
                           GestureDetector(
                             onTap: (){
                               String email = appSettingsModel.getString(SUPPORT_EMAIL);
                               if(email.isEmpty){
                                 showErrorDialog(context, "Email temporarily unavailable, chat with us instead");
                                 return;
                               }
                               sendEmail(email,subject: "Need Assistance",body: "Hi Team, i will be glad if you can assist me");
                             },
                             child: Card(
//                             margin: EdgeInsets.all(10),
                               elevation: 5,clipBehavior: Clip.antiAlias,shadowColor: getBlackColor().withOpacity(.1),
                               color: getWhiteColor(),
                               shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.all(Radius.circular(5)),
                                   side: BorderSide(color: getBlackColor().withOpacity(.1),width: .5)),
                               child:Padding(
                                 padding: const EdgeInsets.all(15),
                                 child: Column(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     Row(
                                       children: [
                                         Flexible(fit: FlexFit.tight,child: Column(
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Text("Email us",style: textStyle(false, 25, getBlackColor()),),
                                             Text(appSettingsModel.getString(SUPPORT_EMAIL),style: textStyle(false, 12, getBlackColor().withOpacity(.5)),),
                                           ],
                                         )),
                                         addSpaceWidth(10),
                                         Icon(Icons.email)
                                       ],
                                     ),

                                   ],
                                 ),
                               ),
                             ),
                           ),
                           addSpace(10),
                           if(appSettingsModel.getString(SUPPORT_PHONE).isNotEmpty)GestureDetector(
                             onTap: (){
                               String phone = appSettingsModel.getString(SUPPORT_PHONE);
                               if(phone.isEmpty){
                                 showErrorDialog(context, "Phone temporarily unavailable, chat with us instead");
                                 return;
                               }
                               placeCall(phone);
                             },
                             child: Card(
//                             margin: EdgeInsets.all(10),
                               elevation: 5,clipBehavior: Clip.antiAlias,shadowColor: getBlackColor().withOpacity(.1),
                               color: getWhiteColor(),
                               shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.all(Radius.circular(5)),
                                   side: BorderSide(color: getBlackColor().withOpacity(.1),width: 1)),
                               child:Padding(
                                 padding: const EdgeInsets.all(15),
                                 child: Column(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     Row(
                                       children: [
                                         Flexible(fit: FlexFit.tight,child: Column(
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Text("Call us",style: textStyle(false, 25, getBlackColor()),),
                                             Text(appSettingsModel.getString(SUPPORT_PHONE),style: textStyle(false, 12, getBlackColor().withOpacity(.5)),),
                                           ],
                                         )),
                                         addSpaceWidth(10),
                                         Icon(Icons.call)
                                       ],
                                     ),

                                   ],
                                 ),
                               ),
                             ),
                           ),
                           addSpace(10),

                        ],
                       ),
                     ))
       ],
     );
   }
 }
