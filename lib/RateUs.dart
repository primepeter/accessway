
 import 'dart:ui';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';

 class RateUs extends StatelessWidget {
   BuildContext con;
   @override
   Widget build(BuildContext context) {
     con = context;
     return WillPopScope(
       onWillPop: () {

       },
       child: Stack(
         fit: StackFit.expand,
         children: <Widget>[
           ClipRect(
               child:BackdropFilter(
                   filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                   child: Container(
                     color: black.withOpacity(.7),
                   ))
           ),
           Container(
             padding: EdgeInsets.all(15),
             child: Center(
               child: Column(
                 mainAxisSize: MainAxisSize.min,
                 children: <Widget>[
                   GestureDetector(
                     onTap: () {
                       if (isAdmin) {
                         Navigator.pop(con);
                       }
                     },
                     child: Image.asset(ic_plain,width: 50,height: 50,),
                   ),
                   addSpace(10),
                   Text(
                     "Enjoying Loopin Material?",
                     style: textStyle(true, 22, white),
                     textAlign: TextAlign.center,
                   ),
                   addSpace(10),
                   Text(
                     "Please support us with 5 stars",
                     style: textStyle(false, 16, white.withOpacity(.5)),
                     textAlign: TextAlign.center,
                   ),
                   addSpace(15),
                   Container(
                     height: 40,//width: double.infinity,
                     child: RaisedButton(
                         materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(25)),
                         color: white,
                         onPressed: () {
                           Navigator.pop(context);
                           rateApp();
                         },
                         child: Text(
                           "Rate Us",
                           style: textStyle(true, 14, dark_green0),
                         )),
                   ),
                   addSpace(15),
                   Container(
                     height: 35,width: 66,
                     child: FlatButton(
                         materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(25)),
                         color: red0,
                         onPressed: () {
                           Navigator.pop(con);
                         },
                         padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                         child: Text(
                           "Later",
                           style: textStyle(true, 14, white),
                         )),
                   )

                 ],
               ),
             ),
           ),
         ],
       ),
     );
   }
 }