import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:flutter/services.dart';

import 'dart:async';

import 'package:flutter/widgets.dart';

import 'HomePage.dart';
import 'auth/WelcomePage.dart';

bool webApp = false;

AndroidNotificationChannel channel;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
var publicKey = '[YOUR_PAYSTACK_PUBLIC_KEY]';
final plugin = PaystackPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await initializeNotifications();
  await initializeSettings();
  Widget pageRoute = WelcomePage();
  final authenticated = await initializeUser();
  if (authenticated) pageRoute = HomePage();
  runApp(MyApp(pageRoute));
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  //   systemNavigationBarColor: Colors.black, // navigation bar color
  //   statusBarColor: Colors.black, // status bar color
  //   statusBarBrightness: Brightness.light, //status bar brigtness
  //   statusBarIconBrightness: Brightness.light, //status barIcon Brightness
  //   systemNavigationBarDividerColor:
  //       Colors.black, //Navigation bar divider color
  //   systemNavigationBarIconBrightness: Brightness.light, //navigation bar icon
  // ));
}

Future<bool> initializeUser() async {
  bool value = false;
  final user = FirebaseAuth.instance.currentUser;
  if (null != user) {
    final doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(user.uid)
        .get();
    if (null != doc && doc.exists) {
      value = true;
      userModel = BaseModel(doc: doc);
    }
  }
  return value;
}

initializeSettings() async {
  final doc = await FirebaseFirestore.instance
      .collection(APP_SETTINGS_BASE)
      .doc(APP_SETTINGS)
      .get();
  if (null == doc || !doc.exists) throw null;

  if (!doc.exists) {
    appSettingsModel = new BaseModel();
    appSettingsModel.saveItem(APP_SETTINGS_BASE, false, document: APP_SETTINGS);
    return;
  }
  appSettingsModel = BaseModel(doc: doc);
}

initializeNotifications() async {
  FirebaseMessaging.onBackgroundMessage(_fcmBackground);
  channel = const AndroidNotificationChannel(
      '.high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.high,
      sound: RawResourceAndroidNotificationSound("solemn"),
      enableLights: true);
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  const AndroidInitializationSettings androidInit =
      AndroidInitializationSettings('ic_notify');

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later
  final IOSInitializationSettings iosInit = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification: (
        int id,
        String title,
        String body,
        String payload,
      ) async {});
  const MacOSInitializationSettings macInit = MacOSInitializationSettings(
    requestAlertPermission: false,
    requestBadgePermission: false,
    requestSoundPermission: false,
  );

  final InitializationSettings initializationSettings = InitializationSettings(
    android: androidInit,
    iOS: iosInit,
    macOS: macInit,
  );
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  });

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
}

Future<void> _fcmBackground(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

class MyApp extends StatelessWidget {
  final Widget page;

  const MyApp(this.page);

  @override
  Widget build(BuildContext c) {
    return Material(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "AccessWay",
        color: white,
        theme: ThemeData(
            fontFamily: "Gilroy",
            // fontFamily: GoogleFonts.nunito().fontFamily,
            primaryColor: appColor,
            textSelectionTheme: TextSelectionThemeData(cursorColor: appColor),
            colorScheme:
                ColorScheme.fromSwatch().copyWith(secondary: appColor)),
        home: page,
      ),
    );
  }

  PageTransitionsBuilder createTransition() {
    return ZoomPageTransitionsBuilder();
  }
}
