import 'package:Loopin/basemodel.dart';
import 'package:shake_widget/controller.dart';
import 'package:shake_widget/shake_widget.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/NumericKeypad.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';

class VerificationSheet extends StatefulWidget {
  final String email, password;
  final bool passReset;
  const VerificationSheet(this.email, this.password, {this.passReset = false});

  @override
  _VerificationSheetState createState() => _VerificationSheetState();
}

class _VerificationSheetState extends State<VerificationSheet> {
  final otpController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: getScreenHeight(context) * .15),
      padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
      decoration: BoxDecoration(
        color: appColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          addSpace(20),
          Center(
            child: Container(
              width: 40,
              height: 5,
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          addSpace(20),
          Text(
            "Verification Code",
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: white),
          ),
          Container(
            margin: EdgeInsets.only(left: 40, right: 40),
            child: Text(
              "Verify your account by entering the 6-digit code sent to your Number or Email.",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 13,
                  color: white.withOpacity(.7)),
            ),
          ),
          addSpace(10),
          ShakeWidget(
            shakeController: shakeController,
            child: Text(
              otpText.isEmpty ? "_ _ _ _ _ _" : otpText,
              style: textStyle(true, 30, white),
            ),
          ),
          NumericKeyboard(
              onKeyboardTap: _onKeyboardTap,
              textColor: Colors.red,
              rightButtonFn: () {
                if (otpText.isEmpty) return;
                otpText = otpText.substring(0, otpText.length - 1);
                setState(() {});
              },
              mainAxisAlignment: MainAxisAlignment.spaceEvenly),
          addSpace(20),
          // if (text.length == 4)
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            child: otpText.length < 6
                ? null
                : TextButton(
                    onPressed: () async {
                      if (otpText.isEmpty) {
                        shakeController.shake();
                        return;
                      }

                      // showProgress(true, context, msg: "Verifying...");
                    },
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.all(15),
                        elevation: 10,
                        // backgroundColor: black,
                        backgroundColor: Color(0XFF0B123E),
                        primary: white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30))),
                    child: Text(
                      "VERIFY",
                      style: textStyle(true, 20, white),
                    )),
          ),
          addSpace(10),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: TextButton.styleFrom(
                  padding: EdgeInsets.all(10),
                  elevation: 10,
                  backgroundColor: red0,
                  primary: white,
                  shape: CircleBorder()),
              child: Icon(
                Icons.close,
                size: 16,
              )),
          if (false)
            Container(
              // height: 60,
              alignment: Alignment.center,
              child: TextButton(
                onPressed: () {},
                child: Text(
                  "Didn't get OTP? Resend Now",
                  style: TextStyle(fontSize: 12),
                ),
                style: TextButton.styleFrom(
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    // padding: EdgeInsets.all(5),
                    primary: white,
                    backgroundColor: transparent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
              ),
            ),
        ],
      ),
    );
  }

  String otpText = "";
  final shakeController = ShakeController();

  _onKeyboardTap(String value) {
    if (otpText.length == 6) return;
    otpText = otpText + value;
    setState(() {});
  }
}
