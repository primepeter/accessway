import 'package:Loopin/HomePage.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

import 'VerificationSheet.dart';

class ResetPasswordSheet extends StatefulWidget {
  @override
  _ResetPasswordSheetState createState() => _ResetPasswordSheetState();
}

class _ResetPasswordSheetState extends State<ResetPasswordSheet> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final rePassController = TextEditingController();

  bool passVisible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // focusNode.addListener(() {
    //   setState(() {});
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding:
              const EdgeInsets.only(top: 0, right: 20, left: 20, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Reset Password",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: black),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      inputField("Email Address", emailController),
                      inputField("New Password", passController,
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }),
                      inputField("Retype Password", rePassController,
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }),
                      addSpace(10),
                      Container(
                        height: 60,
                        child: TextButton(
                          onPressed: () async {
                            String email = emailController.text;
                            String password = passController.text;
                            String rePassword = rePassController.text;

                            if (email.isEmpty) {
                              showError(context, "Enter Email Address!");
                              return;
                            }
                            if (password.isEmpty) {
                              showError(context, "Enter Password!");
                              return;
                            }

                            if (rePassword.isEmpty) {
                              showError(context, "Enter Retype password!");
                              return;
                            }

                            if (password != rePassword) {
                              showError(context, "Password's don't match!");
                              return;
                            }
                          },
                          child: Center(
                              child: Text(
                            "Update Password",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
