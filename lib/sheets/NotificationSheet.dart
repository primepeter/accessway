import 'package:Loopin/HomePage.dart';
import 'package:Loopin/widgets/shimmerListLoader.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

class NotificationSheet extends StatefulWidget {
  @override
  _NotificationSheetState createState() => _NotificationSheetState();
}

class _NotificationSheetState extends State<NotificationSheet> {
  final refreshController = RefreshController();
  // List<Notifications> notifyList = LucroCore.instance.allNotification;
  // bool isNotifyBusy = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showNotifyDot = false;
    friendBusy = friendList.isEmpty;
    reloadController.add(100);
  }

  updateNotification(int p) {
    final notify = friendList[p];
  }

  int parseInt(String ts) => DateTime.parse(ts).millisecondsSinceEpoch;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding:
              const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Center(
                child: Row(
                  children: [
                    Text(
                      "Friends",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                          color: black),
                    ),
                    Spacer(),
                    // TextButton(
                    //     onPressed: () {},
                    //     style: TextButton.styleFrom(
                    //         primary: black,
                    //         shape: CircleBorder(),
                    //         minimumSize: Size(20, 20)),
                    //     child: Icon(Icons.filter_list))
                  ],
                ),
              ),
              Expanded(
                child: Builder(
                  builder: (c) {
                    if (friendBusy)
                      return shimmerListLoader(isNotification: true);

                    if (friendList.isEmpty)
                      return Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(bottom: 80),
                        child: emptyLayout(
                            "assets/icons/notify.png",
                            "No Notifications",
                            "You have not received any notifications yet",
                            height: 250,
                            bottomMargin: 0,
                            iconColor: null),
                      );

                    return SmartRefresher(
                      controller: refreshController,
                      enablePullUp: true,
                      enablePullDown: true,
                      onRefresh: () async {
                        Future.delayed(Duration(seconds: 1), () {
                          refreshController.refreshCompleted();
                        });
                      },
                      onLoading: () async {
                        Future.delayed(Duration(seconds: 1), () {
                          refreshController.loadComplete();
                        });
                      },
                      child: ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: friendList.length,
                          itemBuilder: (c, p) {
                            final notify = friendList[p];
                            // String timestamp = parseTimestamp(notify.timestamp);
                            bool unread = false;
                            // notify.status.toLowerCase() == "unread";

                            return GestureDetector(
                              onTap: () {
                                updateNotification(p);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: unread
                                        ? appColor.withOpacity(.05)
                                        : white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: black.withOpacity(.05),
                                          blurRadius: 5,
                                          spreadRadius: 2)
                                    ],
                                    borderRadius: BorderRadius.circular(8)),
                                padding: EdgeInsets.all(20),
                                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          child: Container(
                                            height: 30,
                                            width: 30,
                                            color: white,
                                            alignment: Alignment.center,
                                            child: Image.asset(
                                              "assets/icons/lucro_small.png",
                                              height: 30,
                                              width: 30,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    addSpaceWidth(10),
                                    Expanded(
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "notify.message",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            addSpace(5),
                                            Text(
                                              "timestamp",
                                              style: TextStyle(
                                                fontSize: 12,
                                                // fontWeight: FontWeight.bold
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                            // return offerItem(notifications[p]);
                          }),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
