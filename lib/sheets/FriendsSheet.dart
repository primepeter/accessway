import 'package:Loopin/HomePage.dart';
import 'package:Loopin/notificationService.dart';
import 'package:cached_network_image/cached_network_image.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

import 'FriendOptionSheet.dart';

class FriendsSheet extends StatefulWidget {
  @override
  _FriendsSheetState createState() => _FriendsSheetState();
}

class _FriendsSheetState extends State<FriendsSheet> {
  int avatarIndex = userModel.getInt(AVATAR_INDEX);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double size = 80;
    double sizeAlt = 25;

    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 10),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Friends List",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: black),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    // padding: EdgeInsets.all(20),
                    // margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    alignment: friendList.length < 3
                        ? Alignment.topLeft
                        : Alignment.center,
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.start,
                      children: List.generate(friendList.length,
                          // 10,
                          (p) {
                        final bm = friendList[p];
                        int avatarIndex = bm.getInt(AVATAR_INDEX);
                        final friendStatus = userModel.getMap(FRIEND_STATUS);
                        int status = friendStatus[bm.getObjectId()] ?? 0;

                        bool active = p == avatarIndex;

                        String name = bm.getName();

                        return GestureDetector(
                          onTap: () {
                            if (status != INCOMING) return;

                            String myName = userModel.getName();

                            yesNoDialog(context, "Accept Request?",
                                "Are you sure you want to accept request from $name as a friend?",
                                () async {
                              NotificationService.sendPush(
                                  topic: bm.getObjectId(),
                                  title: "Request Accepted!",
                                  body:
                                      "$myName has accepted your friend request!");

                              Map status = bm.getMap(FRIEND_STATUS);
                              status[userModel.getObjectId()] = APPROVED;
                              bm
                                ..putInList(
                                    FRIEND_LIST, userModel.getObjectId())
                                ..put(FRIEND_STATUS, status)
                                ..updateItems();

                              Map status1 = userModel.getMap(FRIEND_STATUS);
                              status1[bm.getObjectId()] = APPROVED;
                              userModel
                                ..putInList(FRIEND_LIST, bm.getObjectId())
                                ..put(FRIEND_STATUS, status1)
                                ..updateItems();

                              setState(() {});

                              showMessage(
                                context,
                                Icons.check,
                                green,
                                "Request Accepted!",
                                "You are now friends with $name!",
                              );
                            }, delayInMilli: 0);
                          },
                          child: Container(
                            width: size,
                            height: size + 20,
                            margin: EdgeInsets.all(10),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: size,
                                  width: size,
                                  child: Stack(
                                    children: [
                                      Container(
                                        height: size,
                                        width: size,
                                        alignment: Alignment.center,
                                        // child: Image.asset(
                                        //     "assets/avatars/${avatarIndex + 1}.png"),

                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          child: Builder(
                                            builder: (c) {
                                              String imageUrl =
                                                  bm.getString(IMAGE_URL);
                                              bool online =
                                                  imageUrl.contains("http");
                                              bool empty = imageUrl.isEmpty;

                                              if (avatarIndex != 100)
                                                return Image.asset(
                                                    "assets/avatars/${avatarIndex + 1}.png");

                                              if (!empty & !online)
                                                return Image.network(
                                                  imageUrl,
                                                  height: size,
                                                  width: size,
                                                  fit: BoxFit.cover,
                                                );

                                              if (!empty & online)
                                                return CachedNetworkImage(
                                                  imageUrl: imageUrl,
                                                  height: size,
                                                  width: size,
                                                  fit: BoxFit.cover,
                                                );

                                              return Icon(
                                                Icons.person,
                                                size: 30,
                                                color: appColor.withOpacity(.2),
                                              );
                                            },
                                          ),
                                        ),

                                        decoration: BoxDecoration(
                                            color: white.withOpacity(.2),
                                            boxShadow: [
                                              BoxShadow(
                                                color: black.withOpacity(.1),
                                                spreadRadius: 2,
                                                blurRadius: 5,
                                              )
                                            ],
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: white, width: 3)),
                                      ),
                                      // if (status < 1)
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Container(
                                          height: sizeAlt,
                                          width: sizeAlt,
                                          alignment: Alignment.center,
                                          child: Icon(
                                            status == OUTGOING
                                                ? Icons.arrow_upward
                                                : status == INCOMING
                                                    ? Icons.arrow_downward
                                                    : Icons.check,
                                            size: 14,
                                            color: white,
                                          ),
                                          decoration: BoxDecoration(
                                              color: green_dark,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: black.withOpacity(.1),
                                                  spreadRadius: 2,
                                                  blurRadius: 5,
                                                )
                                              ],
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: white, width: 3)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Text(
                                  name,
                                  maxLines: 1,
                                )
                              ],
                            ),
                          ),
                        );
                      })
                        ..insert(
                            0,
                            GestureDetector(
                              onTap: () async {
                                pushSheet(context, FriendOptionSheet());
                              },
                              child: Container(
                                width: size,
                                height: size + 22,
                                margin: EdgeInsets.all(10),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: size,
                                      width: size,
                                      //margin: EdgeInsets.all(10),
                                      child: Stack(
                                        children: [
                                          Container(
                                            height: size,
                                            width: size,
                                            alignment: Alignment.center,
                                            child: Icon(
                                              Icons.person,
                                              size: 30,
                                              color: appColor.withOpacity(.2),
                                            ),
                                            decoration: BoxDecoration(
                                                color: white.withOpacity(.2),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color:
                                                        black.withOpacity(.1),
                                                    spreadRadius: 2,
                                                    blurRadius: 5,
                                                  )
                                                ],
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                    color: white, width: 3)),
                                          ),
                                          Align(
                                            alignment: Alignment.bottomRight,
                                            child: Container(
                                              height: sizeAlt,
                                              width: sizeAlt,
                                              alignment: Alignment.center,
                                              child: Icon(
                                                Icons.add,
                                                size: 14,
                                                color: white,
                                              ),
                                              decoration: BoxDecoration(
                                                  color: appColor,
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color:
                                                          black.withOpacity(.1),
                                                      spreadRadius: 2,
                                                      blurRadius: 5,
                                                    )
                                                  ],
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                      color: white, width: 3)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    addSpace(2),
                                    Text(
                                      "Add",
                                      maxLines: 1,
                                    )
                                  ],
                                ),
                              ),
                            )),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
