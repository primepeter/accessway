import 'package:Loopin/HomePage.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/notificationService.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

class AddFriendIdSheet extends StatefulWidget {
  @override
  _AddFriendIdSheetState createState() => _AddFriendIdSheetState();
}

class _AddFriendIdSheetState extends State<AddFriendIdSheet> {
  final idController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding:
              const EdgeInsets.only(top: 0, right: 20, left: 20, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Add Friend by ID",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: black),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      inputField("Enter Friend's ID", idController, isNum: true,
                          onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      Container(
                        height: 60,
                        child: TextButton(
                          onPressed: () {
                            String id = idController.text;

                            if (id.isEmpty) {
                              showError(
                                  context, "Enter your friends serfix id!");
                              return;
                            }

                            FocusScope.of(context).unfocus();
                            showProgress(true, context, msg: "Searching...");
                            try {
                              FirebaseFirestore.instance
                                  .collection(USER_BASE)
                                  .where(PLAY_GATE_ID, isEqualTo: id)
                                  .limit(1)
                                  .get()
                                  .then((value) async {
                                await Future.delayed(
                                    Duration(milliseconds: 800));
                                showProgress(false, context);

                                if (value.size == 0) {
                                  showErrorDialog(context,
                                      "No user found with this Playgate Id");
                                  return;
                                }

                                final bm = BaseModel(doc: value.docs.first);

                                if (bm.myItem()) {
                                  showErrorDialog(
                                    context,
                                    "You cannot send a request to yourself.",
                                  );
                                  return;
                                }

                                String name = bm.getName();

                                if (userModel
                                    .getList(FRIEND_LIST)
                                    .contains(bm.getObjectId())) {
                                  Map statusMap = bm.getMap(FRIEND_STATUS);
                                  int status =
                                      statusMap[userModel.getObjectId()] ?? 0;
                                  //= PENDING;

                                  showErrorDialog(
                                    context,
                                    status == PENDING
                                        ? "You have a pending friend request with $name already."
                                        : "Your already friends with $name",
                                  );
                                  return;
                                }

                                String myName = userModel.getName();

                                yesNoDialog(context, "Add User?",
                                    "Are you sure you want to add $name as a friend?",
                                    () async {
                                  NotificationService.sendPush(
                                      topic: bm.getObjectId(),
                                      title: "Friend Request!",
                                      body:
                                          "$myName sent you a friend request!");

                                  Map status = bm.getMap(FRIEND_STATUS);
                                  status[userModel.getObjectId()] = INCOMING;
                                  bm
                                    ..putInList(
                                        FRIEND_LIST, userModel.getObjectId())
                                    ..put(FRIEND_STATUS, status)
                                    ..updateItems();

                                  Map status1 = userModel.getMap(FRIEND_STATUS);
                                  status1[bm.getObjectId()] = OUTGOING;
                                  userModel
                                    ..putInList(FRIEND_LIST, bm.getObjectId())
                                    ..put(FRIEND_STATUS, status1)
                                    ..updateItems();

                                  setState(() {});

                                  showMessage(
                                    context,
                                    Icons.check,
                                    green,
                                    "Request Sent!",
                                    "Your friend request was sent successfully!",
                                  );
                                }, delayInMilli: 1200);
                              });
                            } catch (e) {
                              showProgress(false, context);

                              showErrorDialog(context, e.toString());
                            }
                          },
                          child: Center(
                              child: Text(
                            "Find Friend",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                      ),
                      addSpace(20),
                      Image.asset(
                        "assets/icons/page2.png",
                        // height: 80,
                        // width: 80,
                        scale: 2,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
