import 'dart:async';
import 'dart:io';

import 'package:Loopin/HomePage.dart';
import 'package:Loopin/auth/WelcomePage.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/sheets/AddFriendIdSheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

import 'UpdatePhotoSheet.dart';
import 'UpdateProfileSheet.dart';
import 'UpdatePasswordSheet.dart';

class FriendOptionSheet extends StatefulWidget {
  @override
  _FriendOptionSheetState createState() => _FriendOptionSheetState();
}

class _FriendOptionSheetState extends State<FriendOptionSheet> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: getScreenHeight(context) * .55),
          padding:
              // const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
              const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Text(
                    "Add a Friend",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                        color: black),
                  ),
                ),
              ),

              addSpace(10),
              Text(
                "My PlayGate ID: ${userModel.getString(PLAY_GATE_ID)}",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              addSpace(20),
              Container(
                height: 60,
                child: TextButton(
                  onPressed: () async {},
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          "SHARE MY PLAYGATE ID",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Icon(Icons.share)
                    ],
                  ),
                  style: TextButton.styleFrom(
                      primary: white,
                      backgroundColor: btnPickColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              addSpace(10),
              Container(
                height: 60,
                child: TextButton(
                  onPressed: () async {
                    pushSheet(context, AddFriendIdSheet());
                  },
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          "ADD FRIEND BY ID",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Icon(Icons.add)
                    ],
                  ),
                  style: TextButton.styleFrom(
                      primary: white,
                      backgroundColor: appColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              addSpace(40),
            ],
          ),
        ),
      ],
    );
  }

  settingsItem(
    icon,
    String title,
    String desc,
    onClick,
  ) {
    return GestureDetector(
      onTap: () {
        onClick();
      },
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: white,
            ),
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 30,
                  width: 30,
                  child: Icon(
                    icon,
                    size: 16,
                    color: white,
                  ),
                  decoration: BoxDecoration(
                      color: appColor,
                      // color: Color(0XFF0B123E),
                      // color: Color(0XFF33CA7F).withOpacity(.2),
                      borderRadius: BorderRadius.circular(10)),
                ),
                addSpaceWidth(10),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    // if (desc.isNotEmpty)
                    Text(
                      desc,
                      style: TextStyle(
                        fontSize: 14,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                )),
                addSpaceWidth(10),
                Icon(
                  Icons.navigate_next,
                  size: 25,
                  color: Color(0XFF0B123E),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.only(top: 35, left: 20),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(10),
                ),
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: BoxDecoration(
                      border: Border(
                          // top: BorderSide(
                          //     color: Color(0XFF0B123E).withOpacity(.5),
                          //     width: 5),
                          left: BorderSide(
                              // color: Color(0XFF0B123E).withOpacity(.5),
                              color: appColor.withOpacity(.5),
                              width: 5),
                          bottom: BorderSide(
                              // color: Color(0XFF0B123E).withOpacity(.5),
                              color: appColor.withOpacity(.5),
                              width: 5))),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
