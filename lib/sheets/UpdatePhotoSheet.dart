import 'package:Loopin/HomePage.dart';
import 'package:cached_network_image/cached_network_image.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';
import 'package:images_picker/images_picker.dart';

class UpdatePhotoSheet extends StatefulWidget {
  @override
  _UpdatePhotoSheetState createState() => _UpdatePhotoSheetState();
}

class _UpdatePhotoSheetState extends State<UpdatePhotoSheet> {
  int avatarIndex = userModel.getInt(AVATAR_INDEX);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Photo Update",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: black),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Center(
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: List.generate(12, (p) {
                        bool active = p == avatarIndex;
                        return GestureDetector(
                          onTap: () {
                            avatarIndex = p;
                            userModel
                              ..put(AVATAR_INDEX, p)
                              ..updateItems();
                            setState(() {});
                            reloadController.add(100);
                          },
                          child: Container(
                            height: 100,
                            width: 100,
                            margin: EdgeInsets.all(10),
                            child: Stack(
                              children: [
                                Container(
                                  height: 100,
                                  width: 100,
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                      "assets/avatars/${p + 1}.png"),
                                  decoration: BoxDecoration(
                                      color: white.withOpacity(.2),
                                      boxShadow: [
                                        BoxShadow(
                                          color: black.withOpacity(.1),
                                          spreadRadius: 2,
                                          blurRadius: 5,
                                        )
                                      ],
                                      shape: BoxShape.circle,
                                      border:
                                          Border.all(color: white, width: 3)),
                                ),
                                if (active)
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                      height: 35,
                                      width: 35,
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.check,
                                        size: 16,
                                        color: white,
                                      ),
                                      decoration: BoxDecoration(
                                          color: green_dark,
                                          boxShadow: [
                                            BoxShadow(
                                              color: black.withOpacity(.1),
                                              spreadRadius: 2,
                                              blurRadius: 5,
                                            )
                                          ],
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: white, width: 3)),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        );
                      })
                        ..insert(
                            0,
                            GestureDetector(
                              onTap: () async {
                                // final pick = await ImagePicker()
                                //     .getImage(source: ImageSource.gallery);

                                final pick = await ImagesPicker.pick(
                                    cropOpt:
                                        CropOption(cropType: CropType.circle));

                                if (null == pick) return;
                                // final crop = await ImageCropper.cropImage(
                                //     sourcePath: pick.path,
                                //     cropStyle: CropStyle.circle);
                                // if (null == crop) return;
                                // final imageUrl = await uploadFileTask(crop);

                                final imageUrl =
                                    await uploadFileTask(pick.first.path);
                                avatarIndex = 100;
                                userModel
                                  ..put(AVATAR_INDEX, 100)
                                  ..put(IMAGE_URL, imageUrl)
                                  ..updateItems();

                                setState(() {});
                                reloadController.add(100);
                              },
                              child: Container(
                                height: 100,
                                width: 100,
                                margin: EdgeInsets.all(10),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: 100,
                                      width: 100,
                                      alignment: Alignment.center,
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        child: Builder(
                                          builder: (c) {
                                            String imageUrl =
                                                userModel.getString(IMAGE_URL);
                                            bool online =
                                                imageUrl.contains("http");
                                            bool empty = imageUrl.isEmpty;

                                            if (!empty & !online)
                                              return Image.network(
                                                imageUrl,
                                                height: 100,
                                                width: 100,
                                                fit: BoxFit.cover,
                                              );

                                            if (!empty & online)
                                              return CachedNetworkImage(
                                                imageUrl: imageUrl,
                                                height: 100,
                                                width: 100,
                                                fit: BoxFit.cover,
                                              );

                                            return Icon(
                                              Icons.person,
                                              size: 30,
                                              color: appColor.withOpacity(.2),
                                            );
                                          },
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                          color: white.withOpacity(.2),
                                          boxShadow: [
                                            BoxShadow(
                                              color: black.withOpacity(.1),
                                              spreadRadius: 2,
                                              blurRadius: 5,
                                            )
                                          ],
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: white, width: 3)),
                                    ),
                                    if (avatarIndex == 100)
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Container(
                                          height: 35,
                                          width: 35,
                                          alignment: Alignment.center,
                                          child: Icon(
                                            Icons.check,
                                            size: 16,
                                            color: white,
                                          ),
                                          decoration: BoxDecoration(
                                              color: green_dark,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: black.withOpacity(.1),
                                                  spreadRadius: 2,
                                                  blurRadius: 5,
                                                )
                                              ],
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: white, width: 3)),
                                        ),
                                      ),
                                    Align(
                                      alignment: Alignment.bottomLeft,
                                      child: Container(
                                        height: 35,
                                        width: 35,
                                        alignment: Alignment.center,
                                        child: Icon(
                                          Icons.add,
                                          size: 16,
                                          color: white,
                                        ),
                                        decoration: BoxDecoration(
                                            color: black,
                                            boxShadow: [
                                              BoxShadow(
                                                color: black.withOpacity(.1),
                                                spreadRadius: 2,
                                                blurRadius: 5,
                                              )
                                            ],
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: white, width: 3)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  settingsItem(
    icon,
    String title,
    String desc,
    onClick,
  ) {
    return GestureDetector(
      onTap: () {
        onClick();
      },
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: white,
            ),
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 30,
                  width: 30,
                  child: Icon(
                    icon,
                    size: 18,
                    color: white,
                  ),
                  decoration: BoxDecoration(
                      color: appColor,
                      // color: Color(0XFF0B123E),
                      // color: Color(0XFF33CA7F).withOpacity(.2),
                      borderRadius: BorderRadius.circular(10)),
                ),
                addSpaceWidth(10),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    // if (desc.isNotEmpty)
                    Text(
                      desc,
                      style: TextStyle(
                        fontSize: 14,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                )),
                addSpaceWidth(10),
                Icon(
                  Icons.navigate_next,
                  size: 25,
                  color: Color(0XFF0B123E),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.only(top: 35, left: 20),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(10),
                ),
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: BoxDecoration(
                      border: Border(
                          // top: BorderSide(
                          //     color: Color(0XFF0B123E).withOpacity(.5),
                          //     width: 5),
                          left: BorderSide(
                              // color: Color(0XFF0B123E).withOpacity(.5),
                              color: appColor.withOpacity(.5),
                              width: 5),
                          bottom: BorderSide(
                              // color: Color(0XFF0B123E).withOpacity(.5),
                              color: appColor.withOpacity(.5),
                              width: 5))),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
