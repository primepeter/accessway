import 'package:Loopin/HomePage.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

class UpdatePasswordSheet extends StatefulWidget {
  @override
  _UpdatePasswordSheetState createState() => _UpdatePasswordSheetState();
}

class _UpdatePasswordSheetState extends State<UpdatePasswordSheet> {
  final oldPassController = TextEditingController();
  final passController = TextEditingController();
  final rePassController = TextEditingController();

  bool passVisible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // focusNode.addListener(() {
    //   setState(() {});
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding:
              const EdgeInsets.only(top: 0, right: 20, left: 20, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Update Password",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: black),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      inputField("Current Password", oldPassController,
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }),
                      inputField("New Password", passController,
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }),
                      inputField("Retype Password", rePassController,
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }),
                      addSpace(10),
                      Container(
                        height: 60,
                        child: TextButton(
                          onPressed: () {
                            String password = passController.text;
                            String rePassword = rePassController.text;
                            String oldPassword = oldPassController.text;
                            String currentPassword = userModel.getPassword();

                            if (oldPassword.isEmpty) {
                              showError(context, "Enter Current Password!");
                              return;
                            }
                            if (password.isEmpty) {
                              showError(context, "Enter Password!");
                              return;
                            }

                            if (rePassword.isEmpty) {
                              showError(context, "Enter Retype password!");
                              return;
                            }

                            if (password != rePassword) {
                              showError(context, "Password's don't match!");
                              return;
                            }

                            // if (currentPassword != oldPassword) {
                            //   showError(context, "Incorrect Old Password!");
                            //   return;
                            // }
                          },
                          child: Center(
                              child: Text(
                            "Update Password",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
