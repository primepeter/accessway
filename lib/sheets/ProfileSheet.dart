import 'dart:async';
import 'dart:io';

import 'package:Loopin/HomePage.dart';
import 'package:Loopin/auth/WelcomePage.dart';
import 'package:Loopin/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'UpdatePhotoSheet.dart';
import 'UpdateProfileSheet.dart';
import 'UpdatePasswordSheet.dart';

profileAvatar(context, {double size = 100, bool canEdit = false}) {
  int p = userModel.getInt(AVATAR_INDEX);
  String imageUrl = userModel.getString(IMAGE_URL);

  return GestureDetector(
    onTap: () {
      if (!canEdit) {
        return;
      }
      pushSheet(context, UpdatePhotoSheet(), result: (_) {});
    },
    child: Container(
      height: size,
      width: size,
      margin: EdgeInsets.all(10),
      child: Stack(
        children: [
          Container(
            height: size,
            width: size,
            alignment: Alignment.center,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Builder(builder: (c) {
                if (p == 100) {
                  if (imageUrl.isEmpty)
                    return Icon(
                      Icons.person,
                      size: 30,
                      color: appColor.withOpacity(.2),
                    );

                  return CachedNetworkImage(
                    imageUrl: imageUrl,
                    height: size,
                    width: size,
                    fit: BoxFit.cover,
                  );
                }

                return Image.asset("assets/avatars/${p + 1}.png");
              }),
            ),
            decoration: BoxDecoration(
                color: white.withOpacity(.2),
                boxShadow: [
                  BoxShadow(
                    color: black.withOpacity(.1),
                    spreadRadius: 2,
                    blurRadius: 5,
                  )
                ],
                shape: BoxShape.circle,
                border: Border.all(color: white, width: 3)),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              height: 35,
              width: 35,
              alignment: Alignment.center,
              child: Icon(
                Icons.edit,
                size: 16,
                color: white,
              ),
              decoration: BoxDecoration(
                  color: green_dark,
                  boxShadow: [
                    BoxShadow(
                      color: black.withOpacity(.1),
                      spreadRadius: 2,
                      blurRadius: 5,
                    )
                  ],
                  shape: BoxShape.circle,
                  border: Border.all(color: white, width: 3)),
            ),
          ),
        ],
      ),
    ),
  );
}

double profileLevel(BaseModel bm) {
  int count = bm.getInt(TRANSACTIONS_COUNT);
  if (count == 0) return 1;
  if (count > 0 && count < 10) return 2;
  if (count > 10 && count < 25) return 3;
  if (count > 25 && count < 50) return 4;
  return 5;
}

class ProfileSheet extends StatefulWidget {
  @override
  _ProfileSheetState createState() => _ProfileSheetState();
}

class _ProfileSheetState extends State<ProfileSheet> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var reload = reloadController.stream.listen((p) {
      if (mounted) setState(() {});
      print("reloadController 0");
    });
    subs.add(reload);
  }

  String get levelFeatures {
    final lBm = appSettingsModel.getListModel(LEVELS);
    return lBm[profileLevel(userModel).toInt() - 1].getString(FEATURES);
  }

  String get profileLevelTxt {
    int count = userModel.getInt(TRANSACTIONS_COUNT);
    if (count == 0) return "One";
    if (count > 0 && count < 10) return "Two";
    if (count > 10 && count < 25) return "Three";
    if (count > 25 && count < 50) return "Four";
    return "Five";
  }

  String get name {
    if (userModel.getInt(ACCOUNT_TYPE) == 1)
      return userModel.getString(CHILD_NAME);
    return userModel.getString(NAME);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Text(
                    "Settings (${userModel.getName()})",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                        color: black),
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      profileAvatar(context, canEdit: true),
                      addSpace(20),
                      settingsItem(Icons.person, "Edit Profile",
                          "Tap here to Update your Profile", () {
                        // pushSheet(context, UpdateProfileSheet());
                      }),
                      addLine(1, black.withOpacity(.05), 20, 0, 20, 5),
                      settingsItem(Icons.lock, "Change Password",
                          "Tap here to Update your Password", () {
                        // pushSheet(context, UpdatePasswordSheet());
                      }),
                      addLine(1, black.withOpacity(.05), 20, 0, 20, 5),
                      // settingsItem(Icons.vpn_key, "Change Pin",
                      //     "Tap here to Update your Pin", () {}),
                      // addLine(1, black.withOpacity(.05), 20, 0, 20, 5),
                      settingsItem(Icons.star, "Rate our App",
                          "Kindly tap here to Rate Us", () {
                        String link =
                            appSettingsModel.getString(PLAYSTORE_LINK);
                        if (Platform.isIOS) {
                          link = appSettingsModel.getString(APPLESTORE_LINK);
                        }
                        openLink(link);
                      }),
                      addLine(1, black.withOpacity(.05), 20, 0, 20, 5),

                      settingsItem(Icons.headset_mic, "Contact Support",
                          "Kindly tap here to Send Us a complain", () {
                        String link = appSettingsModel.getString(SUPPORT_EMAIL);
                        sendEmail(link);
                      }),
                      addLine(1, black.withOpacity(.05), 20, 0, 20, 5),
                      settingsItem(Icons.delete, "Delete Account", "", () {
                        showMessage(
                            context,
                            Icons.warning_amber_outlined,
                            red,
                            "Delete?",
                            "Your about to delete your logout, do you want to continue?",
                            clickYesText: "Delete",
                            clickNoText: "Cancel", onClicked: (_) async {
                          if (!_) return;
                          showProgress(true, context, msg: "Please wait...");

                          FirebaseMessaging.instance
                              .unsubscribeFromTopic("all");
                          FirebaseMessaging.instance
                              .unsubscribeFromTopic(userModel.getObjectId());

                          for (var s in subs) s?.cancel();
                          userModel.deleteItem();

                          if (userModel.getBoolean(IS_ANONYMOUS)) {
                            // await deleteFirebaseAccount();
                            await (FirebaseAuth.instance.currentUser.delete());
                          }

                          //await (FirebaseAuth.instance.currentUser.reload());
                          await FirebaseFirestore.instance.clearPersistence();
                          userModel = BaseModel();
                          friendList.clear();
                          pushAndResult(context, WelcomePage(),
                              clear: true, fade: true);
                        });
                      }),
                      addLine(1, black.withOpacity(.05), 20, 0, 20, 5),

                      settingsItem(Icons.exit_to_app, "Logout", "", () {
                        showMessage(
                            context,
                            Icons.warning_amber_outlined,
                            red,
                            "Logout?",
                            "Your about to logout, do you want to continue?",
                            clickYesText: "Logout",
                            clickNoText: "Cancel", onClicked: (_) async {
                          if (!_) return;
                          showProgress(true, context, msg: "Please wait...");

                          await FirebaseMessaging.instance
                              .unsubscribeFromTopic("all");
                          await FirebaseMessaging.instance
                              .unsubscribeFromTopic(userModel.getObjectId());
                          await FirebaseFirestore.instance.clearPersistence();

                          for (var s in subs) s?.cancel();
                          if (userModel.getBoolean(IS_ANONYMOUS)) {
                            userModel.deleteItem();
                            await (FirebaseAuth.instance.currentUser.reload());
                            await (FirebaseAuth.instance.currentUser.delete());
                          } else {
                            await (FirebaseAuth.instance.signOut());
                          }

                          userModel = BaseModel();
                          friendList.clear();
                          await FirebaseFirestore.instance.clearPersistence();

                          pushAndResult(context, WelcomePage(),
                              clear: true, fade: true);
                        });
                      }),
                      addLine(1, black.withOpacity(.05), 20, 0, 20, 5)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<bool> deleteFirebaseAccount() async {
    String email = userModel.getEmail();
    String password = userModel.getPassword();
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    await FirebaseAuth.instance.currentUser.delete();
    return true;
  }

  settingsItem(
    icon,
    String title,
    String desc,
    onClick,
  ) {
    return GestureDetector(
      onTap: () {
        onClick();
      },
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: white,
            ),
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 30,
                  width: 30,
                  child: Icon(
                    icon,
                    size: 16,
                    color: white,
                  ),
                  decoration: BoxDecoration(
                      color: appColor,
                      // color: Color(0XFF0B123E),
                      // color: Color(0XFF33CA7F).withOpacity(.2),
                      borderRadius: BorderRadius.circular(10)),
                ),
                addSpaceWidth(10),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    // if (desc.isNotEmpty)
                    Text(
                      desc,
                      style: TextStyle(
                        fontSize: 14,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                )),
                addSpaceWidth(10),
                Icon(
                  Icons.navigate_next,
                  size: 25,
                  color: Color(0XFF0B123E),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.only(top: 35, left: 20),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(10),
                ),
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: BoxDecoration(
                      border: Border(
                          // top: BorderSide(
                          //     color: Color(0XFF0B123E).withOpacity(.5),
                          //     width: 5),
                          left: BorderSide(
                              // color: Color(0XFF0B123E).withOpacity(.5),
                              color: appColor.withOpacity(.5),
                              width: 5),
                          bottom: BorderSide(
                              // color: Color(0XFF0B123E).withOpacity(.5),
                              color: appColor.withOpacity(.5),
                              width: 5))),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
