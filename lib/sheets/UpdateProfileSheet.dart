import 'package:Loopin/HomePage.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/assets.dart';

class UpdateProfileSheet extends StatefulWidget {
  @override
  _UpdateProfileSheetState createState() => _UpdateProfileSheetState();
}

class _UpdateProfileSheetState extends State<UpdateProfileSheet> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = MaskedTextController(mask: "0000-000-0000");
  final passController = TextEditingController();

  bool passVisible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: transparent,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: kTopHeight),
          padding:
              const EdgeInsets.only(top: 0, right: 20, left: 20, bottom: 20),
          // const EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(20),
              Center(
                child: Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.09),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              // addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(right: 15, left: 15, top: 20, bottom: 20),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Update Profile",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: black),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      inputField("First Name", firstNameController),
                      inputField("Last Name", lastNameController),
                      inputField("Email Address", emailController),
                      inputField("Phone Number", phoneController,
                          inputType: TextInputType.number),
                      addSpace(10),
                      Container(
                        height: 60,
                        child: TextButton(
                          onPressed: () async {
                            String fName = firstNameController.text;
                            String lName = lastNameController.text;
                            String email = emailController.text;
                            String phone = phoneController.text;

                            if (fName.isEmpty) {
                              showError(context, "Enter First name!");
                              return;
                            }
                            if (lName.isEmpty) {
                              showError(context, "Enter Last name!");
                              return;
                            }

                            if (email.isEmpty) {
                              showError(context, "Enter Email address!");
                              return;
                            }

                            if (phone.isEmpty) {
                              showError(context, "Enter Phone number!");
                              return;
                            }

                            FocusScope.of(context).unfocus();
                          },
                          child: Center(
                              child: Text(
                            "UPDATE PROFILE",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                      ),
                      addSpace(150),
                      // addSpace(250),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
