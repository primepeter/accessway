import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/sheets/FriendOptionSheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_carousel/infinite_carousel.dart';
import 'package:loop_page_view/loop_page_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../notificationService.dart';

// git remote set-url https://github.com/yamore/playgate.git
// nice

class StatsPage extends StatefulWidget {
  const StatsPage({Key key}) : super(key: key);

  @override
  _StatsPageState createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage>
    with AutomaticKeepAliveClientMixin {
  final refreshController = RefreshController();
  int limit = 50;

  @override
  initState() {
    super.initState();
    var reload = reloadController.stream.listen((p) {
      if (mounted) setState(() {});
      print("reloadController 0");
    });
    subs.add(reload);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Color(0xffEF7D00),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 120, bottom: 100),
        child: Column(
          children: [
            buildStats(),
            buildSocials(),
            buildBoards(),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  final controller = LoopPageController(viewportFraction: .85);
  int selectedIndex = 0;

  List get statsItems => [
        {
          "icon": "assets/tabs/paper-plane.png",
          "title": "Social Shares",
          "count": 55
        },
        {"icon": "assets/tabs/write.png", "title": "Post Created", "count": 18},
        {
          "icon": "assets/tabs/ranking.png",
          "title": "Most.Influential",
          "count": 4
        },
      ];

  Widget buildStats() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 10),

      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "SHARE STATISTICS".toUpperCase(),
            textAlign: TextAlign.center,
            maxLines: 1,
            style: TextStyle(
                color: white, fontSize: 20, fontWeight: FontWeight.bold),
          ),
          addSpace(10),
          Row(
            children: List.generate(statsItems.length, (p) {
              String icon = statsItems[p]["icon"];
              String title = statsItems[p]["title"];
              String count = statsItems[p]["count"].toString();
              return Expanded(
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(4),
                    margin: EdgeInsets.all(3),
                    decoration: BoxDecoration(border: Border.all(color: white)),
                    height: 120,
                    width: 120,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          icon,
                          height: 50,
                          width: 50,
                          fit: BoxFit.cover,
                        ),
                        addSpace(5),
                        Text(
                          count,
                          textAlign: TextAlign.center,
                          maxLines: 1,
                          style: TextStyle(
                              color: white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        addSpace(5),
                        Text(
                          title,
                          textAlign: TextAlign.center,
                          maxLines: 1,
                          style: TextStyle(
                              color: white,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  List get socialItems => [
        {"icon": "assets/tabs/facebook.png", "title": "13 FB Shares"},
        {"icon": "assets/tabs/instagram.png", "title": "20 IG Shares"},
        {"icon": "assets/tabs/twitter.png", "title": "13 Twitter Shares"},
        {"icon": "assets/tabs/telegram.png", "title": "13 Telegram Shares"},
        {"icon": "assets/tabs/whatsapp.png", "title": "3 Whatsapp Shares"},
        {"icon": "assets/tabs/linkedin.png", "title": "20 LinkedIn Shares"},
      ];

  Widget buildSocials() {
    return Container(
      // padding: EdgeInsets.all(10),
      color: Color(0xfffaffff),
      margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
      alignment: Alignment.center,
      child: Wrap(
        spacing: 8,
        children: List.generate(socialItems.length, (p) {
          String icon = socialItems[p]["icon"];
          String title = socialItems[p]["title"];
          String count = socialItems[p]["count"].toString();
          return InkWell(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.all(4),
              margin: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  color: Color(0xfffaffff), border: Border.all(color: white)),
              height: 120,
              width: (getScreenWidth(context) - 10) / 4,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 70,
                    width: 70,
                    child: Material(
                      shape: CircleBorder(),
                      elevation: 5,
                      child: Container(
                        padding: EdgeInsets.all(25),
                        child: Image.asset(
                          icon,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  addSpace(5),
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  List get leaderItems => [
        {
          "icon": "assets/stories/e0.png",
          "title": "Access Bank Commemorates "
              "World Hepatitis Day"
        },
        {"icon": "assets/stories/e1.png", "title": "Access Bank Swiftpay"},
        {"icon": "assets/stories/e1.png", "title": "Access Bank Swiftpay"},
      ];

  Widget buildBoards() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 10),

      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "LEADERBOARD".toUpperCase(),
            textAlign: TextAlign.center,
            maxLines: 1,
            style: TextStyle(
                color: white, fontSize: 20, fontWeight: FontWeight.bold),
          ),
          addSpace(10),
          Row(
            children: List.generate(leaderItems.length, (p) {
              String icon = leaderItems[p]["icon"];
              String title = leaderItems[p]["title"];
              String count = leaderItems[p]["count"].toString();
              return Expanded(
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(4),
                    margin: EdgeInsets.all(3),
                    decoration: BoxDecoration(border: Border.all(color: white)),
                    height: 120,
                    width: 120,
                    child: Image.asset(
                      icon,
                      // height: 50,
                      // width: 50,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}
