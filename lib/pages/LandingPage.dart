import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/sheets/FriendOptionSheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_carousel/infinite_carousel.dart';
import 'package:loop_page_view/loop_page_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../notificationService.dart';

// git remote set-url https://github.com/yamore/playgate.git
// nice

class LandingPage extends StatefulWidget {
  const LandingPage({Key key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with AutomaticKeepAliveClientMixin {
  // bool isOfferBusy = true;
  // List<Offer> myOffers = [];

  final refreshController = RefreshController();
  int limit = 50;

  @override
  initState() {
    super.initState();
    var reload = reloadController.stream.listen((p) {
      if (mounted) setState(() {});
      print("reloadController 0");
    });
    subs.add(reload);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Color(0xfffaffff),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 120, bottom: 100),
        child: Column(
          children: [buildInfluencers(), buildTopStories(), buildPosts()],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  final controller = LoopPageController(viewportFraction: .85);
  int selectedIndex = 0;

  List get topInfluencerItems => [
        {"icon": "assets/stories/s0.png", "title": "Ademola"},
        {"icon": "assets/stories/s1.png", "title": "Nkechi"},
        {"icon": "assets/stories/s2.png", "title": "kelvin MC."},
        {"icon": "assets/stories/s3.png", "title": "Tony Adebayo"},
        {"icon": "assets/stories/s4.png", "title": "Bambo"},
        {"icon": "assets/stories/s5.png", "title": "Ezekiel"},
      ];

  Widget buildInfluencers() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 10),

      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Top Influencers".toUpperCase(),
            textAlign: TextAlign.center,
            maxLines: 1,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          ),
          addSpace(10),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(topInfluencerItems.length, (p) {
                String icon = topInfluencerItems[p]["icon"];
                String title = topInfluencerItems[p]["title"];
                return InkWell(
                  onTap: () {},
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.all(4),
                        margin: EdgeInsets.all(3),
                        height: 90,
                        width: 70,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(80),
                              child: Image.asset(
                                icon,
                                height: 60,
                                width: 60,
                                fit: BoxFit.cover,
                              ),
                            ),
                            addSpace(5),
                            Text(
                              title,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                      ),
                      if (p == 1)
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            height: 25,
                            width: 25,
                            child: Material(
                              elevation: 4,
                              // color: green_dark,
                              shape: CircleBorder(),
                              child: Container(
                                padding: EdgeInsets.all(2),
                                child: Image.asset(
                                  "assets/stories/crown.png",
                                  height: 20,
                                  width: 20,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  List get topStoriesItems => [
        {
          "icon": "assets/stories/e0.png",
          "title": "Access Bank Commemorates "
              "World Hepatitis Day"
        },
        {"icon": "assets/stories/e1.png", "title": "Access Bank Swiftpay"},
        {"icon": "assets/stories/e1.png", "title": "Access Bank Swiftpay"},
      ];

  Widget buildTopStories() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 20, 10, 10),

      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "TOP STORIES & EVENTS".toUpperCase(),
            textAlign: TextAlign.center,
            maxLines: 1,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          addSpace(10),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(topStoriesItems.length, (p) {
                String photo = topStoriesItems[p]["icon"];
                String msg = topStoriesItems[p]["title"];
                return InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Material(
                      elevation: 5,
                      color: white,
                      borderRadius: BorderRadius.circular(10),
                      child: Container(
                        height: 300,
                        width: 250,
                        // padding: EdgeInsets.all(4),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.all(8),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          ic_launcher,
                                          fit: BoxFit.cover,
                                          height: 28,
                                          width: 28,
                                        ),
                                        addSpaceWidth(4),
                                        Text(
                                          "AccessBank",
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        addSpaceWidth(4),
                                        Image.asset(
                                          "assets/stories/verified.png",
                                          fit: BoxFit.cover,
                                          height: 16,
                                          width: 16,
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextButton(
                                      onPressed: () {
                                        onShare(context, photo, msg);
                                      },
                                      style: TextButton.styleFrom(
                                          tapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          shape: CircleBorder(),
                                          minimumSize: Size(20, 20)),
                                      child: Image.asset(
                                        "assets/tabs/direct.png",
                                        fit: BoxFit.cover,
                                        height: 20,
                                        width: 20,
                                        color: black,
                                      )
                                      // imageItem(
                                      //     "assets/tabs/send.png", 20, black)
                                      )
                                ],
                              ),
                            ),
                            Expanded(
                              child: Image.asset(
                                photo,
                                width: 250,
                                fit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                              child: Text(
                                msg,
                                textAlign: TextAlign.start,
                                maxLines: 1,
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  List get postItems => [
        {
          "isMsg": true,
          "photo": "assets/stories/s4.png",
          "image": "assets/stories/img0.png",
          "name": "Kelvin McJonathan",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s0.png",
          "image": "assets/stories/img0.png",
          "name": "AccessBank",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s1.png",
          "image": "assets/stories/e0.png",
          "name": "Peter Okachie",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s2.png",
          "image": "assets/stories/e1.png",
          "name": "Ademolar Williams",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
      ];

  Widget buildPosts() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),

      alignment: Alignment.center,
      child: Column(
        children: List.generate(postItems.length, (p) {
          final item = postItems[p];
          String photo = item["photo"];
          String image = item["image"];
          String name = item["name"];
          String msg = item["msg"];
          bool isMsg = item["isMsg"] ?? false;

          if (isMsg)
            return Container(
              margin: EdgeInsets.all(8),
              child: Material(
                elevation: 5,
                color: white,
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  // height: 300,
                  // width: 250,
                  // padding: EdgeInsets.all(4),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(40),
                                    child: Image.asset(
                                      photo,
                                      fit: BoxFit.cover,
                                      height: 40,
                                      width: 40,
                                    ),
                                  ),
                                  addSpaceWidth(4),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            name,
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          addSpaceWidth(4),
                                          Image.asset(
                                            "assets/stories/verified.png",
                                            fit: BoxFit.cover,
                                            height: 16,
                                            width: 16,
                                          ),
                                        ],
                                      ),
                                      Text(
                                        "Data Researcher",
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              "12min ago",
                              style: TextStyle(
                                fontSize: 10,
                                // fontWeight: FontWeight.w400
                              ),
                            ),
                            TextButton(
                                onPressed: () {},
                                style: TextButton.styleFrom(
                                    tapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    shape: CircleBorder(),
                                    minimumSize: Size(20, 20)),
                                child: Icon(
                                  Icons.more_horiz,
                                  size: 18,
                                  color: black,
                                )
                                // imageItem(
                                //     "assets/tabs/send.png", 20, black)
                                )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(50, 5, 10, 15),
                        child: Text(
                          msg,
                          textAlign: TextAlign.start,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/heart.png",
                                height: 20,
                                width: 20,
                                color: black,
                              )),
                          TextButton(
                              onPressed: () {
                                onShare(context, "", msg);
                              },
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/direct.png",
                                height: 20,
                                width: 20,
                                color: black,
                              )),
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/bookmark.png",
                                height: 20,
                                width: 20,
                                color: black,
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          return Container(
            margin: EdgeInsets.all(8),
            child: Material(
              elevation: 5,
              color: white,
              borderRadius: BorderRadius.circular(10),
              child: Container(
                // height: 300,
                // width: 250,
                // padding: EdgeInsets.all(4),
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Image.asset(
                                    photo,
                                    fit: BoxFit.cover,
                                    height: 40,
                                    width: 40,
                                  ),
                                ),
                                addSpaceWidth(4),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          name,
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        addSpaceWidth(4),
                                        Image.asset(
                                          "assets/stories/verified.png",
                                          fit: BoxFit.cover,
                                          height: 16,
                                          width: 16,
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "Data Researcher",
                                      style: TextStyle(
                                          fontSize: 11,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Text(
                            "12min ago",
                            style: TextStyle(
                              fontSize: 10,
                              // fontWeight: FontWeight.w400
                            ),
                          ),
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: CircleBorder(),
                                  minimumSize: Size(20, 20)),
                              child: Icon(
                                Icons.more_horiz,
                                size: 18,
                                color: black,
                              )
                              // imageItem(
                              //     "assets/tabs/send.png", 20, black)
                              )
                        ],
                      ),
                    ),
                    Image.asset(
                      image,
                      height: 250,
                      width: getScreenWidth(context),
                      fit: BoxFit.fitWidth,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                      child: Text(
                        msg,
                        textAlign: TextAlign.start,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/heart.png",
                              height: 20,
                              width: 20,
                              color: black,
                            )),
                        TextButton(
                            onPressed: () {
                              onShare(context, image, msg);
                            },
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/direct.png",
                              height: 20,
                              width: 20,
                              color: black,
                            )),
                        TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/bookmark.png",
                              height: 20,
                              width: 20,
                              color: black,
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
