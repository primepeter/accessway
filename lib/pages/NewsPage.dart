import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/sheets/FriendOptionSheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_carousel/infinite_carousel.dart';
import 'package:loop_page_view/loop_page_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../notificationService.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key key}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage>
    with AutomaticKeepAliveClientMixin {
  // bool isOfferBusy = true;
  // List<Offer> myOffers = [];
  List pageItems = [
    "Technologies",
    "Companies",
    "Exchange Rates",
    "Trending",
    "Charity",
    "News",
    "Gossips"
  ];

  final refreshController = RefreshController();
  int limit = 50;

  @override
  initState() {
    super.initState();
    var reload = reloadController.stream.listen((p) {
      if (mounted) setState(() {});
      print("reloadController 0");
    });
    subs.add(reload);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Color(0xfffaffff),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 120, bottom: 100),
        child: Column(
          children: [buildCategories(), buildPosts()],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  final controller = LoopPageController(viewportFraction: .85);
  int selectedIndex = 0;

  Widget buildCategories() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(pageItems.length, (p) {
            String title = pageItems[p];
            return InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                margin: EdgeInsets.all(3),
                decoration: BoxDecoration(
                    color: orange0, borderRadius: BorderRadius.circular(25)),
                alignment: Alignment.center,
                height: 35,
                child: Text(
                  title.toUpperCase(),
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  List get postItems => [
        {
          "isMsg": false,
          "photo": "assets/stories/s0.png",
          "image": "assets/stories/img0.png",
          "name": "AccessBank",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": true,
          "photo": "assets/stories/s4.png",
          "image": "assets/stories/img0.png",
          "name": "Kelvin McJonathan",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s1.png",
          "image": "assets/stories/e0.png",
          "name": "Peter Okachie",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s2.png",
          "image": "assets/stories/e1.png",
          "name": "Ademolar Williams",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
      ];

  Widget buildPosts() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),

      alignment: Alignment.center,
      child: Column(
        children: List.generate(postItems.length, (p) {
          final item = postItems[p];
          String photo = item["photo"];
          String image = item["image"];
          String name = item["name"];
          String msg = item["msg"];
          bool isMsg = item["isMsg"] ?? false;

          if (isMsg)
            return Container(
              margin: EdgeInsets.all(8),
              child: Material(
                elevation: 5,
                color: white,
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  // height: 300,
                  // width: 250,
                  // padding: EdgeInsets.all(4),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(40),
                                    child: Image.asset(
                                      photo,
                                      fit: BoxFit.cover,
                                      height: 40,
                                      width: 40,
                                    ),
                                  ),
                                  addSpaceWidth(4),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            name,
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          addSpaceWidth(4),
                                          Image.asset(
                                            "assets/stories/verified.png",
                                            fit: BoxFit.cover,
                                            height: 16,
                                            width: 16,
                                          ),
                                        ],
                                      ),
                                      Text(
                                        "Data Researcher",
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              "12min ago",
                              style: TextStyle(
                                fontSize: 10,
                                // fontWeight: FontWeight.w400
                              ),
                            ),
                            TextButton(
                                onPressed: () {},
                                style: TextButton.styleFrom(
                                    tapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    shape: CircleBorder(),
                                    minimumSize: Size(20, 20)),
                                child: Icon(
                                  Icons.more_horiz,
                                  size: 18,
                                  color: black,
                                )
                                // imageItem(
                                //     "assets/tabs/send.png", 20, black)
                                )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(50, 5, 10, 15),
                        child: Text(
                          msg,
                          textAlign: TextAlign.start,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/heart.png",
                                height: 20,
                                width: 20,
                                color: black,
                              )),
                          TextButton(
                              onPressed: () {
                                onShare(context, image, msg);
                              },
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/direct.png",
                                height: 20,
                                width: 20,
                                color: black,
                              )),
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/bookmark.png",
                                height: 20,
                                width: 20,
                                color: black,
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          return Container(
            margin: EdgeInsets.all(8),
            child: Material(
              elevation: 5,
              color: white,
              borderRadius: BorderRadius.circular(10),
              child: Container(
                // height: 300,
                // width: 250,
                // padding: EdgeInsets.all(4),
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Image.asset(
                                    photo,
                                    fit: BoxFit.cover,
                                    height: 40,
                                    width: 40,
                                  ),
                                ),
                                addSpaceWidth(4),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          name,
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        addSpaceWidth(4),
                                        Image.asset(
                                          "assets/stories/verified.png",
                                          fit: BoxFit.cover,
                                          height: 16,
                                          width: 16,
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "Data Researcher",
                                      style: TextStyle(
                                          fontSize: 11,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Text(
                            "12min ago",
                            style: TextStyle(
                              fontSize: 10,
                              // fontWeight: FontWeight.w400
                            ),
                          ),
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: CircleBorder(),
                                  minimumSize: Size(20, 20)),
                              child: Icon(
                                Icons.more_horiz,
                                size: 18,
                                color: black,
                              )
                              // imageItem(
                              //     "assets/tabs/send.png", 20, black)
                              )
                        ],
                      ),
                    ),
                    Image.asset(
                      image,
                      height: 250,
                      width: getScreenWidth(context),
                      fit: BoxFit.fitWidth,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                      child: Text(
                        msg,
                        textAlign: TextAlign.start,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/heart.png",
                              height: 20,
                              width: 20,
                              color: black,
                            )),
                        TextButton(
                            onPressed: () {
                              onShare(context, image, msg);
                            },
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/direct.png",
                              height: 20,
                              width: 20,
                              color: black,
                            )),
                        TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/bookmark.png",
                              height: 20,
                              width: 20,
                              color: black,
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
