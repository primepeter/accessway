import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/sheets/FriendOptionSheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_carousel/infinite_carousel.dart';
import 'package:loop_page_view/loop_page_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../notificationService.dart';

// git remote set-url https://github.com/yamore/playgate.git
// nice

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with AutomaticKeepAliveClientMixin {
  final refreshController = RefreshController();
  int limit = 50;

  @override
  initState() {
    super.initState();
    var reload = reloadController.stream.listen((p) {
      if (mounted) setState(() {});
      print("reloadController 0");
    });
    subs.add(reload);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Color(0xfffaffff),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 120, bottom: 100),
        child: Column(
          children: [buildProfile(), buildPosts()],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  final controller = LoopPageController(viewportFraction: .85);
  int selectedIndex = 0;

  List get statusItems => [
        {"count": 11, "title": "People viewed your profile"},
        {"count": 33, "title": "People shared your posts"},
        {"count": 10, "title": "People liked your posts"},
      ];

  Widget buildProfile() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      // margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),

      alignment: Alignment.center,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                height: 100,
                width: 100,
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image.asset(
                        "assets/stories/s4.png",
                        fit: BoxFit.cover,
                        height: 100,
                        width: 100,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        height: 30,
                        width: 30,
                        child: Material(
                          elevation: 4,
                          // color: green_dark,
                          shape: CircleBorder(),
                          child: Container(
                            padding: EdgeInsets.all(5),
                            child: Image.asset(
                              "assets/tabs/plus.png",
                              height: 20,
                              width: 20,
                              color: Color(0xff023B7E),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              addSpaceWidth(10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          "Bambo Adewole",
                          textAlign: TextAlign.center,
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w600),
                        ),
                        addSpaceWidth(4),
                        Image.asset(
                          "assets/stories/verified.png",
                          fit: BoxFit.cover,
                          height: 16,
                          width: 16,
                        ),
                      ],
                    ),
                    Text(
                      "Data Annotator | ML",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                    ),
                    Text(
                      "Access Bank, Oniru",
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    ),
                    Container(
                      height: 45,
                      child: Row(
                        children: [
                          Expanded(
                              child: Container(
                            child: TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  backgroundColor: Color(0xff023B7E),
                                  primary: white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30))),
                              child: Text(
                                "Connections",
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          )),
                          addSpaceWidth(10),
                          Expanded(
                              child: Container(
                            child: TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                primary: Color(0xff023B7E),
                                backgroundColor: white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    side: BorderSide(color: Color(0xff023B7E))),
                              ),
                              child: Text(
                                "Following",
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          addSpace(10),
          Container(
            // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Color(0xff023B7E).withOpacity(.09))),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Row(
                children: List.generate(statusItems.length, (p) {
                  String title = statusItems[p]["title"];
                  String count = statusItems[p]["count"].toString();

                  // Color(0xff023B7E)
                  return Expanded(
                    child: Container(
                      // padding: EdgeInsets.all(2),
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: white,
                          border: Border.all(
                              color: Color(0xff023B7E).withOpacity(.09))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            count,
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff023B7E)),
                          ),
                          Text(
                            title,
                            style: TextStyle(
                                fontSize: 12, color: Color(0xff023B7E)),
                          )
                        ],
                      ),
                    ),
                  );
                }),
              ),
            ),
          )
        ],
      ),
    );
  }

  List get postItems => [
        {
          "isMsg": true,
          "photo": "assets/stories/s4.png",
          "image": "assets/stories/img0.png",
          "name": "Kelvin McJonathan",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s0.png",
          "image": "assets/stories/img0.png",
          "name": "AccessBank",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s1.png",
          "image": "assets/stories/e0.png",
          "name": "Peter Okachie",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
        {
          "isMsg": false,
          "photo": "assets/stories/s2.png",
          "image": "assets/stories/e1.png",
          "name": "Ademolar Williams",
          "msg": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              " Ut vitae pharetra nisi, quis viverra est. Curabitur elit sem, "
              "facilisis sed interdum id, sagittis nec purus."
        },
      ];

  Widget buildPosts() {
    return Container(
      // padding: EdgeInsets.all(4),
      // color: white,
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),

      alignment: Alignment.center,
      child: Column(
        children: List.generate(postItems.length, (p) {
          final item = postItems[p];
          String photo = item["photo"];
          String image = item["image"];
          String name = item["name"];
          String msg = item["msg"];
          bool isMsg = item["isMsg"] ?? false;

          if (isMsg)
            return Container(
              margin: EdgeInsets.all(8),
              child: Material(
                elevation: 5,
                color: white,
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  // height: 300,
                  // width: 250,
                  // padding: EdgeInsets.all(4),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(40),
                                    child: Image.asset(
                                      photo,
                                      fit: BoxFit.cover,
                                      height: 40,
                                      width: 40,
                                    ),
                                  ),
                                  addSpaceWidth(4),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            name,
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          addSpaceWidth(4),
                                          Image.asset(
                                            "assets/stories/verified.png",
                                            fit: BoxFit.cover,
                                            height: 16,
                                            width: 16,
                                          ),
                                        ],
                                      ),
                                      Text(
                                        "Data Researcher",
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              "12min ago",
                              style: TextStyle(
                                fontSize: 10,
                                // fontWeight: FontWeight.w400
                              ),
                            ),
                            TextButton(
                                onPressed: () {},
                                style: TextButton.styleFrom(
                                    tapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    shape: CircleBorder(),
                                    minimumSize: Size(20, 20)),
                                child: Icon(
                                  Icons.more_horiz,
                                  size: 18,
                                  color: black,
                                )
                                // imageItem(
                                //     "assets/tabs/send.png", 20, black)
                                )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(50, 5, 10, 15),
                        child: Text(
                          msg,
                          textAlign: TextAlign.start,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/heart.png",
                                height: 20,
                                width: 20,
                                color: black,
                              )),
                          TextButton(
                              onPressed: () {
                                onShare(context, image, msg);
                              },
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/direct.png",
                                height: 20,
                                width: 20,
                                color: black,
                              )),
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  shape: CircleBorder(),
                                  minimumSize: Size(40, 40)),
                              child: Image.asset(
                                "assets/tabs/bookmark.png",
                                height: 20,
                                width: 20,
                                color: black,
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          return Container(
            margin: EdgeInsets.all(8),
            child: Material(
              elevation: 5,
              color: white,
              borderRadius: BorderRadius.circular(10),
              child: Container(
                // height: 300,
                // width: 250,
                // padding: EdgeInsets.all(4),
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Image.asset(
                                    photo,
                                    fit: BoxFit.cover,
                                    height: 40,
                                    width: 40,
                                  ),
                                ),
                                addSpaceWidth(4),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          name,
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        addSpaceWidth(4),
                                        Image.asset(
                                          "assets/stories/verified.png",
                                          fit: BoxFit.cover,
                                          height: 16,
                                          width: 16,
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "Data Researcher",
                                      style: TextStyle(
                                          fontSize: 11,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Text(
                            "12min ago",
                            style: TextStyle(
                              fontSize: 10,
                              // fontWeight: FontWeight.w400
                            ),
                          ),
                          TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: CircleBorder(),
                                  minimumSize: Size(20, 20)),
                              child: Icon(
                                Icons.more_horiz,
                                size: 18,
                                color: black,
                              )
                              // imageItem(
                              //     "assets/tabs/send.png", 20, black)
                              )
                        ],
                      ),
                    ),
                    Image.asset(
                      image,
                      height: 250,
                      width: getScreenWidth(context),
                      fit: BoxFit.fitWidth,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                      child: Text(
                        msg,
                        textAlign: TextAlign.start,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/heart.png",
                              height: 20,
                              width: 20,
                              color: black,
                            )),
                        TextButton(
                            onPressed: () {
                              onShare(context, image, msg);
                            },
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/direct.png",
                              height: 20,
                              width: 20,
                              color: black,
                            )),
                        TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                shape: CircleBorder(),
                                minimumSize: Size(40, 40)),
                            child: Image.asset(
                              "assets/tabs/bookmark.png",
                              height: 20,
                              width: 20,
                              color: black,
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
