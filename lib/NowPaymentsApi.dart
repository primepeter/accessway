// import 'dart:convert';
// import 'dart:io';
// import 'package:http/http.dart' as http;
//
//
// //Secret Key aMII5odqGiHeCc9ApZxbJHv+Pt+6viFQ
// ///Live && Sandbox
// // GET 1. Get API status
// // GET 2. Get available currencies
// // GET 3. Get estimated price
// // POST 4. Create payment
// // GET 5. Get payment status
// // GET 6. Get the minimum payment amount
//
// ///live ONLY
// // GET 7. Get list of payments
// // POST 8. Create invoice
//
// abstract class NowBase {
//   Future<String> apiStatus();
//   Future<List> availableCurrencies();
//   Future<PriceResponse> estimatedPrice(
//       double amount, String currencyFrom, String currencyTo);
//   Future<PaymentResponse> createPayment(
//       double priceAmount,
//       String priceCurrency,
//       String payCurrency,
//       String ipnCallbackUrl,
//       String orderId,
//       String orderDescription);
//   Future paymentStatus(String paymentId);
//   Future<PriceResponse> minPaymentAmount(
//       String currencyFrom, String currencyTo);
//   Future<List<PaymentResponse>> listPayments(
//       {int limit = 10,
//       int page = 0,
//       String sortBy = "created_at",
//       String orderBy = "asc",
//       String dateFrom,
//       String dateTo});
//   // Future createInvoice();
// }
//
// class ExchangeResponse {
//   final double naira;
//   final double dollar;
//   ExchangeResponse(Map item)
//       : naira = formateDouble(item["NGN"]),
//         dollar = formateDouble(item["USD"]);
// }
//
// class PriceResponse {
//   final String currencyFrom;
//   final String currencyTo;
//   final double amountFrom;
//   final double estimatedAmount;
//   final double minAmount;
//
//   PriceResponse(Map item)
//       : currencyFrom = item["currency_from"],
//         currencyTo = item["currency_to"],
//         amountFrom = formateDouble(item["amount_from"]),
//         estimatedAmount = item["estimated_amount"] ?? 0.0,
//         minAmount = item["min_amount"] ?? 0.0;
// }
//
// formateDouble(var value) {
//   if (null == value) return 0.0;
//
//   if (value is num) return value.toDouble();
//   return num.parse(value.toString()).toDouble();
// }
//
// class PaymentResponse {
//   final String paymentId;
//   final String paymentStatus;
//   final String payAddress;
//   final double priceAmount;
//   final String priceCurrency;
//   final double payAmount;
//   final String payCurrency;
//   final String orderId;
//   final String orderDescription;
//   final String ipnCallbackUrl;
//   final String createdAt;
//   final String updatedAt;
//   final String purchaseId;
//   final String outcomeCurrency;
//   final double outcomeAmount;
//   final double actuallyPaid;
//
//   PaymentResponse(Map item)
//       : paymentId = item["payment_id"],
//         paymentStatus = item["payment_status"],
//         payAddress = item["pay_address"],
//         priceAmount = item["price_amount"] ?? 0.0,
//         priceCurrency = item["price_currency"],
//         payAmount = item["pay_amount"] ?? 0.0,
//         payCurrency = item["pay_currency"],
//         orderId = item["order_id"],
//         orderDescription = item["order_description"],
//         ipnCallbackUrl = item["ipn_callback_url"],
//         createdAt = item["created_at"],
//         updatedAt = item["updated_at"],
//         purchaseId = item["purchase_id"],
//         outcomeCurrency = item["outcome_currency"],
//         outcomeAmount = item["outcome_amount"] ?? 0.0,
//         actuallyPaid = item["actually_paid"] ?? 0.0;
// }
//
// class NowPaymentsApi extends NowBase {
//   final bool sandBox;
//   final String requestUrl;
//   final Map<String, String> headers;
//   final String key;
//   NowPaymentsApi({
//     this.sandBox = false,
//     this.key,
//   })  : requestUrl = sandBox
//             ? "https://api.sandbox.nowpayments.io/v1/"
//             : "https://api.nowpayments.io/v1/",
//         headers = {"x-api-key": key, "Content-Type": "application/json"};
//
//   @override
//   Future<String> apiStatus() async {
//     final response = await http.get(requestUrl + 'status');
//     if (response.statusCode == HttpStatus.ok) {
//       Map map = jsonDecode(response.body);
//       String message = map["message"];
//       return message;
//     }
//     return Future.error("An error occurred while fetching api status.");
//   }
//
//   Future<ExchangeResponse> exchangeRate(String fromCurrency) async {
//     final response = await http.get(
//         "https://min-api.cryptocompare.com/data/price?fsym=$fromCurrency&tsyms=USD,NGN");
//     if (response.statusCode == HttpStatus.ok) {
//       Map map = jsonDecode(response.body);
//       return ExchangeResponse(map);
//     }
//     return Future.error(
//         "An error occurred while fetching currency exchange rate.");
//   }
//
//   @override
//   Future<List> availableCurrencies() async {
//     final response =
//         await http.get(requestUrl + 'currencies', headers: headers);
//     if (response.statusCode == HttpStatus.ok) {
//       Map map = jsonDecode(response.body);
//       List currencies = List.from(map["currencies"]);
//       return currencies;
//     }
//     return Future.error(
//         "An error occurred while fetching available Currencies.");
//   }
//
//   // @override
//   // Future createInvoice() {
//   //   // TODO: implement createInvoice
//   //   throw UnimplementedError();
//   // }
//
//   @override
//   Future<PaymentResponse> createPayment(
//       double priceAmount,
//       String priceCurrency,
//       String payCurrency,
//       String ipnCallbackUrl,
//       String orderId,
//       String orderDescription) async {
//     final response =
//         await http.post(requestUrl + "payment", headers: headers, body: {
//       "price_amount": priceAmount,
//       "price_currency": priceCurrency,
//       "pay_currency": payCurrency,
//       "ipn_callback_url": ipnCallbackUrl,
//       "order_id": orderId,
//       "order_description": orderDescription
//     });
//     if (response.statusCode == HttpStatus.created) {
//       Map map = jsonDecode(response.body);
//       return PaymentResponse(map);
//     }
//     return Future.error(
//         "An error occurred while fetching available Currencies.");
//   }
//
//   @override
//   Future<PriceResponse> estimatedPrice(
//       double amount, String currencyFrom, String currencyTo) async {
//     final response = await http.get(
//         requestUrl +
//             "estimate?amount=$amount&currency_from=$currencyFrom&currency_to=$currencyTo",
//         headers: headers);
//     print(response.body);
//     if (response.statusCode == HttpStatus.ok) {
//       Map map = jsonDecode(response.body);
//       return PriceResponse(map);
//     }
//
//     return Future.error("An error occurred while fetching price estimate.");
//   }
//
//   @override
//   Future<List<PaymentResponse>> listPayments(
//       {int limit = 10,
//       int page = 0,
//       String sortBy = "created_at",
//       String orderBy = "asc",
//       String dateFrom,
//       String dateTo}) async {
//     if (null == dateFrom)
//       // dateFrom = formateDate(DateTime.now().millisecondsSinceEpoch);
//     if (null == dateTo)
//       // dateTo = formateDate(DateTime.now().millisecondsSinceEpoch);
//
//     final response = await http.get(
//         requestUrl +
//             "payment/?limit=$limit&page=$page&sortBy=$sortBy&orderBy=$orderBy&dateFrom=$dateFrom&dateTo=$dateTo",
//         headers: headers);
//     // if (response.statusCode == HttpStatus.ok) {
//     //   List items = List.from(jsonDecode(response.body));
//     //   return items.map((e) => PaymentResponse(e)).toList();
//     // }
//     return Future.error(
//         "An error occurred while fetching available Currencies.");
//   }
//
//   @override
//   Future<PriceResponse> minPaymentAmount(
//       String currencyFrom, String currencyTo) async {
//     final response = await http.get(
//         requestUrl +
//             "min-amount?currency_from=$currencyFrom&currency_to=$currencyTo",
//         headers: headers);
//     if (response.statusCode == HttpStatus.ok) {
//       Map map = jsonDecode(response.body);
//       return PriceResponse(map);
//     }
//
//     return Future.error(
//         "An error occurred while fetching available Currencies.");
//   }
//
//   @override
//   Future<PaymentResponse> paymentStatus(String paymentId) async {
//     final response =
//         await http.get(requestUrl + 'payment/$paymentId', headers: headers);
//     if (response.statusCode == HttpStatus.ok) {
//       Map map = jsonDecode(response.body);
//       return PaymentResponse(map);
//     }
//     return Future.error("An error occurred while fetching payment status.");
//   }
// }
