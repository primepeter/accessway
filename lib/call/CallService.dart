import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

// typedef OnIncomingCall = Function(String callerId);

enum CallEvent { incoming, outgoing }

class CallService {
  static CallService _instance;
  static CallService get instance {
    if (null == _instance) {
      _instance = CallService();
    }
    return _instance;
  }

  // static OnIncomingCall onIncomingCall;

  BuildContext context;
  final _listener = StreamController<CallEvent>.broadcast();

  void dispose() {
    _listener?.close();
  }

  void initialize(BuildContext context, listener) {
    this.context = context;

    // handle call events
    this._listener.stream.listen((event) {
      if (event == CallEvent.incoming) {}

      if (event == CallEvent.outgoing) {}
    });

    //listen for calls
  }

  void placeCall() async {
    bool onAndroid = Platform.isAndroid;
  }
}

// git remote add origin https://mtellect@bitbucket.org/primepeter/accessway.git
