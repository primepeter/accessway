import 'package:im_animations/im_animations.dart';
import 'dart:async';
import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:smart_flare/actors/smart_flare_actor.dart';

StreamController<dynamic> progressController =
    StreamController<dynamic>.broadcast();
bool progressDialogShowing = false;

class progressDialog extends StatefulWidget {
  String message;
  bool cancelable;
  double countDown;
  progressDialog(
      {this.message = "", this.cancelable = false, this.countDown = 0});
  @override
  _progressDialogState createState() => _progressDialogState();
}

class _progressDialogState extends State<progressDialog> {
  String message;
  bool cancelable;
  double countDown;
  StreamSubscription sub;

  bool showBack = false;
  bool hideUI = true;
  bool hidePro = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    message = widget.message ?? "Please wait";
    cancelable = widget.cancelable ?? false;
    countDown = widget.countDown ?? 0;
    sub = progressController.stream.listen((_) {
      if (!_) {
        progressDialogShowing = false;
        closePage(() {
          Navigator.pop(context);
        });
      } else {
        if (_ is String) {
          message = _;
          setState(() {});
        }
      }
    });

    Future.delayed(Duration(milliseconds: 200), () {
      hideUI = false;
      if (mounted) setState(() {});
    });
    Future.delayed(Duration(milliseconds: 500), () {
      showBack = true;
      if (mounted) setState(() {});
    });
    Future.delayed(Duration(milliseconds: 3000), () {
      hidePro = true;
      if (mounted) setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    sub.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Stack(fit: StackFit.expand, children: <Widget>[
        AnimatedOpacity(
          opacity: hideUI
              ? 0
              : showBack
                  ? 1
                  : 0,
          duration: Duration(milliseconds: 300),
          child: ClipRect(
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    color: black.withOpacity(.7),
                  ))),
        ),
        // Center(
        //   child: AnimatedOpacity(
        //     opacity: showBack?0:1,duration: Duration(milliseconds: 300),
        //     child: Container(
        //       width: 80,
        //       height: 80,
        //       decoration: BoxDecoration(
        //         color: white.withOpacity(.5),
        //         shape: BoxShape.circle
        //       ),
        //     ),
        //   )
        // ),
        page(),
      ]),
      onWillPop: () {
        if (cancelable)
          closePage(() {
            Navigator.pop(context);
          });
        return;
      },
    );
  }

  page() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        // if(!hidePro)Center(child:Container(
        //     width: 100,height: 100,
        //     child: LoadingIndicator(indicatorType: Indicator.ballScale, color: white,))),

        // Center(
        //     child: Container(
        //   margin: EdgeInsets.only(bottom: 60),
        //   child: SmartFlareActor(
        //       width: 180,
        //       height: 100,
        //       startingAnimation: "1",
        //       // playStartingAnimationWhenRebuilt: false,
        //       // controller: flareController,
        //       filename: 'assets/intro.flr'),
        // )),

        Center(
          child: HeartBeat(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: Image.asset(
                "assets/icons/ic_launcher.png",
                // "assets/icons/lucro_small.png",
                height: 80,
                width: 80,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Center(
          child: Center(
            child: Container(
                width: 20,
                height: 20,
                //margin: EdgeInsets.only(top: 13),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(white.withOpacity(.6)),
                  strokeWidth: 3,
                )),
          ),
        ),

        Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: new Container(),
              flex: 1,
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(
                countDown > 0
                    ? "$message (in ${countDown.toInt()} secs)"
                    : message,
                style: textStyle(false, 16, white),
                textAlign: TextAlign.center,
              ),
            ),
            GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Icon(
                    Icons.cancel,
                    color: white.withOpacity(.5),
                    size: 35,
                  )),
                ))
          ],
        ),
      ],
    );
  }

  closePage(onClosed) {
    showBack = false;
    setState(() {});
    Future.delayed(Duration(milliseconds: 100), () {
      Future.delayed(Duration(milliseconds: 200), () {
        hideUI = true;
        setState(() {});
      });
      onClosed();
    });
  }
}
