import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart';
import 'package:share_plus/share_plus.dart';
import 'package:social_share/social_share.dart';

///sharing platform
enum Share {
  facebook,
  twitter,
  whatsapp,
  whatsapp_personal,
  whatsapp_business,
  share_system,
  share_instagram,
  share_telegram
}

class DemoMe extends StatefulWidget {
  @override
  DemoMeState createState() => DemoMeState();
}

class DemoMeState extends State<DemoMe> {
  String text = '';
  String subject = '';
  List<String> imagePaths = [];
  List<Media> pickedFiles = [];
  String response = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Share Plugin Demo'),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextField(
                  decoration: const InputDecoration(
                    labelText: 'Share text:',
                    hintText: 'Enter some text and/or link to share',
                  ),
                  maxLines: 2,
                  onChanged: (String value) => setState(() {
                    text = value;
                  }),
                ),
                TextField(
                  decoration: const InputDecoration(
                    labelText: 'Share subject:',
                    hintText: 'Enter subject to share (optional)',
                  ),
                  maxLines: 2,
                  onChanged: (String value) => setState(() {
                    subject = value;
                  }),
                ),
                const Padding(padding: EdgeInsets.only(top: 12.0)),
                Previews(pickedFiles, onDelete: _onDeleteImage),
                ListTile(
                  leading: Icon(Icons.add),
                  title: Text("Open Gallery"),
                  onTap: () async {
                    final picks = await ImagesPicker.pick(
                        count: 10, pickType: PickType.all);

                    if (null == picks) return;

                    for (var p in picks) {
                      pickedFiles.add(p);
                    }
                    // pickedFiles = picks;
                    setState(() {});
                  },
                ),
                const Padding(padding: EdgeInsets.only(top: 12.0)),
                Row(
                  children: List.generate(Share.values.length, (index) {
                    final value = Share.values[index];
                    String data = describeEnum(value);

                    return Expanded(
                        child: ElevatedButton(
                      child: Text("Share to $data"),
                      onPressed: text.isEmpty && imagePaths.isEmpty
                          ? null
                          : () => _onShare(context, value),
                    ));
                  }),
                )
              ],
            ),
          ),
        ));
  }

  _onDeleteImage(int position) {
    setState(() {
      pickedFiles.removeAt(position);
      // imagePaths.removeAt(position);
    });
  }

  _onShare(BuildContext context, Share value) async {
    switch (value) {
      case Share.facebook:
        // TODO: Handle this case.

        break;
      case Share.twitter:
        // TODO: Handle this case.
        break;
      case Share.whatsapp:
        // TODO: Handle this case.
        break;
      case Share.whatsapp_personal:
        // TODO: Handle this case.
        break;
      case Share.whatsapp_business:
        // TODO: Handle this case.
        break;
      case Share.share_system:
        // TODO: Handle this case.
        break;
      case Share.share_instagram:
        // TODO: Handle this case.
        break;
      case Share.share_telegram:
        // TODO: Handle this case.
        break;
    }

    // A builder is used to retrieve the context immediately
    // surrounding the ElevatedButton.
    //
    // The context's `findRenderObject` returns the first
    // RenderObject in its descendent tree when it's not
    // a RenderObjectWidget. The ElevatedButton's RenderObject
    // has its position and size after it's built.
    // final RenderBox box = context.findRenderObject() as RenderBox;
    //
    // if (pickedFiles.isNotEmpty) {
    //   await Share.shareFiles(pickedFiles.map((e) => e.path).toList(),
    //       text: text,
    //       subject: subject,
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // } else {
    //   await Share.share(text,
    //       subject: subject,
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // }
  }
}

/// Widget for displaying a preview of images
class Previews extends StatelessWidget {
  /// The image paths of the displayed images
  final List<Media> pickedFiles;
  // final List<String> pickedFiles;

  /// Callback when an image should be removed
  final Function(int) onDelete;

  /// Creates a widget for preview of images. [pickedFiles] can not be empty
  /// and all contained paths need to be non empty.
  const Previews(this.pickedFiles, {Key key, this.onDelete}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (pickedFiles.isEmpty) {
      return Container();
    }

    List<Widget> imageWidgets = [];
    for (int i = 0; i < pickedFiles.length; i++) {
      imageWidgets.add(_ImagePreview(
        pickedFiles[i],
        onDelete: onDelete != null ? () => onDelete(i) : null,
      ));
    }

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: imageWidgets),
    );
  }
}

class _ImagePreview extends StatelessWidget {
  // final String media;
  final Media media;
  final VoidCallback onDelete;
  const _ImagePreview(this.media, {Key key, this.onDelete}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    File imageFile = File(media.thumbPath);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: 200,
              maxHeight: 200,
            ),
            child: Image.file(imageFile),
          ),
          Positioned(
            right: 0,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: Colors.red,
                      primary: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      minimumSize: Size(30, 30)),
                  child: Icon(
                    Icons.delete,
                    size: 18,
                  ),
                  onPressed: onDelete),
            ),
          ),
        ],
      ),
    );
  }
}
