import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/ShareDemo.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/controller.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share_plus/share_plus.dart';

import 'HomePage.dart';

class CreatePost extends StatefulWidget {
  @override
  _CreatePostState createState() => _CreatePostState();
}

class _CreatePostState extends State<CreatePost> {
  List get items => [
        {"icon": "assets/tabs/picture.png", "title": "Images"},
        {"icon": "assets/tabs/video-player.png", "title": "Videos"},
        {"icon": "assets/tabs/podcast.png", "title": "Audio"},
        {"icon": "assets/tabs/text.png", "title": "Text"},
      ];

  int activeIndex = -1;
  String text = '';
  final textControl = TextEditingController();
  String subject = '';
  List<Media> pickedFiles = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: transparent,
      child: Stack(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
          ),
          GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              margin: EdgeInsets.only(top: kTopHeight, bottom: 0),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xfffaffff),
                // backgroundColor: Color(0xfffaffff),

                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Column(
                children: [
                  addSpace(10),
                  Center(
                    child: Container(
                      width: 40,
                      height: 5,
                      decoration: BoxDecoration(
                        color: black.withOpacity(.09),
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  ),
                  addSpace(5),
                  Container(
                    // height: 80,
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(20),
                    child: Text(
                      "What would you like to post today?",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                          color: black),
                    ),
                  ),
                  addSpace(5),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      child: Builder(builder: (c) {
                        if (activeIndex == -1) {
                          return GridView.count(
                            crossAxisCount: 2,
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20,
                            children: List.generate(items.length, (p) {
                              bool active = activeIndex == p;
                              return GestureDetector(
                                onTap: () async {
                                  activeIndex = p;
                                  setState(() {});

                                  if (p < 2) {
                                    addImages();
                                    return;
                                  }
                                },
                                child: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        items[p]["icon"],
                                        fit: BoxFit.cover,
                                        height: 60,
                                        width: 60,
                                        color: active ? white : null,
                                      ),
                                      Text(
                                        items[p]["title"],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: active ? white : null,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: active ? Color(0xff023B7E) : white,
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: active
                                              ? white
                                              : Color(0xff023B7E)
                                                  .withOpacity(.09))),
                                ),
                              );
                            }),
                          );
                        }

                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (activeIndex == 3)
                              Container(
                                decoration: BoxDecoration(
                                    color: white,
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                        color: Color(0xff023B7E)
                                            .withOpacity(.09))),
                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: TextField(
                                  controller: textControl,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    labelText: 'Share text:',
                                    hintText:
                                        'Enter some text and/or link to share',
                                  ),
                                  maxLines: 2,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                            if (activeIndex < 2)
                              Previews(
                                pickedFiles,
                                onDelete: _onDeleteImage,
                              ),
                            Row(
                              children: [
                                if (pickedFiles.isNotEmpty ||
                                    textControl.text.isNotEmpty)
                                  if (activeIndex < 2)
                                    Container(
                                      height: 50,
                                      child: TextButton(
                                          onPressed: () {
                                            addImages();
                                          },
                                          child: Text("Add More!")),
                                    ),
                                if (pickedFiles.isNotEmpty ||
                                    textControl.text.isNotEmpty)
                                  Expanded(
                                    child: Container(
                                      height: 50,
                                      margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                                      child: TextButton(
                                        onPressed: () async {
                                          if (activeIndex == 3)
                                            _onShareWithEmptyOrigin(context);
                                          if (activeIndex < 2)
                                            _onShare(context);
                                        },
                                        child: Text("Share Content!"),
                                        style: TextButton.styleFrom(
                                            elevation: 8,
                                            backgroundColor: white,
                                            // backgroundColor: Color(0xff023B7E),
                                            primary: Color(0xff023B7E),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                            )),
                                      ),
                                    ),
                                  ),
                                TextButton(
                                    style: TextButton.styleFrom(
                                        elevation: 8,
                                        backgroundColor: Colors.red,
                                        primary: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                        minimumSize: Size(50, 50)),
                                    child: Icon(
                                      Icons.clear,
                                      size: 18,
                                    ),
                                    onPressed: () {
                                      activeIndex = -1;
                                      textControl.clear();
                                      pickedFiles.clear();

                                      setState(() {});
                                    })
                              ],
                            )
                          ],
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void addImages() async {
    final picks = await ImagesPicker.pick(
        count: 10,
        pickType: activeIndex == 0 ? PickType.image : PickType.video);
    if (null == picks) return;

    for (var p in picks) {
      pickedFiles.add(p);
    }
    // pickedFiles = picks;
    setState(() {});
  }

  _onDeleteImage(int position) {
    setState(() {
      pickedFiles.removeAt(position);
      // imagePaths.removeAt(position);
    });
  }

  _onShare(BuildContext context) async {
    // A builder is used to retrieve the context immediately
    // surrounding the ElevatedButton.
    //
    // The context's `findRenderObject` returns the first
    // RenderObject in its descendent tree when it's not
    // a RenderObjectWidget. The ElevatedButton's RenderObject
    // has its position and size after it's built.
    final RenderBox box = context.findRenderObject() as RenderBox;

    if (pickedFiles.isNotEmpty) {
      await Share.shareFiles(pickedFiles.map((e) => e.path).toList(),
          subject: subject,
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    } else {
      await Share.share(text,
          subject: subject,
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }
  }

  _onShareWithEmptyOrigin(BuildContext context) async {
    await Share.share(textControl.text);
  }

  @override
  Widget buildAppTemplate() {
    // TODO: implement buildAppTemplate
    throw UnimplementedError();
  }

  @override
  Widget buildWebTemplate() {
    // TODO: implement buildWebTemplate
    throw UnimplementedError();
  }
}
