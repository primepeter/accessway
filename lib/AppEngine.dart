import 'dart:async';
import 'package:Loopin/EdgeAlert.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart' as cachedLib;
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:package_info/package_info.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'package:Loopin/dialogs/inputDialog.dart';
import 'package:Loopin/dialogs/listDialog.dart';
import 'package:Loopin/dialogs/messageDialog.dart';
import 'package:Loopin/dialogs/progressDialog.dart';
import 'package:Loopin/main.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:mailto/mailto.dart';
import 'package:ntp/ntp.dart';
import 'package:path_provider/path_provider.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
// import 'package:permission_handler/permission_handler.dart' as pemLib;
import 'package:responsive_builder/responsive_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:permission_handler/permission_handler.dart';
//import 'package:photo_view/photo_view.dart';
//import 'package:photo_view/photo_view_gallery.dart';
import 'package:timeago/timeago.dart' as timeAgo;

import 'package:uuid/uuid.dart';

import 'package:flutter/gestures.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:clipboard_manager/clipboard_manager.dart';

import 'notificationService.dart';

final formatter = NumberFormat("#,###");
bool refreshPlan = false;
List AllHeadLineList = [];

SizedBox addSpace(double size) {
  return SizedBox(
    height: size,
  );
}

addSpaceWidth(double size) {
  return SizedBox(
    width: size,
  );
}

int getSeconds(String time) {
  List parts = time.split(":");
  int mins = int.parse(parts[0]) * 60;
  int secs = int.parse(parts[1]);
  return mins + secs;
}

String getTimerText(int seconds, {bool three = false}) {
  int hour = seconds ~/ Duration.secondsPerHour;
  int min = (seconds ~/ 60) % 60;
  int sec = seconds % 60;

  String h = hour.toString();
  String m = min.toString();
  String s = sec.toString();

  String hs = h.length == 1 ? "0$h" : h;
  String ms = m.length == 1 ? "0$m" : m;
  String ss = s.length == 1 ? "0$s" : s;

  return three ? "$hs:$ms:$ss" : "$ms:$ss";
}

Container addLine(
    double size, color, double left, double top, double right, double bottom) {
  return Container(
    height: size,
    width: double.infinity,
    color: color,
    margin: EdgeInsets.fromLTRB(left, top, right, bottom),
  );
}

Container bigButton(double height, double width, String text, textColor,
    buttonColor, onPressed) {
  return Container(
    height: height,
    width: width,
    child: RaisedButton(
      onPressed: onPressed,
      color: buttonColor,
      textColor: white,
      child: Text(
        text,
        style: TextStyle(
            fontSize: 20,
            fontFamily: "NirmalaB",
            fontWeight: FontWeight.normal,
            color: textColor),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    ),
  );
}

Container boxedText(text, int key, int keyHolder, Color normalColor,
    Color selectedColor, Color normalTextColor, Color selectedTextColor) {
  bool selected = key == keyHolder;
  return Container(
    height: 45,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: selected ? selectedColor : null,
        border: !selected
            ? Border.all(width: 1, color: normalColor, style: BorderStyle.solid)
            : null),
    child: Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          text,
          style: TextStyle(
              fontSize: 15,
              fontFamily: "NirmalaB",
              fontWeight: FontWeight.normal,
              color: selected ? selectedTextColor : normalTextColor),
        ),
      ),
    ),
  );
}

Future<File> loadFile(String path, String name) async {
  final ByteData data = await rootBundle.load(path);
  Directory tempDir = await getTemporaryDirectory();
  File tempFile = File('${tempDir.path}/$name');
  await tempFile.writeAsBytes(data.buffer.asUint8List(), flush: true);
  return tempFile;
}

textStyle(bool bold, double size, color,
    {underlined = false,
    bool withShadow = false,
    double shadowOffset = 4.0,
    bool crossed = false,
    int type = 0,
    double height,
    bool thick = false}) {
  return TextStyle(
      color: color,
      fontWeight: bold ? FontWeight.bold : null,
      height: height,

      // fontFamily: type == 0 && bold
      //     ? "NirmalaB"
      //     : type == 0
      //         ? "Nirmala"
      //         : type == 1 && bold
      //             ? "JosefinSans-Bold"
      //             : type == 2 && bold
      //                 ? "Sansation-Bold"
      //                 : type == 1
      //                     ? "JosefinSans-Regular"
      //                     : type == 2
      //                         ? "Sansation-Regular"
      //                         : "Arayara",
      fontSize: size,
      shadows: !withShadow
          ? null
          : (<Shadow>[
              Shadow(
                  offset: Offset(shadowOffset, shadowOffset),
                  blurRadius: 6.0,
                  color: black.withOpacity(.5)),
            ]),
      decoration: crossed
          ? TextDecoration.lineThrough
          : underlined
              ? TextDecoration.underline
              : TextDecoration.none);
}

ThemeData darkTheme() {
  final ThemeData base = ThemeData();
  return base.copyWith(hintColor: white);
}

placeHolder(double height, {double width = 200}) {
  return new Container(
    height: height,
    width: width,
    color: dark_green03.withOpacity(.1),
    child: Center(
        child: Opacity(
            opacity: .3,
            child: Image.asset(
              ic_launcher,
              width: 20,
              height: 20,
            ))),
  );
}

actionButton(String text, var icon, var color, onClicked) {
//  color = dark_green0;
  return Flexible(
    fit: FlexFit.tight,
    child: Container(
      height: 40,
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
      child: RaisedButton(
          onPressed: () {
            onClicked();
          },
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          elevation: 1,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25),
              side: BorderSide(color: white, width: 2)),
          color: transparent,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                  child: Text(
                text,
                maxLines: 1,
                style: textStyle(true, 14, white),
              )),
              addSpaceWidth(5),
              Icon(
                icon,
                size: 14,
                color: white,
              )
            ],
          )),
    ),
  );
}

getWarningWidget(BaseModel farmModel, margin) {
  return Container();
  /*if(farmModel.myItem())return Container();

  int verifyStatus = farmModel.getInt(VERIFY_STATUS);
  String warning = appSettingsModel.getString(
      verifyStatus == VERIFY_STATUS_TRUSTED ? WARNING_MESSAGE_TRUSTED :
      verifyStatus == VERIFY_STATUS_VERIFIED ? WARNING_MESSAGE_VERIFIED : WARNING_MESSAGE);
  if (warning.isEmpty) return Container();
  if (verifyStatus == VERIFY_STATUS_PENDING) {
    return tipBox(
        red0, warning, white,margin: margin);
  }
  if (verifyStatus == VERIFY_STATUS_VERIFIED)
    return tipBox(blue0, warning, white, icon: Icons.check_circle,
        margin:margin);

return tipBox(blue6, warning, white, icon: Icons.verified_user,
margin: margin);*/
}

tipBox(boxColor, String text, textColor, {icon, margin}) {
  return Container(
    //width: double.infinity,
    margin: margin,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: white,
        border: Border.all(color: boxColor, width: 1)),
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(shape: BoxShape.circle, color: boxColor),
            padding: EdgeInsets.all(5),
            child: Icon(
              icon != null ? icon : Icons.info,
              size: 14,
              color: white,
            ),
          ),
          addSpaceWidth(10),
          Flexible(
            flex: 1,
            child: Text(
              text,
              style: textStyle(true, 14, boxColor),
            ),
          )
        ],
      ),
    ),
  );
}

textBox(title, icon, mainText, tap) {
  return new Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        title,
        style: textStyle(false, 14, white.withOpacity(.5)),
      ),
      addSpace(10),
      new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Image.asset(
            icon,
            width: 14,
            height: 14,
            color: white,
          ),
          addSpaceWidth(15),
          Flexible(
            flex: 1,
            child: Column(
              children: <Widget>[
                new Container(
                  width: double.infinity,
                  child: InkWell(
                      onTap: tap,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Text(
                          mainText,
                          style: textStyle(false, 17, white),
                        ),
                      )),
                ),
                addLine(2, white, 0, 0, 0, 0),
              ],
            ),
          )
        ],
      ),
    ],
  );
}

Widget slideTransition(BuildContext context, Animation<double> animation,
    Animation<double> secondaryAnimation, Widget child) {
  var begin = Offset(1.0, 0.0);
  var end = Offset.zero;
  var tween = Tween(begin: begin, end: end);
  var offsetAnimation = animation.drive(tween);

  return SlideTransition(
    position: offsetAnimation,
    child: child,
  );
  ;
}

Widget fadeTransition(BuildContext context, Animation<double> animation,
    Animation<double> secondaryAnimation, Widget child) {
  return FadeTransition(
    opacity: animation,
    child: child,
  );
}

/*selectCurrency(context, result) {
  List<String> images = List();
  List<String> titlesMain = List.from(currenciesText);
  List<String> titles = List.from(currenciesText);

  titles.sort((s1, s2) => s1.compareTo(s2));

  for (String s in titles) {
    images.add(currencies[titlesMain.indexOf(s)]);
  }

  pushAndResult(
      context,
      listDialog(
        titles,
        title: "Choose Currency",
        images: images,
      ), result: (_) {
    String title = _;
    result(title);
  });
}*/

loadingLayout(
    {bool trans = false,
    bool loadWhite = false,
    double bottomMargin = 15,
    double height,
    bool center = true}) {
  return new Container(
    color: trans ? transparent : white,
    height: height,
    child: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        /*
        Center(
          child: CircularProgressIndicator(
            //value: 20,
            valueColor: AlwaysStoppedAnimation<Color>(trans?white:blue5),
            strokeWidth: 2,
          ),
        ),*/
        Center(
            child: Container(
                width: 90,
                height: 90,
                child: LoadingIndicator(
                  indicatorType: Indicator.ballBeat,
                  color: appColor,
                ))),
        Center(
          child: Opacity(
            opacity: 1,
            child: Image.asset(
              "assets/icons/lucro_small.png",
              width: 30,
              height: 30,
              color: white,
              // color: loadWhite ? gold : white,
            ),
          ),
        ),
      ],
    ),
  );
}

errorDialog(retry, cancel, {String text}) {
  return Stack(
    fit: StackFit.expand,
    children: <Widget>[
      Container(
        color: black.withOpacity(.8),
      ),
      Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Center(
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        color: red0,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                          child: Text(
                        "!",
                        style: textStyle(true, 30, white),
                      ))),
                  addSpace(10),
                  Text(
                    "Error",
                    style: textStyle(false, 14, red0),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              text == null ? "An unexpected error occurred, try again" : text,
              style: textStyle(false, 14, white.withOpacity(.5)),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      )),
      Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: new Container(),
            flex: 1,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: FlatButton(
                      onPressed: retry,
                      child: Text(
                        "RETRY",
                        style: textStyle(true, 15, white),
                      )),
                ),
                addSpace(15),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: FlatButton(
                      onPressed: cancel,
                      child: Text(
                        "CANCEL",
                        style: textStyle(true, 15, white),
                      )),
                ),
              ],
            ),
          )
        ],
      ),
    ],
  );
}

addExpanded() {
  return Expanded(
    child: new Container(),
    flex: 1,
  );
}

addFlexible() {
  return Flexible(
    child: new Container(),
    flex: 1,
  );
}

emptyLayout(icon, String title, String text,
    {color = default_white,
    iconColor = black,
    click,
    clickText,
    double height,
    double bottomMargin = 15}) {
  return Container(
    height: height,
    child: Center(
      child: Container(
        margin: EdgeInsets.fromLTRB(20, 15, 20, bottomMargin),
        padding: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
            // color: black.withOpacity(.02),
            color: white,
            boxShadow: [
              BoxShadow(
                  color: black.withOpacity(.05), blurRadius: 5, spreadRadius: 2)
            ],
            border: Border.all(
              color: black.withOpacity(.02),
            ),
            borderRadius: BorderRadius.circular(10)),
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 80,
              height: 80,
              child: Stack(
                children: <Widget>[
                  new Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                        color: Color(0XFF0B123E).withOpacity(1),
                        // color: appColor.withOpacity(1),
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: appColor.withOpacity(.05), width: 4)),
                  ),
                  new Center(
                      child: !(icon is String)
                          ? Icon(
                              icon,
                              size: 30,
                              color: iconColor,
                            )
                          : Image.asset(
                              icon,
                              height: 40,
                              width: 40,
                              // color: iconColor,
                            )),
                ],
              ),
            ),
            addSpace(10),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (title.isNotEmpty)
                  Container(
                    child: Text(
                      title,
                      style: textStyle(true, 18, black),
                      textAlign: TextAlign.center,
                    ),
//                  padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
//                  decoration: BoxDecoration(
//                    color: default_white,
//                    borderRadius: BorderRadius.circular(25)
//                  ),
                  ),
//                if(click!=null)GestureDetector(
//                  onTap: (){
//                    click();
//                  },
//                  child: Container(
//                      padding: EdgeInsets.all(5),
//                      decoration: BoxDecoration(color: red6,shape: BoxShape.circle)
//                      ,child: Icon(Icons.refresh,color: white,size: 14,)),
//                )
              ],
            ),
            if (text.isNotEmpty && title.isNotEmpty) addSpace(5),
            if (text.isNotEmpty)
              Flexible(
                child: Text(
                  text,
                  style: textStyle(false, 15, black.withOpacity(.5)),
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
//            if(click!=null)addSpace(10),
            if (click != null)
              Flexible(
                child: Container(
                  height: 30,
                  margin: EdgeInsets.only(top: 10),
                  child: FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                            10), //side: BorderSide(width: 2,color:black)
                      ),
                      color: gold,
                      onPressed: click,
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: Text(
                        clickText,
                        style: textStyle(true, 15, white),
                      )),
                ),
              )
          ],
        ),
      ),
    ),
  );
}

List<String> getFromList(String key, List<BaseModel> models,
    {String sortKey, bool sortWithNumber = true}) {
  List<String> list = new List();
  //List<BaseModel> models = new List();

  if (sortKey != null) {
    models.sort((b1, b2) {
      if (sortWithNumber) {
        int a = b1.getInt(sortKey);
        int b = b2.getInt(sortKey);
        return a.compareTo(b);
      }
      String a = b1.getString(sortKey);
      String b = b2.getString(sortKey);
      return a.compareTo(b);
    });
  }

  for (BaseModel bm in models) {
    list.add(bm.getString(key));
  }

  return list;
}

int memberStatus(List list) {
  for (Map map in list) {
    BaseModel bm = new BaseModel(items: map);
    if (bm.getObjectId() == (userModel.getObjectId())) {
      if (bm.getBoolean(GROUP_ADMIN)) return ADMIN_MEMBER;
      return MEMBER;
    }
  }

  return NOT_MEMBER;
}

//bool levelHasLoaded = false;
//Map<String, BaseModel> levelList = new Map();
//
//bool schoolHasLoaded = false;
//Map<String, BaseModel> schoolList = new Map();
//
//bool studyHasLoaded;
//Map<String, BaseModel> studyList = new Map();

/*List<BaseModel> schoolList = new List();
List<BaseModel> studyList = new List();
List<BaseModel> levelList = new List();*/

/*void listenFromFire(String dBase, onLoaded) {
  FirebaseFirestore.instance.collection(dBase).snapshots().listen((shots) {
    List<BaseModel> list = List();
    for (DocumentSnapshot d in shots.docs) {
      BaseModel model = BaseModel(doc: d);
      list.add(model);
    }
    onLoaded(list);
  });
}*/

pushSheet(context, item,
    {result,
    bool isScrollControlled = true,
    bool isDismissible = true,
    bool enableDrag = true}) {
  showModalBottomSheet(
    isScrollControlled: isScrollControlled,
    backgroundColor: transparent,
    isDismissible: isDismissible,
    enableDrag: enableDrag,
    // backgroundColor: black.withOpacity(0),
    context: context,
    builder: (context) {
      return item;
    },
  ).then((_) {
    if (_ != null) {
      if (result != null) result(_);
    }
  });
}

pushAndResult(context, item,
    {
    //context,
    result,
    opaque = false,
    bool replace = false,
    bool clear = false,
    bool fade = false,
    transitionBuilder,
    transitionDuration}) {
  print(null == context);
  //return;

  if (fade) transitionBuilder = fadeTransition;

  if (clear) {
    Navigator.of(context)
        .pushAndRemoveUntil(
            new PageRouteBuilder(
                transitionsBuilder: transitionBuilder ?? slideTransition,
                transitionDuration:
                    transitionDuration ?? Duration(milliseconds: 300),
                opaque: opaque,
                pageBuilder: (BuildContext context, _, __) {
                  return item;
                }),
            (Route<dynamic> route) => false)
        .then((_) {
      if (_ != null) {
        if (result != null) result(_);
      }
    });
    return;
  }

  if (replace) {
    Navigator.pushReplacement(
        context ?? baseContext,
        PageRouteBuilder(
            transitionsBuilder: transitionBuilder ?? slideTransition,
            transitionDuration:
                transitionDuration ?? Duration(milliseconds: 300),
            opaque: opaque,
            pageBuilder: (context, _, __) {
              return item;
            })).then((_) {
      if (_ != null) {
        if (result != null) result(_);
      }
    });
    return;
  }
  Navigator.push(
      context ?? baseContext,
      PageRouteBuilder(
          transitionsBuilder: transitionBuilder ?? slideTransition,
          transitionDuration: transitionDuration ?? Duration(milliseconds: 300),
          opaque: opaque,
          pageBuilder: (context, _, __) {
            return item;
          })).then((_) {
    if (_ != null) {
      if (result != null) result(_);
    }
  });
}

pushAndResultX(context, item,
    {result,
    opaque = false,
    bool replace = false,
    transitionBuilder,
    transitionDuration}) {
  if (replace) {
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(
            transitionsBuilder: transitionBuilder ?? slideTransition,
            transitionDuration:
                transitionDuration ?? Duration(milliseconds: 300),
            opaque: opaque,
            pageBuilder: (context, _, __) {
              return item;
            })).then((_) {
      if (_ != null) {
        if (result != null) result(_);
      }
    });
    return;
  }
  Navigator.push(
      context,
      PageRouteBuilder(
          transitionsBuilder: transitionBuilder ?? slideTransition,
          transitionDuration: transitionDuration ?? Duration(milliseconds: 300),
          opaque: opaque,
          pageBuilder: (context, _, __) {
            return item;
          })).then((_) {
    if (_ != null) {
      if (result != null) result(_);
    }
  });

//  checkInternet();
//  checkPlanDaysLeft();
}

pushToNamedScreen(context, String name,
    {args,
    result,
    opaque = false,
    bool replace = false,
    transitionBuilder,
    transitionDuration}) {
  if (replace) {
    Navigator.pushReplacementNamed(context, name, arguments: args).then(
      (_) {
        if (_ != null) {
          if (result != null) result(_);
        }
      },
    );
    return;
  }
  Navigator.pushNamed(context, name, arguments: args).then((_) {
    if (_ != null) {
      if (result != null) result(_);
    }
  });

//  checkInternet();
//  checkPlanDaysLeft();
}

String getRandomId() {
  var uuid = new Uuid();
  return uuid.v1();
}

String getRandomIdShort() {
  var uuid = new Uuid();
  String id = uuid.v1();
  List parts = id.split("-");
  StringBuffer sb = StringBuffer();
  for (int i = 0; i < parts.length; i++) {
    String part = parts[i];
    String newPart =
        "${part.substring(0, 1)}${part.substring(part.length - 1, part.length)}";
    sb.write(newPart);
  }
  List passList = [];
  String sbString = sb.toString();
  for (int i = 0; i < sb.length; i++) {
    passList.add(sbString[i]);
  }
  passList.shuffle();
  StringBuffer newPassword = StringBuffer();
  for (String s in passList) {
    newPassword.write(s);
  }
  return newPassword.toString().trim();
}

/*
void loadListFromFire(String dBase, {onLoaded(error, result)}) {
  FirebaseFirestore.instance.collection(dBase).get().then((shots) {
    List<BaseModel> list = List();

    for (DocumentSnapshot d in shots.docs) {
      BaseModel model = BaseModel(doc: d);
      list.add(model);
    }

    if (onLoaded != null) onLoaded(null, list);
  }).catchError((error) {
    if (onLoaded != null) onLoaded(error, null);
  });
}*/

String getCountryCode(context) {
  return Localizations.localeOf(context).countryCode;
}

//String getDeviceId() {}
String getExtImage(String fileExtension) {
  if (fileExtension == null) return "";
  fileExtension = fileExtension.toLowerCase().trim();
  if (fileExtension.contains("doc")) {
    return icon_file_doc;
  } else if (fileExtension.contains("pdf")) {
    return icon_file_pdf;
  } else if (fileExtension.contains("xls")) {
    return icon_file_xls;
  } else if (fileExtension.contains("ppt")) {
    return icon_file_ppt;
  } else if (fileExtension.contains("txt")) {
    return icon_file_text;
  } else if (fileExtension.contains("zip")) {
    return icon_file_zip;
  } else if (fileExtension.contains("xml")) {
    return icon_file_xml;
  } else if (fileExtension.contains("png") ||
      fileExtension.contains("jpg") ||
      fileExtension.contains("jpeg")) {
    return icon_file_photo;
  } else if (fileExtension.contains("mp4") ||
      fileExtension.contains("3gp") ||
      fileExtension.contains("mpeg") ||
      fileExtension.contains("avi")) {
    return icon_file_video;
  } else if (fileExtension.contains("mp3") ||
      fileExtension.contains("m4a") ||
      fileExtension.contains("m4p")) {
    return icon_file_audio;
  }

  return icon_file_unknown;
}

uploadFile(File file, onComplete(res, error)) async {
//  FirebaseFirestore fb = FirebaseFirestore.instance;
//  fb.app.storage().refFromURL("YOUR BUCKET URL HERE eg: 'gs://project-ID.appspot.com'");

  /*if(webApp){
    String mimeType = mime(path.basename(file.path));
    final extension = extensionFromMime(mimeType);
    var metadata = fb.UploadMetadata(
      contentType: mimeType,
    );

    fb.StorageReference ref = fb
        .app()
        .storage()
        .refFromURL('gs://splitam-b3664.appspot.com')
        .child("images/_${DateTime.now().millisecondsSinceEpoch}.${extension}");

    bool exists = await file.exists();
    print("Upload Path: ${file.path} exists:${exists}");

    fb.UploadTask uploadTask = ref.put(file, metadata);

    print("Ext: $extension, Mime: $mimeType");
    return;


    fb.UploadTaskSnapshot taskSnapshot = await uploadTask.future;


    taskSnapshot.ref.getDownloadURL().then((_) {
      BaseModel model = new BaseModel();
      model.put(FILE_URL, _.toString());
      model.put(REFERENCE, ref);
      model.saveItem(REFERENCE_BASE, false);

      onComplete(_.toString(), null);
    }, onError: (error) {
      onComplete(null, error);
    });


    return;
  }*/

  final String ref = getRandomId();
  var storageReference = FirebaseStorage.instance.ref().child(ref);
  var uploadTask = storageReference.putFile(
    file,
  );
  uploadTask.then((task) {
    if (task != null) {
      task.ref.getDownloadURL().then((_) {
        BaseModel model = new BaseModel();
        model.put(FILE_URL, _.toString());
        model.put(REFERENCE, ref);
        model.saveItem(REFERENCE_BASE, false);

        onComplete(_.toString(), null);
      }, onError: (error) {
        onComplete(null, error);
      });
    }
  }, onError: (error) {
    onComplete(null, error);
  });
}

Future<String> uploadFileTask(file,
    {bool isVideo = false, String contentType}) async {
  try {
    final String ref = getRandomId();
    var storageReference = FirebaseStorage.instance.ref().child(ref);

    UploadTask uploadTask;

    if (kIsWeb) {
      final metadata = SettableMetadata(
          contentType: null != contentType
              ? contentType
              : isVideo
                  ? 'video/mp4'
                  : 'image/jpeg',
          customMetadata: {'picked-file-path': file.path});
      uploadTask = storageReference.putData(await file.readAsBytes(), metadata);
    } else {
      uploadTask = storageReference.putFile(
        file,
      );
    }
    final task = await uploadTask;
    final _ = await task.ref.getDownloadURL();
    BaseModel model = new BaseModel();
    model.put(FILE_URL, _.toString());
    model.put(REFERENCE, ref);
    model.saveItem(REFERENCE_BASE, false);
    return _;
  } catch (error) {
    throw (error);
  }
}

Future<bool> isConnected() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('connected');
      return Future<bool>.value(true);
    }
  } on SocketException catch (_) {
    print('not connected');
    return Future<bool>.value(false);
  }

  return Future<bool>.value(false);
}

void showProgress(bool show, BuildContext context,
    {String msg, bool cancellable = true}) {
  if (!show) {
    progressDialogShowing = false;
    progressController.add(false);
    return;
  }
  progressDialogShowing = true;
  pushAndResult(
      context,
      progressDialog(
        message: msg,
        cancelable: cancellable,
      ),
      // fade: true,
      transitionBuilder: scaleTransition,
      transitionDuration: Duration(milliseconds: 800));
}

void showMessage(context, icon, iconColor, title, message,
    {int delayInMilli = 0,
    clickYesText = "OK",
    onClicked,
    clickNoText,
    bool cancellable = true,
    double iconPadding,
    bool = true}) {
  Future.delayed(Duration(milliseconds: delayInMilli), () {
    pushAndResult(
        context,
        messageDialog(
          icon,
          iconColor,
          title,
          message,
          clickYesText,
          noText: clickNoText,
          cancellable: cancellable,
          iconPadding: iconPadding,
        ),
        result: onClicked,
        opaque: false,
        transitionBuilder: scaleTransition,
        transitionDuration: Duration(milliseconds: 500));
  });
}

Widget scaleTransition(
  BuildContext context,
  Animation<double> animation,
  Animation<double> secondaryAnimation,
  Widget child,
) {
  return ScaleTransition(
    scale: Tween<double>(
      begin: 2.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      ),
    ),
    child: child,
  );
}

showErrorDialog(context, String message,
    {onOkClicked,
    bool cancellable = true,
    String okText = "OK",
    int delay = 800}) {
  showMessage(context, Icons.error, red0, "Oops!", message,
      delayInMilli: delay, cancellable: cancellable, onClicked: (_) {
    if (_ == true) {
      if (onOkClicked != null) onOkClicked();
    }
  }, clickYesText: okText, clickNoText: okText == "Retry" ? "Cancel" : null);
}

bool isEmailValid(String email) {
  if (!email.contains("@") ||
      !email.contains(".") ||
      !email.endsWith("@accessbankplc.com")) return false;
  return true;
}

gradientLine(
    {double height = 4,
    color = black,
    bool reverse = false,
    double alpha = .3}) {
  return Container(
    width: double.infinity,
    height: height,
    decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: reverse
                ? [
                    color.withOpacity(alpha),
                    transparent,
                  ]
                : [transparent, color.withOpacity(alpha)])),
  );
}

int getAge(int date) {
  int now = DateTime.now().millisecondsSinceEpoch;
  int diff = now - date;
  Duration duration = Duration(milliseconds: diff);
  int age = duration.inDays ~/ 365;
  return age;
}

String formatDOB(int v) {
  if (v < 10) return "0$v";
  return "$v";
}

String getBirthDay(int time) {
  DateTime date = DateTime.fromMillisecondsSinceEpoch(time);
  int year = date.year;
  int month = date.month;
  int day = date.day;
  return "$year-${formatDOB(month)}-${formatDOB(day)}";
}

openLink(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  }
}

void yesNoDialog(
  context,
  title,
  message,
  clickedYes, {
  int delayInMilli = 0,
  bool cancellable = true,
  color = red0,
  clickYesText = "Yes",
}) {
  Future.delayed(Duration(milliseconds: delayInMilli), () {
    // pushAndResult(
    //     context,
    //     messageDialog(
    //       icon,
    //       iconColor,
    //       title,
    //       message,
    //       clickYesText,
    //       noText: clickNoText,
    //       cancellable: cancellable,
    //       iconPadding: iconPadding,
    //     ),
    //     result: onClicked,
    //     opaque: false,
    //     transitionBuilder: scaleTransition,
    //     transitionDuration: Duration(milliseconds: 500));

    Navigator.push(
        context,
        PageRouteBuilder(
            transitionsBuilder: scaleTransition,
            opaque: false,
            pageBuilder: (context, _, __) {
              return messageDialog(
                Icons.error,
                color,
                title,
                message,
                clickYesText,
                noText: "Cancel",
                cancellable: cancellable,
              );
            })).then((_) {
      if (_ != null) {
        if (_ == true) {
          clickedYes();
        }
      }
    });
  });
}

String formatToK(num) {
  return NumberFormat.compactCurrency(decimalDigits: 0, symbol: "")
      .format(num is String ? int.parse(num) : num);
}

formatPrice(String price) {
  if (price.contains("000000")) {
    price = price.replaceAll("000000", "");
    price = "${price}M";
  } else if (price.length > 6) {
    double pr = (int.parse(price)) / 1000000;
    return "${pr.toStringAsFixed(1)}M";
  } else if (price.contains("000")) {
    price = price.replaceAll("000", "");
    price = "${price}K";
  } else if (price.length > 3) {
    double pr = (int.parse(price)) / 1000;
    return "${pr.toStringAsFixed(1)}K";
  }
  return price;
}

replyThis(
  context,
  BaseModel mainComment,
  BaseModel comment,
  onEdited,
) {
  pushAndResult(
      context, inputDialog("Reply", hint: "Write a reply...", okText: "SEND"),
      result: (_) {
    BaseModel model = new BaseModel();
    model.put(MESSAGE, _);
    model.put(ITEM_ID, comment.getString(ITEM_ID));
    model.put(COMMENT_ID, mainComment.getObjectId());
    model.saveItem(COMMENT_BASE, true);
    Future.delayed(Duration(seconds: 1), () {
      onEdited();

      createNotification(
          receiverIds: [comment.getUserId()],
          message: "replied your comment\n\"$_\"",
          type: "Reply${comment.getObjectId()}",
          itemId: mainComment.getString(ITEM_ID),
          itemDB: POST_BASE,
          commentId: comment.getObjectId());
//      createNotification([comment.getUserId()], "replied your comment", comment,
//          ITEM_TYPE_COMMENT,
//          user: userModel, id: "${comment.getObjectId()}");
//      pushToPerson(comment, "replied your comment");
    });
  }, transitionBuilder: fadeTransition);
}

showCommentOptions(context, BaseModel mainComment, BaseModel model, onEdited,
    onDeleted, bool myPost, bool isReply) {
  List<String> options = List();
  if (isAdmin) options.add(model.getBoolean(HIDDEN) ? "Unhide" : "Hide");
  if (isAdmin || model.myItem()) {
    options.addAll(["Edit", "Copy", "Delete"]);
  } else if (myPost) {
    options.addAll(["Copy", "Delete"]);
  } else {
    options.addAll(["Copy"]);
  }
  pushAndResult(context, listDialog(options), result: (_) {
    if (_ == "Hide") {
      yesNoDialog(context, "Hide Comment?",
          "Are you sure you want to hide this comment?", () {
        model.put(HIDDEN, true);
        onEdited();
      });
    } else if (_ == "Unhide") {
      model.put(HIDDEN, false);
      onEdited();
    } else if (_ == "Reply") {
      replyThis(
        context,
        mainComment,
        model,
        onEdited(),
      );
    } else if (_ == "Edit") {
      pushAndResult(
          context,
          inputDialog("Edit Comment",
              message: model.getString(MESSAGE),
              hint: "Comment...",
              okText: "UPDATE"), result: (_) {
        model.put(MESSAGE, _.toString());
        model.updateItems();
        onEdited();
      });
    } else if (_ == "Delete") {
      yesNoDialog(
          context, "Delete?", "Are you sure you want to delete this comment?",
          () {
        model.deleteItem();
        /*commentsList
            .removeWhere((bm) => bm.getObjectId() == model.getObjectId());*/
        onDeleted(model);
      });
    } else if (_ == "Copy") {
//      ClipboardManager.copyToClipBoard(model.getString(MESSAGE));
    }
  });
}

void uploadItem(StreamController<String> uploadingController,
    String uploadingText, String successText, BaseModel model,
    {BaseModel listExtras, onComplete}) {
  List keysToUpload = model.getList(FILES_TO_UPLOAD);
  if (keysToUpload.isEmpty) {
    model.saveItem(model.getString(DATABASE_NAME), true,
        document: model.getObjectId(), onComplete: (e) {
      if (successText != null) {
        uploadingController.add(successText);
        Future.delayed(Duration(seconds: 2), () {
          uploadingController.add(null);
        });
      }
      if (onComplete != null) onComplete();
    });
    return;
  }

  if (uploadingText != null) uploadingController.add(uploadingText);

  String key = keysToUpload[0];
  var item = model.get(key);

  if (item is List) {
    uploadItemFiles(item, List(), (res, error) {
      if (error != null) {
        uploadItem(uploadingController, uploadingText, successText, model,
            listExtras: listExtras, onComplete: onComplete);
        return;
      }
      if (listExtras != null) {
        List ext = List.from(listExtras.getList(key));
        //List ext = List.from(extraImages);
        ext.addAll(res);
        model.put(key, ext);
      } else {
        model.put(key, res);
      }
      keysToUpload.removeAt(0);
      model.put(FILES_TO_UPLOAD, keysToUpload);
      uploadItem(uploadingController, uploadingText, successText, model,
          listExtras: listExtras, onComplete: onComplete);
    });
  } else {
    List list = List();
    list.add(item);
    uploadItemFiles(list, List(), (res, error) {
      if (error != null) {
        uploadItem(uploadingController, uploadingText, successText, model,
            listExtras: listExtras, onComplete: onComplete);
        return;
      }
      List urls = res;
      model.put(key, urls[0].toString());
      keysToUpload.removeAt(0);
      model.put(FILES_TO_UPLOAD, keysToUpload);
      uploadItem(uploadingController, uploadingText, successText, model,
          listExtras: listExtras, onComplete: onComplete);
    });
  }
}

uploadItemFiles(List files, List urls, onComplete) {
  if (files.isEmpty) {
    onComplete(urls, null);
    return;
  }
  var item = files[0];
  var file = item is String ? File(item) : item;
  uploadFile(file, (res, error) {
    if (error != null) {
      onComplete(null, error);
      return;
    }

    files.removeAt(0);
    urls.add(res.toString());
    uploadItemFiles(files, urls, onComplete);
  });
}

Future<String> get localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> getLocalFile(String name) async {
  final path = await localPath;
  return File('$path/$name');
}

Future<File> getDirFile(String name) async {
  final dir = await getExternalStorageDirectory();
  var testDir =
      await Directory("${dir.path}/Loopin Material").create(recursive: true);
  return File("${testDir.path}/$name");
}

String formatDuration(Duration position) {
  final ms = position.inMilliseconds;

  int seconds = ms ~/ 1000;
  final int hours = seconds ~/ 3600;
  seconds = seconds % 3600;
  var minutes = seconds ~/ 60;
  seconds = seconds % 60;

  final hoursString = hours >= 10
      ? '$hours'
      : hours == 0
          ? '00'
          : '0$hours';

  final minutesString = minutes >= 10
      ? '$minutes'
      : minutes == 0
          ? '00'
          : '0$minutes';

  final secondsString = seconds >= 10
      ? '$seconds'
      : seconds == 0
          ? '00'
          : '0$seconds';

  final formattedTime =
      '${hoursString == '00' ? '' : hoursString + ':'}$minutesString:$secondsString';

  return formattedTime;
}

int getPositionForLetter(String text) {
  return az.indexOf(text.toUpperCase());
}

String az = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
String getLetterForPosition(int position) {
  return az.substring(position, position + 1);
}

String convertListToString(String divider, List list) {
  StringBuffer sb = new StringBuffer();
  for (int i = 0; i < list.length; i++) {
    String s = list[i];
    sb.write(s);
    if (divider != ",") sb.write(" ");
    if (i != list.length - 1) sb.write(divider);
    sb.write(" ");
  }

  return sb.toString().trim();
}

List<String> convertStringToList(String divider, String text) {
  if (text.trim().isEmpty) return [];
  List<String> list = new List();
  var parts = text.split(divider);
  for (String s in parts) {
    list.add(s.trim());
  }
  return list;
}

class ReadMoreText extends StatefulWidget {
  String text;
  bool full;
  var toggle;
  int minLength;
  double fontSize;
  var textColor;
  var moreColor;
  bool center;
  bool canExpand;

  ReadMoreText(
    this.text, {
    this.full = false,
    this.minLength = 150,
    this.fontSize = 14,
    this.toggle,
    this.textColor = black,
    this.moreColor = red0,
    this.center = false,
    this.canExpand = true,
  });

  @override
  _ReadMoreTextState createState() => _ReadMoreTextState();
}

class _ReadMoreTextState extends State<ReadMoreText> {
  bool expanded;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    expanded = widget.full;
  }

  @override
  Widget build(BuildContext context) {
    return text();
  }

  text() {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
              text: widget.text.length <= widget.minLength
                  ? widget.text
                  : expanded
                      ? widget.text
                      : (widget.text.substring(0, widget.minLength)),
              style: textStyle(false, widget.fontSize, widget.textColor)),
          TextSpan(
              text: widget.text.length < widget.minLength || expanded
                  ? ""
                  : "...",
              style: textStyle(false, widget.fontSize, black)),
          TextSpan(
            text: widget.text.length < widget.minLength
                ? ""
                : expanded
                    ? " Read Less"
                    : "Read More",
            style: textStyle(true, widget.fontSize - 2, widget.moreColor,
                underlined: false),
            recognizer: new TapGestureRecognizer()
              ..onTap = () {
                setState(() {
                  if (widget.canExpand) expanded = !expanded;
                  if (widget.toggle != null) widget.toggle(expanded);
                });
              },
          )
        ],
      ),
      textAlign: widget.center ? TextAlign.center : TextAlign.left,
    );
  }
}

createReport(context, BaseModel theModel, int type, String message) {
  BaseModel report = BaseModel();
  report.put(MESSAGE, message);
  report.put(THE_MODEL, theModel.items);
  report.put(REPORT_TYPE, type);
  report.put(STATUS, STATUS_UNDONE);
  report.saveItem(REPORT_BASE, true);
  NotificationService.sendPush(
      topic: 'admin',
      title: 'Pending',
      tag: 'reports',
      body: 'Some reports are pending');
  showMessage(context, Icons.report, dark_green03, "Report Sent",
      "Thank you for submitting a report, we will review this and take neccessary actions",
      cancellable: true, delayInMilli: 500);
}

void createNotification(
    {@required List receiverIds,
    @required String message,
    @required String itemId,
    String itemDB,
    @required String type,
    String commentId}) {
  String id = "${itemId}$type";
  FirebaseFirestore.instance
      .collection(NOTIFY_BASE)
      .doc(id)
      .get(GetOptions(source: Source.server))
      .then((_) {
    //toastInAndroid(_.toString());
    BaseModel model = BaseModel(doc: _);
    List people = model.getList(PEOPLE);
    int p = people.indexWhere(
      (map) => map[USER_ID] == userModel.getObjectId(),
    );
    //toastInAndroid(p.toString());
    if (p == -1) {
      model.put(PARTIES, receiverIds);
//      model.put(TITLE, title);
      model.put(MESSAGE, message);
      model.put(ITEM_ID, itemId);
      model.put(COMMENT_ID, commentId);
      model.put(ITEM_DB, itemDB);

      Map thePerson = Map();
      thePerson[USER_ID] = userModel.getObjectId();
      thePerson[USER_IMAGE] = userModel.getString(USER_IMAGE);
      thePerson[FIRST_NAME] = userModel.getString(FIRST_NAME);
      thePerson[LAST_NAME] = userModel.getString(LAST_NAME);

      people.add(thePerson);
      model.put(PEOPLE, people);
      model.remove(READ_BY);

      if (!_.exists) {
        model.saveItem(NOTIFY_BASE, true, document: id);
      } else {
        model.updateItems();
      }
    }
  }, onError: (_) async {
    //toastInAndroid(_.toString());
    await Future.delayed(Duration(seconds: 5));
    createNotification(
        receiverIds: receiverIds,
        message: message,
        type: type,
        itemId: itemId,
        itemDB: itemDB);
  });
}

tipMessageItem(String title, String message) {
  return Container(
    //width: 300,
    //height: 300,
    child: new Card(
        color: red03,
        elevation: .5,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: new Padding(
          padding: const EdgeInsets.fromLTRB(8, 8, 8, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.info,
                    size: 14,
                    color: white,
                  ),
                  addSpaceWidth(5),
                  Text(
                    title,
                    style: textStyle(true, 12, white.withOpacity(.5)),
                  ),
                ],
              ),
              addSpace(5),
              Text(
                message,
                style: textStyle(false, 16, white),
                //overflow: TextOverflow.ellipsis,
              ),
              /*Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(3)),
                child: Text(
                  "APPLY",
                  style: textStyle(true, 9, black),
                ),
              ),*/
            ],
          ),
        )),
  );
}

niceButton(double width, text, click,
    {icon, String image = "", bool selected = false, bool tintImage = false}) {
  //bool selected = image != null;
  return new Container(
    width: width,
    height: 40,
    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: new FlatButton(
        padding: EdgeInsets.all(0),
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        shape: RoundedRectangleBorder(
            side: BorderSide(color: dark_green0, width: 2),
            borderRadius: BorderRadius.circular(25)),
        color: selected ? dark_green0 : transparent,
        onPressed: click,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            addSpaceWidth(15),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Text(
                text,
                style: textStyle(true, 14, selected ? white : dark_green0),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            addSpaceWidth(10),
            new Container(
                margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    color: selected ? white : dark_green0,
                    shape: BoxShape.circle),
                child: Center(
                    child: selected
                        ? Image.asset(
                            image,
                            width: 12,
                            height: 12,
                            color: tintImage ? dark_green03 : null,
                          )
                        : Icon(
                            icon,
                            color: white,
                            size: 12,
                          ))),
          ],
        )),
  );
}

placeCall(String phone) {
  openLink("tel://$phone");
}

openWhatsapp(context, String phone, {String text = ""}) async {
  var whatsappUrl = "whatsapp://send?phone=$phone&text=$text";
  await canLaunch(whatsappUrl)
      ? launch(whatsappUrl)
      : showErrorDialog(context, "Whatsapp not found");
}

sendEmail(String email, {String subject, String body}) async {
  openLink("mailto:$email");
  final mailtoLink = Mailto(
    to: [email],
    subject: subject,
    body: body,
  );
  // Convert the Mailto instance into a string.
  // Use either Dart's string interpolation
  // or the toString() method.
  await launch('$mailtoLink');
}

//List<BaseModel> levelList = List();

smallButton(icon, text, clicked) {
  return new Container(
    height: 40,
    child: new FlatButton(
        padding: EdgeInsets.all(0),
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        color: blue09,
        onPressed: clicked,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            addSpaceWidth(10),
            Center(
                child: Icon(
              icon,
              color: dark_green03,
              size: 14,
            )),
            addSpaceWidth(5),
            Text(
              text,
              style: textStyle(true, 12, dark_green03),
              maxLines: 1,
            ),
            addSpaceWidth(12),
          ],
        )),
  );
}

pickCountry(context, onPicked(Country country)) {
  // pushAndResult(context, SelectCountry(),result: (_){
  //   onPicked(_);
  // },opaque: false);
}

List<String> getSearchString(String text) {
  text = text.toLowerCase().trim();
  text = text.replaceAll(",", " ");
  if (text.trim().isEmpty) return List();

  List<String> list = List();
  list.add(text);
  var parts = text.split(" ");
  for (String s in parts) {
    if (s.isNotEmpty && !list.contains(s)) list.add(s);
    for (int i = 0; i < s.length; i++) {
      String sub = s.substring(0, i);
      if (sub.isNotEmpty && !list.contains(sub)) list.add(sub);
    }
  }
  for (int i = 0; i < text.length; i++) {
    String sub = text.substring(0, i);
    if (sub.isNotEmpty && !list.contains(sub)) list.add(sub.trim());
  }
  String longText = text.replaceAll(" ", "");
  for (int i = 0; i < longText.length; i++) {
    String sub = longText.substring(0, i);
    if (sub.isNotEmpty && !list.contains(sub)) list.add(sub.trim());
  }
  return list;
}

filterItem(icon, String text, onClicked, {String prefix}) {
  return Container(
    height: 35,
    margin: EdgeInsets.fromLTRB(10, 10, 0, 10),
    child: FlatButton(
      onPressed: () {
        onClicked();
      },
//        elevation: .5,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      color: transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          side: BorderSide(color: getBlackColor(), width: 1)),
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (icon != null)
            icon is String
                ? (Image.asset(
                    icon,
                    width: 15,
                    height: 15,
                    color: getBlackColor(),
                  ))
                : Icon(
                    icon,
                    size: 15,
                    color: getBlackColor(),
                  ),
          if (prefix != null && icon != null) addSpaceWidth(5),
          if (prefix != null)
            Text(
              prefix,
              style: textStyle(false, 12, getBlackColor().withOpacity(.7)),
            ),
          addSpaceWidth(3),
          Text(
            text,
            style: textStyle(true, 11, getBlackColor()),
          ),
        ],
      ),
    ),
  );
}

launchLink(String key) {
  String link = appSettingsModel.getString(key);
  if (link.isEmpty) return;
  openLink(link);
}

rateApp() async {
  if (webApp) return;
  SharedPreferences shed = await SharedPreferences.getInstance();
  String link = appSettingsModel
      .getString(Platform.isAndroid ? PLAYSTORE_LINK : APPLESTORE_LINK);
  if (link.isEmpty) return;
  shed.setBool(HAS_RATED, true);
  openLink(link);
}

labelWidget(icon, String text, double iconSize, {bool showLine = true}) {
  if (text.isEmpty) return Container();
  return Container(
    //height: 30,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                  color: blue09,
                  shape: BoxShape.circle,
                  border: Border.all(color: black.withOpacity(.1), width: .5)),
              child: Center(
                child: !(icon is String)
                    ? Icon(icon, size: iconSize, color: black.withOpacity(.5))
                    : Image.asset(
                        icon,
                        width: iconSize,
                        height: iconSize,
                        color: black.withOpacity(.5),
                      ),
              ),
            ),
            addSpaceWidth(5),
            Flexible(
              child: Text(
                text,
                style: textStyle(false, 12, black.withOpacity(.4)),
              ),
            )
          ],
        ),
        !showLine
            ? Container()
            : addLine(.5, black.withOpacity(.1), 25, 0, 0, 0),
      ],
    ),
  );
}

handleSplittingx(produceId, {@required bool enable}) async {
  DocumentSnapshot doc = await FirebaseFirestore.instance
      .collection(PRODUCT_BASE)
      .doc(produceId)
      .get(GetOptions(source: Source.server));

  if (doc == null) return;
  doc.reference.update({SPLITTABLE: enable});
}

createChatNotice(String chatId, String message, {String key = ""}) {
  BaseModel model = BaseModel();
  model.put(MESSAGE, message);
  model.put(CHAT_ID, chatId);
  model.saveItem(CHAT_BASE, false, document: "${userModel.getObjectId()}$key");
}

String createChatId(String hisId) {
  String myId = userModel.getObjectId();
  List ids = [];
  for (int i = 0; i < myId.length; i++) {
    ids.add(myId[i]);
  }
  for (int i = 0; i < hisId.length; i++) {
    ids.add(hisId[i]);
  }
  ids.sort((a, b) => a.compareTo(b));
  StringBuffer sb = StringBuffer();
  for (String s in ids) {
    sb.write(s);
  }
  return sb.toString().trim();
}

bool isSameDay(int time1, int time2) {
  DateTime date1 = DateTime.fromMillisecondsSinceEpoch(time1);

  DateTime date2 = DateTime.fromMillisecondsSinceEpoch(time2);

  return (date1.day == date2.day) &&
      (date1.month == date2.month) &&
      (date1.year == date2.year);
}

bool chatRemoved(BaseModel chat) {
  if (chat.getBoolean(DELETED)) {
    return true;
  }
  if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
    return true;
  }
  return false;
}

String parseTimestamp(String timestamp) {
  DateTime dt = DateTime.parse(timestamp);
  return getChatDate(dt.millisecondsSinceEpoch);
  return timeAgo.format(dt);
}

String getDateFormat(int milli, String format) {
  final formatter = DateFormat(format);
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

String getPaybackDate(int milli) {
  final formatter = DateFormat("EEEE MMMM d, yyyy");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

String getChatDate(var time) {
  // final formatter = DateFormat("EEE MMM d 'at' h:mm a");
  final formatter = DateFormat("EEE MMM d, h:mm a");
  if (time is DateTime) return formatter.format(time);
  if (time is Timestamp) {
    Timestamp timestamp = time;
    return formatter.format(
        DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch));
  }
  DateTime date = DateTime.fromMillisecondsSinceEpoch(time);
  return formatter.format(date);
}

String getTransDate(int milli) {
  final formatter = DateFormat("M/d/y\nh:mm a");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

String getBvnDate(int milli) {
  final formatter = DateFormat("d-MMM-yyyy");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

String getDateOfBirth(int milli) {
  final formatter = DateFormat("d MMM, yyyy");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

String getChatTime(int milli) {
  final formatter = DateFormat("h:mm a");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

tabIndicator(int tabCount, int currentPosition, {margin}) {
  return Container(
    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
    margin: margin,
    decoration: BoxDecoration(
        color: black.withOpacity(.7), borderRadius: BorderRadius.circular(25)),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: getTabs(tabCount, currentPosition),
    ),
  );
  /*return Marker(
      markerId: MarkerId(""),
      infoWindow: InfoWindow(),
      icon: await BitmapDescriptor.fromAssetImage(ImageConfiguration(), ""));*/
}

getTabs(int count, int cp) {
  List<Widget> items = List();
  for (int i = 0; i < count; i++) {
    bool selected = i == cp;
    items.add(Flexible(
      fit: FlexFit.loose,
      child: Container(
        width: selected ? 10 : 8,
        height: selected ? 10 : 8,
        //margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
        decoration: BoxDecoration(
            color: white.withOpacity(selected ? 1 : (.5)),
            shape: BoxShape.circle),
      ),
    ));
    if (i != count - 1) items.add(addSpaceWidth(5));
  }

  return items;
}

String getExtToUse(String fileExtension) {
  if (fileExtension == null) return "";
  fileExtension = fileExtension.toLowerCase().trim();
  if (fileExtension.contains("doc")) {
    return "doc";
  } else if (fileExtension.contains("xls")) {
    return "xls";
  } else if (fileExtension.contains("ppt")) {
    return "ppt";
  }

  return fileExtension;
}

class RaisedGradientButton extends StatelessWidget {
  final Widget child;
  final Gradient gradient;
  final double width;
  final double height;
  final Function onPressed;
  final bool round;
  final bool addShadow;

  const RaisedGradientButton(
      {Key key,
      @required this.child,
      this.gradient,
      this.width = double.infinity,
      this.height = 50.0,
      this.onPressed,
      this.addShadow = true,
      this.round = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          gradient: gradient,
          borderRadius: round ? null : BorderRadius.circular(25),
          boxShadow: !addShadow
              ? null
              : [
                  BoxShadow(
                    color: Colors.grey[500],
                    offset: Offset(0.0, 1.5),
                    blurRadius: 1.5,
                  ),
                ],
          shape: round ? BoxShape.circle : BoxShape.rectangle),
      child: Material(
        color: Colors.transparent,
        child: FlatButton(
            shape: round
                ? CircleBorder()
                : RoundedRectangleBorder(
                    borderRadius: round ? null : BorderRadius.circular(25),
                  ),
            color: Colors.transparent,
            onPressed: onPressed,
            padding: EdgeInsets.all(0),
            child: Center(
              child: child,
            )),
      ),
    );
  }
}

smallTitle(String text,
    {buttonIcon = Icons.search, String buttonText, onButtonClicked}) {
  return Container(
    margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
    child: Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Text(
            text,
            style: textStyle(true, 14, black.withOpacity(.5)),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        buttonText == null
            ? Container()
            : new Container(
                //width: 50,
                //margin: EdgeInsets.fromLTRB(5, 0, 5, 5),
                height: 25,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side:
                          BorderSide(color: black.withOpacity(.1), width: .5)),
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  onPressed: onButtonClicked,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        buttonIcon,
                        size: 12,
                        color: black.withOpacity(.4),
                      ),
                      addSpaceWidth(2),
                      Text(
                        buttonText,
                        style: textStyle(true, 10, black.withOpacity(.4)),
                      ),
                    ],
                  ),
                  color: blue09,
                ),
              )
      ],
    ),
  );
}

checkSummary(String courseId, String matType) async {
  DocumentSnapshot doc = await FirebaseFirestore.instance
      .collection(STUDY_BASE)
      .doc(courseId)
      .get();
  BaseModel bm = BaseModel(doc: doc);
  if (bm.getString(SUMMARY).isEmpty) {
    bm.put(SUMMARY, "1 $matType");
    bm.updateItems();
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 10.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}

String getLastSeen(BaseModel user, {bool nullIfNotOnline = false}) {
  int time = user.getInt(TIME_UPDATED);
  int now = DateTime.now().millisecondsSinceEpoch;
  int diff = now - time;

  bool online = isOnline(user);
  if (nullIfNotOnline && !online) return null;
  if (online) return "Online";
  if (diff > (Duration.millisecondsPerDay * 15)) return null;
  return diff > (Duration.millisecondsPerDay * 14)
      ? "Last seen: some days ago"
      : "Last seen: ${timeAgo.format(DateTime.fromMillisecondsSinceEpoch(time), locale: "en")}";
}

getFlag(BaseModel personModel, {bool small = false}) {
  if (personModel.getString(COUNTRY).isEmpty) return Container();
  return Card(
    clipBehavior: Clip.antiAlias,
    elevation: 0,
    shape: RoundedRectangleBorder(
        side: BorderSide(color: white, width: 1),
        borderRadius: BorderRadius.circular(5)),
    child: Image.asset(
      CountryPickerUtils.getFlagImageAssetPath(personModel.getString(COUNTRY)),
      height: small ? 12 : 16.0,
      width: small ? 17 : 25.0,
      fit: BoxFit.fill,
      package: "country_pickers",
    ),
  );
}

clickLogout(context, {onComplete}) {
  yesNoDialog(context, "Logout?", "Are you sure you want to logout?", () async {
    showProgress(true, context, msg: "Logging Out");
    userModel.put(IS_ONLINE, false);
    userModel.updateItems();
    await FirebaseAuth.instance.signOut();
    // await googleSignIn.signOut();
    Future.delayed(Duration(seconds: 3), () {
      userModel = BaseModel();
      isLoggedIn = false;
      showProgress(false, context);
      if (onComplete != null) onComplete();
      Future.delayed(Duration(seconds: 1), () {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (c) {
          // return AppSetter();
        }));
      });
    });
  });
}

nameItem(String title, String text,
    {color: black, bool center = false, bool paddBottom = true}) {
  return Container(
    margin: EdgeInsets.only(bottom: paddBottom ? 10 : 0),
    child: RichText(
      text: TextSpan(children: [
        TextSpan(text: title, style: textStyle(true, 13, color)),
        TextSpan(
            text: "  ", style: textStyle(false, 14, color.withOpacity(.5))),
        TextSpan(
            text: "$text", style: textStyle(false, 14, color.withOpacity(.5)))
      ]),
      textAlign: center ? TextAlign.center : TextAlign.left,
    ),
  );
}

checkBox(bool selected,
    {double size: 13,
    checkColor = blue3,
    iconColor = white,
    boxColor = blue09}) {
  return new Container(
    //padding: EdgeInsets.all(2),
    child: Container(
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: boxColor,
          border: Border.all(color: black.withOpacity(.1), width: 2)),
      child: Container(
        width: size,
        height: size,
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: selected ? checkColor : transparent,
        ),
        child: Icon(
          Icons.check,
          size: size - 5,
          color: !selected ? transparent : iconColor,
        ),
      ),
    ),
  );
}

showListDialog(context, List items, onSelected,
    {title,
    images,
    bool useTint = true,
    selections,
    bool returnIndex = false,
    bool singleSelection = false}) {
  pushAndResult(
      context,
      listDialog(
        items,
        title: title,
        images: images,
        useTint: useTint,
        selections: selections,
        singleSelection: singleSelection,
      ), result: (_) {
    if (_ is List) {
      onSelected(_);
    } else {
      onSelected(returnIndex ? items.indexOf(_) : _);
    }
  },
      opaque: false,
      transitionBuilder: scaleTransition,
      transitionDuration: Duration(milliseconds: 800));
}

//abstract class OnListItemSelected{
//  onSelected({int position,List selections});
//}

int getTodayMilli() {
  DateTime now = DateTime.now();
  return DateTime(now.year, now.month, now.day).millisecondsSinceEpoch;
}

int getWeekMilli() {
  DateTime now = DateTime.now();
  return DateTime(now.year, now.month, now.weekday).millisecondsSinceEpoch;
}

int getMonthMilli() {
  DateTime now = DateTime.now();
  return DateTime(
    now.year,
    now.month,
  ).millisecondsSinceEpoch;
}

int getYearMilli() {
  DateTime now = DateTime.now();
  return DateTime(
    now.year,
  ).millisecondsSinceEpoch;
}

String getOutreachPlaces(List soulList) {
  List list = [];
  for (BaseModel bm in soulList) {
    String mapName = bm.getString(MAP_NAME);
    if (!list.contains(mapName)) list.add(mapName);
  }
  return convertListToString(",", list);
}

String getAges(List soulList) {
  int minAge = 100000;
  int maxAge = 0;
  for (BaseModel bm in soulList) {
    int age = bm.getInt(SOUL_AGE);
    minAge = age < minAge ? age : minAge;
    maxAge = age > maxAge ? age : maxAge;
  }
  return "$minAge - $maxAge";
}

int getMilestoneCount(List soulList, String key) {
  int count = 0;
  for (BaseModel bm in soulList) {
    if (bm.getInt(key) != 0) count++;
  }
  return count;
}

bool nameValid(String name) {
  int sameCount = 0;
  String prevText = "";
  for (int i = 0; i < name.length; i++) {
    String s = name[i].toLowerCase();
    if (prevText.isEmpty) {
      prevText = s;
      continue;
    }
    if (s == prevText) {
      sameCount++;
      if (sameCount > 2) return false;
    } else {
      sameCount = 0;
    }
  }

  return true;
}

showSnack(GlobalKey<ScaffoldState> key, String text, {bool useLoopin = false}) {
  key.currentState
      .showSnackBar(getSnack(key.currentContext, text, useLoopin: useLoopin));
}

SnackBar getSnack(context, String text, {bool useLoopin = false}) {
  return SnackBar(
    content: Text(
      text,
      style: textStyle(false, 16, white),
      textAlign: TextAlign.center,
    ),
    backgroundColor: !useLoopin ? black : dark_green0,
    duration: Duration(seconds: 3),
  );
}

getLoopinColor() {
  return blue0;
}

getLoopinColor2() {
  return blue3;
}

Widget getAssetImage(String asset) {
  return Image.asset(
    asset,
    height: 30.0,
    width: 30.0,
    color: Colors.amber,
  );
}

bool passwordVisible = false;
textbox(TextEditingController controller, String hint,
    {int lines = 1,
    bool isName = false,
    focusNode,
    bool isPass = false,
    refresh,
    maxLength,
    bool center = true,
    onChanged}) {
  return Container(
    margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
    padding: EdgeInsets.fromLTRB(isPass ? 40 : 10, 0, 10, 0),
    height: lines > 1 ? null : 50,
    decoration: BoxDecoration(
        border: Border.all(color: black.withOpacity(.1), width: 1),
        borderRadius: BorderRadius.circular(5),
        color: blue09),
    child: new TextField(
      controller: controller,
      textInputAction:
          lines > 1 ? TextInputAction.newline : TextInputAction.done,
      focusNode: focusNode,
      decoration: InputDecoration(
          hintText: hint,
          isDense: true,
          suffix: !isPass
              ? null
              : GestureDetector(
                  onTap: () {
                    passwordVisible = !passwordVisible;
                    if (refresh != null) refresh();
                  },
                  child: Text(
                    passwordVisible ? "HIDE" : "SHOW",
                    style: textStyle(false, 12, black.withOpacity(.5)),
                  )),
          hintStyle: textStyle(
            false,
            22,
            black.withOpacity(.35),
          ),
          border: InputBorder.none),
      textAlign: center ? TextAlign.center : TextAlign.left,
      style: textStyle(
        false,
        22,
        black,
      ),
      maxLength: isName ? 30 : null,
      cursorColor: black, obscureText: isPass && !passwordVisible,
      onChanged: onChanged,
      //maxLength: 200,
      cursorWidth: 1,
      minLines: lines, maxLines: lines,
    ),
  );
}

textboxTv(String text, String hint, onTap) {
  return GestureDetector(
    onTap: () {
      onTap();
    },
    child: Container(
      margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      height: 50,
      decoration: BoxDecoration(
          border: Border.all(color: black.withOpacity(.1), width: 1),
          borderRadius: BorderRadius.circular(5),
          color: blue09),
      child: Center(
        child: Text(text.isEmpty ? hint : text,
            style: textStyle(
              false,
              22,
              black.withOpacity(text.isNotEmpty ? (1) : .35),
            )),
      ),
    ),
  );
}

String getFullName(item) {
//  return "${bm.getString(NAME)}";
  BaseModel bm;
  if (item is Map) {
    bm = BaseModel(items: item);
  } else {
    bm = item;
  }
  return "${bm.getString(FIRST_NAME)} ${bm.getString(LAST_NAME)} ";
}

Map createLoveMap(BaseModel user) {
  Map myMap = Map();
  myMap[TIME] = DateTime.now().millisecondsSinceEpoch;
  myMap[OBJECT_ID] = user.getUserId();
  myMap[STATUS] = PENDING;
  return myMap;
}

/*bool hasLove(BaseModel user,String userId){
  List lovers = user.getList(LOVE_LIST);
  return (lovers.indexWhere((map)=>map[OBJECT_ID]==userId))!=-1;
}*/

getTheOtherId(
  BaseModel chatSetupModel,
) {
  List parties = chatSetupModel.getList(PARTIES);
  parties.remove(userModel.getObjectId());
  return parties[0];
}

String getProfileKey(BaseModel user) {
  return "${user.getString(USER_ID)}${user.getInt(PROFILE_UPDATED)}";
}

bool isBlocked(model, {String userId}) {
  if (userId != null && userId == userModel.getObjectId()) return false;
//  List allBlocked = [];
//  allBlocked.addAll(List.from(userModel.getList(BLOCKED)));
//  allBlocked.addAll(blockedIds);
  if (userId != null) {
    if (userId.isNotEmpty && blockedIds.contains(userId)) return true;
    return false;
  }

  String oId = model.getObjectId();
  String uId = model.getString(USER_ID);
  String dId = model.getString(DEVICE_ID);
  if (oId.isNotEmpty && blockedIds.contains(oId)) return true;
  if (uId.isNotEmpty && blockedIds.contains(uId)) return true;
  if (dId.isNotEmpty && blockedIds.contains(dId)) return true;

//  List parties = model.getList(PARTIES);
//  for(String id in parties){
//    if(blockedIds.contains(id))return true;
//  }

  List blocked = model.getList(BLOCKED);
  if (blocked.contains(userModel.getObjectId())) {
    if (!blockedIds.contains(uId)) blockedIds.add(uId);
    return true;
  }
  if (userModel.getString(DEVICE_ID).isNotEmpty &&
      blocked.contains(userModel.getString(DEVICE_ID))) {
    if (!blockedIds.contains(uId)) blockedIds.add(uId);
    return true;
  }

  return false;
}

updateSettingsItem(context, String title, String key,
    {@required bool isNumber, allowEmpty}) {
  pushAndResult(
      context,
      inputDialog(
        title,
        hint: title,
        inputType: isNumber ? TextInputType.number : TextInputType.text,
        message: !isNumber
            ? (appSettingsModel.getString(key))
            : appSettingsModel.getInt(key).toString(),
        allowEmpty: allowEmpty,
      ), result: (_) {
    appSettingsModel.put(key, !isNumber ? (_.trim()) : int.parse(_.trim()));
    appSettingsModel.updateItems();
  });
}

Color getColorForKey(String key) {
  if (key == RED) return red0;
  if (key == GREEN) return light_green3;
  if (key == BROWN) return brown0;
  if (key == DARK_GREEN) return dark_green0;
  if (key == ORANGE) return orange3;
  if (key == DARK_BLUE) return blue4;
  return red01;
}

double screenWidth(context) {
  return MediaQuery.of(context).size.width;
}

double screenHeight(context) {
  return MediaQuery.of(context).size.height;
}

onlineDot() {
  return Container(
    width: 10,
    height: 10,
    margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      border: Border.all(color: white, width: 2),
      color: red0,
    ),
  );
}

//getFullname(BaseModel personModel){
//  return "${personModel.getString(FIRST_NAME)} ${personModel.getString(LAST_NAME)}";
//}

bool isOnline(BaseModel user) {
  int now = DateTime.now().millisecondsSinceEpoch;
  int lastUpdated = user.getInt(TIME_UPDATED);
  bool notOnline =
      ((now - lastUpdated) > (Duration.millisecondsPerMinute * 10));
  return user.getBoolean(IS_ONLINE) && (!notOnline);
}

deleteFileOnline(String url) async {
  QuerySnapshot shots = await FirebaseFirestore.instance
      .collection(REFERENCE_BASE)
      .where(FILE_URL, isEqualTo: url)
      .limit(1)
      .get();

  for (DocumentSnapshot doc in shots.docs) {
    BaseModel model = BaseModel(doc: doc);
    String ref = model.getString(REFERENCE);
    var storageReference = FirebaseStorage.instance.ref().child(ref);
    storageReference.delete();
  }
}

getScreenHeight(context) {
  return MediaQuery.of(context).size.height;
}

getScreenWidth(context) {
  return MediaQuery.of(context).size.width;
}

String oldNumber = "";

clickText(String title, String text, onClicked,
    {icon,
    String hint = "",
    double height: 55,
    bool centralize = false,
    bool padBottom = true,
    double marginTop = 20,
    double marginBottom = 0,
    Widget addChild,
    clickHelp}) {
  text = text ?? "";
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
//      if(text.isNotEmpty)Text(
//        title,
//        style: textStyle(true, 14, dark_green03),
//      ),
//      if(text.isNotEmpty)addSpace(10),
      GestureDetector(
        onTap: () {
          if (onClicked != null) onClicked();
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(
              0, marginTop, 0, padBottom ? marginBottom : 0),
          child: Stack(
            children: [
              Container(
                height: 55,
                width: double.infinity,
                padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
                decoration: BoxDecoration(
//              color: blue09,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    // borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: gold, width: 1)),
                child: Center(
                  child: Row(
                    children: <Widget>[
                      if (icon != null)
                        Icon(
                          icon,
                          size: 18,
                          color: getBlackColor().withOpacity(.5),
                        ),
                      if (icon != null) addSpaceWidth(15),
                      Expanded(
                        child: new Text(
                          text.isNotEmpty ? text : hint,
                          style: textStyle(
                              false,
                              16,
                              text.isEmpty
                                  ? getBlackColor().withOpacity(.3)
                                  : getBlackColor()),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      if (null != addChild) addChild
                    ],
                  ),
                ),
              ),
              if (title.isNotEmpty)
                Align(
                  alignment:
                      centralize ? Alignment.topCenter : Alignment.topLeft,
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                        decoration: BoxDecoration(
                          color: gold,
//                       borderRadius: BorderRadius.circular(10),
//               border: Border.all(color: gold,width: 1),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(0),
                          ),
//
                        ),
                        child: Text(
                          title,
                          style: textStyle(false, 12, Colors.white),
                          maxLines: 1,
                        ),
                      ),
                      Spacer(),
                      if (null != clickHelp)
                        Container(
                          //margin: EdgeInsets.only(left: 2),
                          child: FlatButton(
                              onPressed: () {
                                clickHelp();
                              },
                              height: 20,
                              minWidth: 20,
                              padding: EdgeInsets.zero,
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              shape: CircleBorder(),
                              color: black.withOpacity(.09),
                              child: Icon(
                                Icons.help_outline,
                                size: 15,
                                // color: black,
                                color: black.withOpacity(.6),
                              )),
                        )
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
    ],
  );
}

String formatAmount(var text, {int decimal = 0, bool useK = false}) {
  if (text == null) return "0.00";
  text = text.toString();
  if (text.toString().trim().isEmpty) return "0.00";
  text = text.replaceAll(",", "");
  try {
    text = double.parse(text).toStringAsFixed(decimal);
  } catch (e) {}
  RegExp reg = new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
  Function mathFunc = (Match match) => '${match[1]},';

  String val = text.replaceAllMapped(reg, mathFunc);
  // if (useK && formatPrice(text).toString().contains("M"))
  //   return formatPrice(text);

  return val;
}

clickTextDate(context, String title, String hint, int time, onChanged(int time),
    {minTime, maxTime}) {
  return clickText(title, time == 0 ? "" : getDateOfBirth(time), () {
    bool empty = time == 0;

    int year;
    int month;
    int day;
    if (!empty) {
      year = DateTime.fromMillisecondsSinceEpoch(time).year;
      month = DateTime.fromMillisecondsSinceEpoch(time).month;
      day = DateTime.fromMillisecondsSinceEpoch(time).day;
    }

    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: minTime,
      maxTime: maxTime,
      onChanged: (date) {},
      onConfirm: (date) {
        onChanged(date.millisecondsSinceEpoch);
        //final f = new DateFormat('dd-MM-yyyy');
        //getBvnDate(date.millisecond);
        // onChanged(DateFormat('dd-MM-yyyy').format(date));
      },
      currentTime: empty ? null : DateTime(year, month, day),
    );
  }, hint: hint);
}

// checkLocation(context, onComplete(BaseModel locModel),
//     {bool silently = false}) async {
//   locationLib.Location location = new locationLib.Location();
//   var serviceEnabled = await location.serviceEnabled();
//   if (!serviceEnabled) {
//     location.requestService().then((value) {
//       if (value) {
//         checkLocation(context, onComplete);
//       } else {
//         onComplete(null);
//       }
//     });
//     return;
//   }
//   var permissionGranted = await pemLib.Permission.location.isGranted;
//   print("Granted $permissionGranted");
//   if (!permissionGranted) {
//     pemLib.Permission.location.request().then((pemLib.PermissionStatus value) {
//       if (value.isGranted) {
//         checkLocation(context, onComplete);
//       } else {
//         onComplete(null);
//       }
//     });
//     return;
//   }
//   if (!silently) showProgress(true, context, msg: "Detecting Location..");
//   setUpLocation((_) {
//     Future.delayed(Duration(milliseconds: 500), () async {
//       showProgress(false, context);
// //      String state = _.getString(STATE);
// //      String city = _.getString(CITY);
// //      print("$state $city");
//       await Future.delayed(Duration(milliseconds: 500));
//       onComplete(_);
//     });
//   });
// }

setUpLocation(onComplete(BaseModel locModel)) async {
//  print("Setting up location...");
//  Location location = new Location();
//  var locationData = await location.getLocation();
//  Geoflutterfire geo = Geoflutterfire();
//
//  var myLocation = geo.point(
//      latitude: locationData.latitude, longitude: locationData.longitude);
//
//  //Placemark.
//  final placemark = await Geolocator()
//      .placemarkFromCoordinates(myLocation.latitude, myLocation.longitude);
//
////    print(
////        "PlaceMarker ${placemark[0].toJson()}  Lat ${myLocation.latitude} Long ${myLocation.longitude}");
//
//  BaseModel locModel = BaseModel();
//  if(placemark!=null && placemark.isNotEmpty) {
//    Map loc = placemark[0].toJson();
//    String placeName = loc["name"] ?? "";
//    String state = loc["administrativeArea"] ?? "";
//    String subState = loc["subAdministrativeArea"] ?? "";
//    String city = loc["locality"] ?? "";
//    String city1 = loc["subLocality"] ?? "";
//    String city3 = loc["thoroughfare"] ?? "";
//    String country = loc["country"] ?? "";
//
//    city = subState.isNotEmpty?subState:city;
//    locModel.put(ADDRESS, placeName);
////    locModel.put(STATE, state);
//    locModel.put(CITY, city);
//    locModel.put(COUNTRY, country);
//    locModel.put(LATITUDE, locationData.latitude);
//    locModel.put(LONGITUDE, locationData.longitude);
//  }
//    onComplete(locModel);
}

bool isLocalFile(String path) {
  if (path == null) return false;
  if (path.startsWith("http://") || path.startsWith("https://")) return false;
  return true;
}

String getFirstImage(BaseModel model) {
  List medias = model.getList(IMAGES);
  String firstImage = "";
  if (medias.isNotEmpty) {
    Map media = medias[0];
    bool isVideo = media[IS_VIDEO] ?? "";
    firstImage = media[isVideo ? THUMBNAIL_PATH : PATH];
  }
  return firstImage;
}

optionItem(
    context,
    String text,
    int index,
    onEditted(
  bool removed,
),
    {String subTitle = ""}) {
//  int index = optionsIndexes.indexWhere((s)=>s==text);

  return Container(
    key: ValueKey(text),
    width: double.infinity,
//    height: 40,
    margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
    child: Card(
      margin: EdgeInsets.all(0),
      clipBehavior: Clip.antiAlias,
      elevation: 0,
      shape: RoundedRectangleBorder(
          side: BorderSide(color: black.withOpacity(.1), width: 2),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      color: white,
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    text,
                    style: textStyle(false, 16, black),
                  ),
                  if (subTitle.isNotEmpty) addSpace(6),
                  if (subTitle.isNotEmpty)
                    Text(
                      subTitle,
                      style: textStyle(false, 12, black.withOpacity(.5)),
                    ),
                ],
              ),
            ),
            addSpaceWidth(10),
            GestureDetector(
                onTap: () {
                  yesNoDialog(context, "Remove Item?",
                      "Are you sure you want to remove this item?", () {
//                        optionsIndexes.removeAt(index);
                    onEditted(true);
                  }, cancellable: true);
                },
                child: Icon(
                  Icons.cancel,
                  size: 20,
                  color: red0,
                ))
          ],
        ),
      ),
    ),
  );
}

String createPhoneNumer(String phonePref, String phone) {
  phone = phone.startsWith("0") ? phone.substring(1) : phone;
  phone = "+$phonePref$phone";
  return phone;
}

String getCurrencyLogo(String country) {
  String currencyLogo = "";
  List countryList = appSettingsModel.getList(COUNTRY_LIST);
  for (Map m in countryList) {
    if (m[COUNTRY_NAME] == country) {
      currencyLogo = m[CURRENCY_LOGO];
    }
  }
  return currencyLogo;
}

String getCurrencyText(String country) {
  String currency = "";
  List countryList = appSettingsModel.getList(COUNTRY_LIST);
  for (Map m in countryList) {
    if (m[COUNTRY_NAME] == country) {
      currency = m[CURRENCY];
    }
  }
  return currency;
}

double getAmountInLocalCurrency(String country, double amount) {
  List countryList = appSettingsModel.getList(COUNTRY_LIST);
  double usdValue = 0;
  double myUsdValue = 0;
  for (Map m in countryList) {
    if (m[COUNTRY_NAME] == country) {
      usdValue = m[VALUE_TO_ONE_DOLLAR];
    }
    if (m[COUNTRY_NAME] == currentCountry) {
      myUsdValue = m[VALUE_TO_ONE_DOLLAR];
    }
  }
  double realValue = amount / usdValue;
  double myValue = myUsdValue * realValue;

  return myValue;
}

Future<bool> checkLocalFile(String name) async {
  final path = await localPath;
  File file = File('$path/$name');
  return await file.exists();
}

getDefaultLine() {
  return addLine(.5, black.withOpacity(.1), 0, 0, 0, 0);
}

updateMySearches(List searchs) {
  if (!isLoggedIn) return;
  List mySearch = userModel.getList(ALL_SEARCHES);
  bool updated = false;
  for (String s in searchs) {
    if (mySearch.contains(s.toLowerCase())) continue;
    updated = true;
    mySearch.add(s.toLowerCase());
  }
  if (updated) {
    userModel.put(ALL_SEARCHES, mySearch);
    userModel.updateItems();
  }
}

checkError(context, e, {bool indexErrorOnly = false}) {
  String error = e.toString();
  print("The error >>> $error <<");
  if (error.toLowerCase().contains("index")) {
    String link = error.substring(error.indexOf("https://"), error.length);
    showMessage(context, Icons.error, blue0, "Index Needed", link,
        clickYesText: "Create Index", onClicked: (_) {
      if (_ == true) {
        openLink(link);
      }
    });
  } else {
    if (!indexErrorOnly)
      showErrorDialog(
        context,
        e.toString(),
      );
  }
}

openMap(lat, lon) {
  openLink("https://www.google.com/maps/search/?api=1&query=$lat,$lon");
}

checkButton(String title, bool on, onToggle) {
  return GestureDetector(
    onTap: () {
      onToggle();
    },
    child: Container(
      height: 35, color: transparent,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
//                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
//                                decoration: BoxDecoration(
//                                    border: Border.all(color: blue0, width: 2),color: transparent,
//                                    borderRadius: BorderRadius.circular(25)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: 20,
            height: 20,
            decoration: BoxDecoration(
                color: on ? light_green3 : blue09,
                border: Border.all(color: light_green3, width: 1),
                shape: BoxShape.circle),
            child: on
                ? Center(
                    child: Icon(
                    Icons.check,
                    color: white,
                    size: 15,
                  ))
                : Container(),
          ),
          addSpaceWidth(10),
          Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Text(
                title,
                style: textStyle(true, 16, light_green3),
              )),
        ],
      ),
    ),
  );
}

updateStats(String key,
    {@required String userId,
    @required String itemId,
    @required String databaseName,
    int increment = 1}) async {
  if (userId == userModel.getObjectId()) return;
  DocumentReference doc =
      FirebaseFirestore.instance.collection(STATS_BASE).doc(itemId);
  doc.update({
    "$key${getTodayMilli()}": FieldValue.increment(increment)
  }).catchError((e) {
    if (e.toString().toLowerCase().contains("found")) {
      doc.set({
        TIME: DateTime.now().millisecondsSinceEpoch,
        USER_ID: userId,
        DATABASE_NAME: databaseName,
        "$key${getTodayMilli()}": FieldValue.increment(increment)
      });
    }
  });
}

int getStatsInfo(List statsList, String statsKey,
    {bool todayOnly = false, String db}) {
  int total = 0;
  for (BaseModel model in statsList) {
    if (db != null && model.getString(DATABASE_NAME) != db) continue;
    List keys = model.items.keys.toList();
    for (String key in keys) {
      if (key.startsWith(statsKey)) {
        int count = model.get(key);
        if (todayOnly) {
          if (key.contains("${getTodayMilli()}")) {
            total = total + count;
          }
        } else {
          total = total + count;
        }
      }
    }
  }
  return total;
}

Map getStatsMap(List statsList, String statsKey,
    {bool todayOnly = false, String db}) {
  Map data = {};
  for (BaseModel model in statsList) {
    if (db != null && model.getString(DATABASE_NAME) != db) continue;
    List keys = model.items.keys.toList();
    for (String key in keys) {
      if (key.startsWith(statsKey)) {
        int count = model.get(key);
//        print("Key: $key count: $count  Todaymilli:${getTodayMilli()}");
        String timeVal = key.replaceAll(statsKey, "");
        int time = int.parse(timeVal);
        if (todayOnly) {
          if (key.contains("${getTodayMilli()}")) {
            int prevCount = data[time] ?? 0;
            data[time] = prevCount + count;
          }
          continue;
        }
        int prevCount = data[time] ?? 0;
        data[time] = prevCount + count;
      }
    }
  }
  return data;
}

// CachedNetworkImage(
//     {@required String imageUrl,
//     double width,
//     double height,
//     BoxFit fit = BoxFit.cover,
//     color,
//     bool showLoading = false,
//     onImageLoaded}) {
//   if (imageUrl.contains("asset")) {
//     return Image.asset(
//       imageUrl,
//       width: width,
//       height: height,
//       fit: fit,
//       color: color,
//     );
//   }
//
//   if (isLocalFile(imageUrl)) {
//     return Image.file(
//       File(imageUrl),
//       width: width,
//       height: height,
//       fit: fit,
//       color: color,
//     );
//   }
//
//   return cachedLib.CachedNetworkImage(
//     imageUrl: imageUrl,
//     width: width,
//     height: height,
//     fit: fit,
//     color: color,
//   );
// }

infoBox(var boxColor, String title, String message,
    {clickText, onClick, icon, margin}) {
  return Container(
    width: double.infinity,
    margin: margin ?? EdgeInsets.fromLTRB(15, 5, 15, 5),
    padding: EdgeInsets.all(10),
    decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: boxColor, width: 2)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Icon(
              icon ?? Icons.info,
              color: boxColor,
            ),
            addSpaceWidth(8),
            Flexible(
                child: Text(
              title,
              style: textStyle(true, 18, boxColor),
            )),
          ],
        ),
        if (message.isNotEmpty) addSpace(5),
        if (message.isNotEmpty)
          Text(
            message,
            style: textStyle(false, 14, boxColor),
          ),
//        addSpace(10),
        if (clickText != null)
          Container(
            margin: EdgeInsets.only(top: 10),
            height: 30,
            child: RaisedButton(
              onPressed: () {
                onClick();
              },
              child: Text(
                clickText,
                style: textStyle(true, 14, white),
              ),
              color: boxColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
            ),
          )
      ],
    ),
  );
}

//bool checkingInternet = false;
bool noInternet = false;
//int lastChecked = 0;
checkInternet({onChecked}) async {
  if (webApp) return;
  int now = DateTime.now().millisecondsSinceEpoch;
//  if(now-lastChecked
//      <(Duration(minutes: 1).inMilliseconds))return;
//  lastChecked = now;
  print("checking time");
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('connected');
      noInternet = false;
      //mainController.add(true);
    }
  } on SocketException catch (_) {
    print('not connected');
    noInternet = true;
    //mainController.add(true);
  }
}

getPlanColor(int p) {
//  int p = page??currentPage;
  return p == 0
      ? light_green3
      : p == 1
          ? blue0
          : p == 2
              ? dark_green0
              : p == 3
                  ? red3
                  : blue6;
}

int planDaysLeft = -1;
bool checkingPlan = false;
bool planChecked = false;
checkPlanDaysLeft({bool force = false, onComplete}) async {
  if (!isLoggedIn) return;
  if (userModel.items.isEmpty) return;
  if (userModel.get(PLAN_SETTINGS) == null) return;
  if (planChecked && !force) return;
  if (checkingPlan && !force) return;
  checkingPlan = true;
  print("Checking Plan Days");
  DateTime _currentTime = DateTime.now().toLocal();

  var offset = webApp
      ? 0
      : await NTP.getNtpOffset(localTime: _currentTime).catchError((error) {
          checkingPlan = false;
        });
  if (offset == null) {
    return;
  }
//  NTP.getNtpOffset(localTime: _currentTime).then((offset) async {
//    int _ntpOffset = offset;
  int nowMilli =
      _currentTime.add(Duration(milliseconds: offset)).millisecondsSinceEpoch;

  Map planSettings = userModel.getMap(PLAN_SETTINGS);
  int planExpiry = planSettings[PLAN_EXPIRY_TIME];
  int left = (planExpiry - nowMilli);
  if (left < 0) {
    userModel.remove(PLAN_SETTINGS);
    userModel.updateItems();
    if (onComplete != null) onComplete();
  } else {
    planDaysLeft = left ~/ Duration.millisecondsPerDay;
    if (onComplete != null) onComplete();
  }
  planChecked = true;
//  }).catchError((error) {
//    checkingPlan=false;
//  });
}

class StartPointerBox extends CustomPainter {
  double space;
  StartPointerBox(this.space);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true
      ..color = white;

    double height = size.height;
    double width = size.width;

    Path path = Path();
    path.lineTo(0, 0);
    path.lineTo(width, 0);
    path.lineTo(width, 10);
    path.lineTo((space + 10), 10);
    path.lineTo((space + 5), 0);
    path.lineTo(space, 10);
    path.lineTo(0, 10);
    path.lineTo(0, 0);
    path.close();

    canvas.drawPath(path, paint);
//    canvas.drawPath(path2, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class EndPointerBox extends CustomPainter {
  double space;
  EndPointerBox(
    this.space,
  );

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true
      ..color = white;

    double height = size.height;
    double width = size.width;

    Path path = Path();

    path.lineTo(0, 0);
    path.lineTo(width, 0);
    path.lineTo(width, 10);
    path.lineTo(width - space, 10);
    path.lineTo(width - (space + 5), 0);
    path.lineTo(width - (space + 10), 10);
    path.lineTo(0, 10);
    path.lineTo(0, 0);
    path.close();

    canvas.drawPath(path, paint);
//    canvas.drawPath(path2, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

bool isNameValid(String value) {
  return RegExp(r'^[a-zA-Z0-9_\-,\. &]+$').hasMatch(value);
}

bool isUsernameValid(String value) {
  return RegExp(r'^[a-zA-Z0-9]+$').hasMatch(value);
}

String getTimeTitle() {
  int hour = TimeOfDay.now().hour;
  if (hour >= 3 && hour <= 11) {
    return "this morning";
  } else if (hour >= 12 && hour <= 16) {
    return "this afternoon";
  } else if (hour >= 17 && hour <= 20) {
    return "this evening";
  } else if (hour >= 21 && hour <= 24) {
    return "tonight";
  } else {
    return "right now";
  }
}

Color getWhiteColor() {
  return darkMode ? black : white;
}

Color getWhiteColor2() {
  return darkMode ? dark_grey_two : white;
}

Color getWhiteColor3() {
  return darkMode ? dark_grey_two : default_white;
}

Color getBlackColor() {
  return darkMode ? white : black;
}

Color getBlackColor2() {
  return darkMode ? white : dark_grey_two;
}

bool sameGender(String gender) {
  String myGender = userModel.getString(GENDER);
  if (myGender == "Male" && gender == "Guys") return true;
  if (myGender == "Female" && gender == "Ladies") return true;
  return false;
}

int countKiss(BaseModel model) {
  Map item = model.items;
  int count = 0;
  item.forEach((key, value) {
    if (key.toString().startsWith("kiss")) {
      count = count + value;
    }
  });
  return count;
}

int countMyKiss(BaseModel model, {String specialKey = ""}) {
  Map item = model.items;
  int count = 0;
  item.forEach((key, value) {
    if (key.toString().startsWith("kiss") &&
        key.toString().endsWith(userModel.getObjectId())) {
      if (specialKey.isNotEmpty) {
        if (key.toString().contains(specialKey)) {
          count = count + value;
        }
      } else {
        count = count + value;
      }
    }
  });
  return count;
}

String showAllId = "";

createLikeNotify(BaseModel user) {
  String theKey = "match${user.getObjectId()}${userModel.getObjectId()}";
  DocumentReference matchDoc =
      FirebaseFirestore.instance.collection(NOTIFY_BASE).doc(theKey);
  matchDoc.set(
    {
      TIME: DateTime.now().millisecondsSinceEpoch,
      TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
      USER_ID: userModel.getObjectId(),
      USER_IMAGE: userModel.getString(USER_IMAGE),
      FIRST_NAME: userModel.getString(FIRST_NAME),
      LAST_NAME: userModel.getString(LAST_NAME),
      ITEM_ID: user.getObjectId(),
      ITEM_DB: user.getString(DATABASE_NAME),
      KISS_TYPE: KISS_TYPE_POST,
      DATABASE_NAME: NOTIFY_BASE,
      MESSAGE: "is interested in you",
      PARTIES: [user.getString(USER_ID)],
//    PARTIES:[userModel.getString(USER_ID)],
    },
  );
}

createKissNotifyForPost(BaseModel item) {
  String theKey = "kiss${item.getObjectId()}${userModel.getObjectId()}";
  DocumentReference kissDoc =
      FirebaseFirestore.instance.collection(NOTIFY_BASE).doc(theKey);
  kissDoc.update({
    KISS_COUNT: FieldValue.increment(1),
    TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
    READ_BY: [],
  }).catchError((e) {
    if (e.toString().toLowerCase().contains("found")) {
      kissDoc.set({
        TIME: DateTime.now().millisecondsSinceEpoch,
        TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
        USER_ID: userModel.getObjectId(),
        USER_IMAGE: userModel.getString(USER_IMAGE),
        LAST_NAME: userModel.getString(LAST_NAME),
        ITEM_ID: item.getObjectId(),
        ITEM_DB: item.getString(DATABASE_NAME),
        KISS_TYPE: KISS_TYPE_POST,
        DATABASE_NAME: NOTIFY_BASE,
        PARTIES: [item.getString(USER_ID)],
        KISS_COUNT: FieldValue.increment(1),
      });
    }
  });
}

createKissNotifyForPhoto(String userId, String photoId) {
  String theKey = "kiss${photoId}${userModel.getObjectId()}";
  DocumentReference kissDoc =
      FirebaseFirestore.instance.collection(NOTIFY_BASE).doc(theKey);
  kissDoc.update({
    KISS_COUNT: FieldValue.increment(1),
    TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
    READ_BY: [],
  }).catchError((e) {
    if (e.toString().toLowerCase().contains("found")) {
      kissDoc.set({
        TIME: DateTime.now().millisecondsSinceEpoch,
        TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
        USER_ID: userModel.getObjectId(),
        USER_IMAGE: userModel.getString(USER_IMAGE),
        LAST_NAME: userModel.getString(LAST_NAME),
        ITEM_ID: photoId,
//        ITEM_DB:item.getString(DATABASE_NAME),
        KISS_TYPE: KISS_TYPE_PHOTO,
        DATABASE_NAME: NOTIFY_BASE,
        PARTIES: [userId],
        KISS_COUNT: FieldValue.increment(1),
      });
    }
  });
}

createKissNotifyForProfile(
  String userId,
) {
  String theKey = "kiss${userId}${userModel.getObjectId()}";
  DocumentReference kissDoc =
      FirebaseFirestore.instance.collection(NOTIFY_BASE).doc(theKey);
  kissDoc.update({
    KISS_COUNT: FieldValue.increment(1),
    TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
    READ_BY: [],
  }).catchError((e) {
    if (e.toString().toLowerCase().contains("found")) {
      kissDoc.set({
        TIME: DateTime.now().millisecondsSinceEpoch,
        TIME_UPDATED: DateTime.now().millisecondsSinceEpoch,
        USER_ID: userModel.getObjectId(),
        USER_IMAGE: userModel.getString(USER_IMAGE),
        FIRST_NAME: userModel.getString(FIRST_NAME),
        LAST_NAME: userModel.getString(LAST_NAME),
        ITEM_ID: userId,
//        ITEM_DB:USER_BASE,
        KISS_TYPE: KISS_TYPE_PROFILE,
        DATABASE_NAME: NOTIFY_BASE,
        PARTIES: [userId],
        KISS_COUNT: FieldValue.increment(1),
      });
    }
  });
}

getPinkColor() {
  return darkMode ? red0 : red3;
}

getPinkColor2() {
  return darkMode ? red0 : red08;
}

getPinkColor3() {
  return darkMode ? red2 : red05;
}

performBlocking(BaseModel model) {
  List blocked = userModel.getList(BLOCKED);
  String userId = model.getUserId();
  String objectId = model.getObjectId();
  String deviceId = model.getString(DEVICE_ID);
  if (userId.isNotEmpty && !blocked.contains(userId)) blocked.add(userId);
  if (objectId.isNotEmpty && !blocked.contains(objectId)) blocked.add(objectId);
  if (deviceId.isNotEmpty && !blocked.contains(deviceId)) blocked.add(deviceId);
  List following = userModel.getList(FOLLOWING);
  following.remove(userId);
  userModel.put(FOLLOWING, following);
  userModel.put(BLOCKED, blocked);
  userModel.updateItems();

  if (userId.isNotEmpty) blockedIds.add(userId);
  if (objectId.isNotEmpty) blockedIds.add(objectId);
  if (deviceId.isNotEmpty) blockedIds.add(deviceId);
}

performUnblocking(BaseModel model) {
  List blocked = userModel.getList(BLOCKED);
  String userId = model.getUserId();
  String objectId = model.getObjectId();
  String deviceId = model.getString(DEVICE_ID);
  if (userId.isNotEmpty) blocked.remove(userId);
  if (objectId.isNotEmpty) blocked.remove(objectId);
  if (deviceId.isNotEmpty) blocked.remove(deviceId);
  blockedIds.remove(userId);
  blockedIds.remove(objectId);
  blockedIds.remove(deviceId);
  userModel.put(BLOCKED, blocked);
  userModel.updateItems();
}

bool isListEmpty(List list) {
  int count = 0;
  for (BaseModel model in list) {
    if (!isBlocked(model)) {
      count++;
    }
  }
  return count == 0;
}

int getMyKissLimit() {
  Map settings = userModel.getMap(PLAN_SETTINGS);
  int kissCount = settings[KISS_COUNT] ?? 0;
  if (kissCount == 0) {
    kissCount = appSettingsModel.getInt(KISS_COUNT);
    kissCount = kissCount == 0 ? 10 : kissCount;
  }
  return kissCount;
}

Future<bool> hasFace(context, File file) async {
  return true;
  // final image = FirebaseVisionImage.fromFile(file);
  // final faceDetector = FirebaseVision.instance.faceDetector();
  // List<Face> faces = await faceDetector.processImage(image);
  // return faces.isNotEmpty;
}

bool dontShowUser(BaseModel user) {
  if (appSettingsModel.getList(BANNED).contains(user.getString(USER_ID)))
    return true;
  if (appSettingsModel.getList(DISABLED).contains(user.getString(USER_ID)))
    return true;
  if (isBlocked(user)) return true;
  if (user.getBoolean(HIDE_PROFILE)) return true;

  return false;
}

makeFirstUpper(String text) {
  if (text.trim().isEmpty) return text;
  StringBuffer sb = StringBuffer();
  List parts = text.split(" ");
  for (String s in parts) {
    s = s.trim();
    if (s.isEmpty) continue;
    if (s.length == 1) {
      sb.write("$s ");
      continue;
    }
    sb.write(s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase());
    sb.write(" ");
  }
  return sb.toString().trim();
}

handleMobileCredits(String id, String userId, int credits, bool add,
    {onAdded, bool hasFree = false}) async {
  if (userId == userModel.getObjectId()) {
    hmcr(
      userModel,
      id,
      credits,
      add,
      hasFree,
      onAdded: onAdded,
    );
    return;
  }
  FirebaseFirestore.instance
      .collection(USER_BASE)
      .doc(userId)
      .get(GetOptions(source: Source.server))
      .then((shot) {
    BaseModel model = BaseModel(doc: shot);
    hmcr(model, id, credits, add, hasFree);
  }, onError: (e) {
    handleMobileCredits(id, userId, credits, add);
  });
}

hmcr(BaseModel model, String id, int credits, bool add, bool hasFree,
    {onAdded}) {
  List mcrIds = model.getList(MCR_IDS);
  int mcr = model.getInt(MCR);
  int mcrFree = model.getInt(MCR_FREE);
  if (!mcrIds.contains(id)) {
    mcrIds.add(id);
    model.put(MCR_IDS, mcrIds);

    if (add) {
      mcr = mcr + credits;
      if (hasFree) {
        mcrFree = mcrFree + credits;
      }
    } else {
      mcr = mcr - credits;
      mcrFree = mcrFree > mcr ? mcr : mcrFree;
    }

    model.put(MCR, mcr);
    model.put(MCR_FREE, mcrFree);
    model.updateItems();
    if (onAdded != null) onAdded();

    if (add) {
      createNotification(
        receiverIds: [model.getObjectId()],
        message: "have been credited with $credits coins",
        itemId: null,
        type: "coins$id",
      );
      if (!model.getBoolean(DONT_PUSH)) {
        NotificationService.sendPush(
          body: "You have been credited with $credits coins",
          topic: model.getObjectId(),
        );
      }
    }
  }
}

imageItem(icon, double size, color, {bool ignoreWidth = false}) {
  return icon is IconData
      ? Icon(
          icon,
          size: size,
          color: color,
        )
      : Image.asset(
          icon,
          width: ignoreWidth ? null : size,
          height: size,
          color: color,
        );
}

indexItem(int p, {color: white}) {
  return Container(
    margin: EdgeInsets.only(top: 5),
    width: 40,
    height: 40,
    decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
        border: Border.all(color: app_blue.withOpacity(.5), width: 1)),
    child: Center(
      child: Text(
        "${p + 1}",
        style: textStyle(false, 16, black.withOpacity(.4)),
      ),
    ),
  );
}

String getErrorMessage(error) {
//    print("The error $error");
  if (error is Exception) {
    try {
      if (error is DioError) {
        switch (error.type) {
          case DioErrorType.cancel:
            return "Request canceled";
            break;
          case DioErrorType.receiveTimeout:
            return "Connection timeout";
            break;

          case DioErrorType.receiveTimeout:
            return "Receive timeout";
            break;
          case DioErrorType.response:
            switch (error.response.statusCode) {
              case 400:
                return "Request unauthorized";
                break;
              case 401:
                return "Request unauthorized";
                break;
              case 403:
                return "Request unauthorized";
                break;
              case 404:
                return "Not found";
                break;
              case 405:
                return "Method not allowed";
                break;
              case 409:
                return "Network conflict error";
                break;
              case 408:
                return "Request timeout";
                break;
              case 500:
                return "Internal server error";
                break;
              case 503:
                return "Service unavailable";
                break;
              default:
                var responseCode = error.response.statusCode;
                return "Received invalid status code: $responseCode";
            }
            break;
          case DioErrorType.sendTimeout:
            return "Send timeout";
            break;
          case DioErrorType.connectTimeout:
            // TODO: Handle this case.
            break;
          case DioErrorType.other:
            // TODO: Handle this case.
            break;
        }
      } else if (error is SocketException) {
        return "No internet connectivity";
      } else {
        return "Unexpected error code";
      }
    } on FormatException catch (e) {
      // Helper.printError(e.toString());
      return "Format error";
    } catch (_) {
      return "Unexpected error code";
    }
  } else {
    return error.toString(); // "Unexpected error occurred format";
    if (error.toString().contains("is not a subtype of")) {}
    /*else {
      return "Unexpected error occurred on format";
    }*/
  }
}

String getSimpleDate(int milli, {String pattern = "d MMMM, yyyy"}) {
  final formatter = DateFormat(pattern);
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

rowItem(String title, var item,
    {bool isAmount = false,
    bool isDate = false,
    bool hideBottomLine = false,
    bool copy = false,
    context,
    double textSize = 13,
    textColor = blue0}) {
  String text = item == null ? "" : item.toString();
  String dateText = "-";
  try {
    dateText = getSimpleDate(item);
  } catch (e) {}
  return Container(
    margin: EdgeInsets.only(top: 8),
    padding: EdgeInsets.only(bottom: 8),
    decoration: BoxDecoration(
        border: Border(
            bottom: BorderSide(
                color: hideBottomLine ? transparent : black.withOpacity(.1),
                width: .3))),
    child: Row(
      children: [
        Flexible(
          fit: FlexFit.tight,
          child: Container(
            width: 100,
            child: Text(title,
                style: textStyle(
                    false, textSize, getBlackColor().withOpacity(.4))),
          ),
        ),
        addSpaceWidth(5),
        RichText(
          text: TextSpan(children: [
            if (isAmount)
              WidgetSpan(
                child: Container(
                    margin: EdgeInsets.only(bottom: 3, right: 3),
                    child: Image.asset(
                      naira,
                      width: textSize - 4,
                      height: textSize - 4,
                      color: textColor,
                    )),
              ),
            TextSpan(
                text: text == null || text.isEmpty
                    ? "-"
                    : isDate
                        ? dateText
                        : !isAmount
                            ? "$text"
                            : "${formatAmount(text, decimal: 0)}",
                style: textStyle(
                  false,
                  textSize,
                  textColor,
                )),
          ]),
          textAlign: TextAlign.left,
        ),
        if (copy)
          GestureDetector(
            onTap: () {
              Clipboard.setData(ClipboardData(text: text));
              showMessage(context, Icons.copy, blue0, "Copied", "");
            },
            child: Container(
                margin: EdgeInsets.only(left: 5),
                color: transparent,
                child: Icon(
                  Icons.copy,
                  color: black.withOpacity(.5),
                  size: textSize,
                )),
          )
      ],
    ),
  );
}

getMyFullName([BaseModel bm]) {
  BaseModel user = bm ?? userModel;
  return "${user.getString(LAST_NAME)} ${user.getString(FIRST_NAME)} ${user.getString(OTHER_NAME)}";
}

void getApplicationsAPICall(
  context,
  String url,
  onComplete(response, error), {
  @required data,
  bool getMethod = false,
  bool putMethod = false,
  bool deleteMethod = false,
  bool patchMethod = false,
  String progressMessage,
  @required bool silently,
  bool queryInUrl = false,
  bool otpRequest = false,
  bool ignoreResponseStatus = false,
  bool ignoreResponseMessage = false,
}) async {
  silently = silently ?? false;
  if (!silently) showProgress(true, context, msg: progressMessage);

  // url = BASE_API + url;

  if (getMethod && data != null) {
    url = "$url?";
    data.forEach((key, value) {
      url = "$url$key=$value&";
    });
    if (url.endsWith("&")) {
      url = url.substring(0, url.length - 1);
    }
  }
  if ((!(await isConnected()))) {
    if (!silently) showProgress(false, context);
    await Future.delayed(Duration(milliseconds: 600));
    onComplete(null, "No internet connectivity");
    return;
  }
  try {
    Dio dio = Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      // Do something before request is sent
      options.headers['content-Type'] = 'application/json';
      options.headers['Authorization'] = "Bearer $raveSecretKey";
      return options;
    }));
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    String reqUrl = url; //+  (getMethod?"":"?obj=$data");
    print(">>> Requesting $reqUrl  $data<<<");
    var res = putMethod
        ? (await dio.put(
            url,
            data: data,
          ))
        : getMethod
            ? (await dio.get(
                reqUrl, //queryParameters: data
              ))
            : patchMethod
                ? (await dio.patch(
                    url,
                    data: data,
                  ))
                : await dio.post(
                    reqUrl,
                    data: data,
                  );
    if (!silently) showProgress(false, context);
    await Future.delayed(Duration(milliseconds: 600));
    String error = getResponseErrorMessage(
      res,
      ignoreResponseStatus: ignoreResponseStatus,
      ignoreResponseMessage: ignoreResponseMessage,
    );
    var response = getResponse(res);
    print("errorxx: $error, response>>: ${response} <<");
    onComplete(response, error);
  } catch (e) {
    if (!silently) showProgress(false, context);
    await Future.delayed(Duration(milliseconds: 600));
    onComplete(null, getErrorMessage(e));
  }
}

getResponse(res) {
//  print("Yeeep! $res");
  print("getResponse>>> $res");
  if (res == null) return null;
  if (res is String) return res.toString();
  if (res.data is List) return List.from(res.data);
  if (res.data is String) return res.data;
  if (res.data is Map) return Map.from(res.data);
  return res;
}

String getResponseErrorMessage(res,
    {bool ignoreResponseMessage = false, bool ignoreResponseStatus = false}) {
  if (res == null) return "No response from server";
  if (res is String) return null;
  if (res.data is String) return null;
  if (res.data == null) return "Unexpected error occurred";
  if (!(res.data is Map)) return null;
  Map data = Map.from(res.data);
  if (data == null) return null;
  String status = data["status"];
  if (status == "success") return null;
  String message = data["message"];
  if (message == null && ignoreResponseMessage) return null;
  if (message == null) return "No error description";
  return message;
}

showSuccessDialog(context, String message,
    {onOkClicked, bool cancellable = true}) {
  showMessage(context, Icons.check, blue0, "Successful", message,
      delayInMilli: 500, cancellable: cancellable, onClicked: (_) {
    if (_ == true) {
      if (onOkClicked != null) onOkClicked();
    }
  });
}

// openTheFile(context, String path) async {
//   OpenResult open = await OpenFile.open(path);
//   if (open.type == ResultType.error) {
//     showErrorDialog(context, "Error opening file");
//   } else if (open.type == ResultType.fileNotFound) {
//     showErrorDialog(context, "File not found, please restart your App");
//   } else if (open.type == ResultType.noAppToOpen) {
//     showErrorDialog(context, "No pdf reader found on your device");
//   } else if (open.type == ResultType.permissionDenied) {
//     showErrorDialog(context, "You need read permission to open this file");
//   }
// }

String getPaybackDays(int days) {
  if ((days % 7) == 0) {
    int weeks = days ~/ 7;
    return "$weeks week${weeks > 1 ? "s" : ""}";
  }

  return "$days day${days > 1 ? "s" : ""}";
}

showError(context, String err) {
  EdgeAlert.show(context,
      description: err,
      gravity: EdgeAlert.TOP,
      backgroundColor: red0,
      icon: Icons.info_outline);
}
