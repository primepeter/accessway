import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/auth/LoginPage.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final passController2 = TextEditingController();

  bool passVisible = false;
  bool passVisible2 = false;
  bool rePassVisible = false;
  bool termsAccepted = false;

  List<BaseModel> allSongWriters = [];
  bool songWriterBusy = true;

  int currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xfffaffff),
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(20, 80, 0, 0),
                child: Image.asset(
                  "assets/tabs/access_img.png",
                  height: 70,
                  width: 70,
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  // padding: EdgeInsets.zero,
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 100),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Let's get you started!.",
                        style: TextStyle(
                            fontSize: 30,
                            color: black,
                            fontWeight: FontWeight.bold),
                      ),
                      addSpace(10),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: TextButton(
                          onPressed: () {
                            pushAndResult(context, LoginPage(),
                                fade: true, replace: true);
                          },
                          child: Text.rich(
                            TextSpan(children: [
                              TextSpan(text: "Already have an account? "),
                              TextSpan(
                                text: "Login Now",
                                style: TextStyle(
                                    color: appBlue,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15),
                              )
                            ]),
                            style: TextStyle(
                                backgroundColor: Color(0xfffaffff),
                                color: black.withOpacity(.5),
                                fontWeight: FontWeight.normal,
                                fontSize: 15),
                          ),
                          style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                      ),
                      addSpace(10),
                      inputField("First Name", firstNameController,
                          autofillHints: [AutofillHints.name], onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      inputField("Last Name", lastNameController,
                          autofillHints: [AutofillHints.familyName],
                          onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      inputField("Email Address", emailController,
                          autofillHints: [AutofillHints.email], onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      inputField("Password", passController,
                          autofillHints: [AutofillHints.password],
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }, onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      inputField("Confirm Password", passController2,
                          autofillHints: [AutofillHints.password],
                          isPass: true,
                          passwordVisible: passVisible2, onVisiChanged: () {
                        passVisible2 = !passVisible2;
                        setState(() {});
                      }, onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      Container(
                        height: 60,
                        child: TextButton(
                          onPressed: () {
                            String first = firstNameController.text;
                            String last = lastNameController.text;
                            String email = emailController.text;
                            String pass = passController.text;
                            String pass2 = passController2.text;

                            if (first.isEmpty) {
                              showError(context, "Enter First Name!");
                              return;
                            }
                            if (last.isEmpty) {
                              showError(context, "Enter Last Name!");
                              return;
                            }
                            if (email.isEmpty) {
                              showError(context, "Enter Email address!");
                              return;
                            }

                            if (!isEmailValid(email)) {
                              showError(context, "Enter Valid Email address!");
                              return;
                            }

                            if (pass.isEmpty) {
                              showError(context, "Enter Password!");
                              return;
                            }

                            if (pass2.isEmpty) {
                              showError(context, "Enter Confirm Password!");
                              return;
                            }

                            if (pass != pass2) {
                              showError(context, "Passwords don't match!");
                              return;
                            }

                            showProgress(true, context,
                                msg: "Creating Account");

                            FirebaseAuth.instance
                                .createUserWithEmailAndPassword(
                                    email: email, password: pass)
                                .then((value) {
                              userModel
                                ..put(EMAIL, email)
                                ..put(PASSWORD, pass)
                                ..put(FIRST_NAME, first)
                                ..put(LAST_NAME, last)
                                ..saveItem(USER_BASE, false);

                              pushAndResult(context, HomePage(),
                                  fade: true, replace: true);
                            }).catchError((e) {
                              showProgress(false, context);
                              showErrorDialog(context, e.toString(),
                                  delay: 900);
                            });
                          },
                          child: Center(
                            child: Text(
                              "Create Account",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                          ),
                          style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                          ),
                        ),
                      ),
                      addSpace(20),
                      Center(
                        child: Text(
                          "By creating an account you agree to our Terms & Condition",
                          style: TextStyle(
                              fontSize: 12,
                              color: black.withOpacity(.4),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
