import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SignUpPage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passController = TextEditingController();

  bool passVisible = false;
  bool rePassVisible = false;
  bool termsAccepted = false;

  List<BaseModel> allSongWriters = [];
  bool songWriterBusy = true;

  int currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xfffaffff),
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(20, 80, 0, 0),
                child: Image.asset(
                  "assets/tabs/access_img.png",
                  height: 70,
                  width: 70,
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  // padding: EdgeInsets.zero,
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 100),

                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Hey,\nLogin Now.",
                        style: TextStyle(
                            fontSize: 30,
                            color: black,
                            fontWeight: FontWeight.bold),
                      ),
                      addSpace(10),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: TextButton(
                          onPressed: () {
                            pushAndResult(context, SignUpPage(),
                                fade: true, replace: true);
                          },
                          child: Text.rich(
                            TextSpan(children: [
                              TextSpan(text: "Don't have an account? "),
                              TextSpan(
                                text: "Create Now",
                                style: TextStyle(
                                    color: appBlue,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15),
                              )
                            ]),
                            style: TextStyle(
                                backgroundColor: Color(0xfffaffff),
                                color: black.withOpacity(.5),
                                fontWeight: FontWeight.normal,
                                fontSize: 15),
                          ),
                          style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                      ),
                      addSpace(10),
                      inputField("Email Address", emailController,
                          autofillHints: [AutofillHints.email], onChanged: (s) {
                        setState(() {});
                      }),
                      addSpace(10),
                      inputField("Password", passController,
                          autofillHints: [AutofillHints.password],
                          isPass: true,
                          passwordVisible: passVisible, onVisiChanged: () {
                        passVisible = !passVisible;
                        setState(() {});
                      }, onChanged: (s) {
                        setState(() {});
                      }),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushReplacementNamed(
                                context, "/forgetPass");
                          },
                          child: Text(
                            "Forgot password?",
                            style: TextStyle(
                                color: appBlue,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                          style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                      ),
                      addSpace(10),
                      Container(
                        height: 60,
                        child: TextButton(
                          onPressed: () {
                            String email = emailController.text;
                            String pass = passController.text;

                            if (email.isEmpty) {
                              showError(context, "Enter Email address!");
                              return;
                            }
                            if (pass.isEmpty) {
                              showError(context, "Enter Password!");
                              return;
                            }

                            showProgress(true, context, msg: "Logging In");

                            FirebaseAuth.instance
                                .signInWithEmailAndPassword(
                                    email: email, password: pass)
                                .then((value) {
                              FirebaseFirestore.instance
                                  .collection(USER_BASE)
                                  .doc(value.user.uid)
                                  .get()
                                  .then((value) async {
                                if (!value.exists) {
                                  showProgress(false, context);
                                  showErrorDialog(
                                      context, "No user with record was found.",
                                      delay: 900);
                                  return;
                                }

                                userModel = BaseModel(doc: value);

                                final pref =
                                    await SharedPreferences.getInstance();
                                pref.setString("user", userModel.getObjectId());

                                // Navigator.pushReplacementNamed(
                                //     context, "/home");

                                pushAndResult(context, HomePage(),
                                    fade: true, replace: true);
                              }).catchError((e) {
                                showProgress(false, context);
                                showErrorDialog(context, e.toString(),
                                    delay: 900);
                              });
                            }).catchError((e) {
                              showProgress(false, context);
                              showErrorDialog(context, e.toString(),
                                  delay: 900);
                            });
                          },
                          child: Center(
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                          ),
                          style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                          ),
                        ),
                      ),
                      //
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
