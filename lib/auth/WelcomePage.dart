import 'package:Loopin/HomePage.dart';
import 'package:Loopin/auth/LoginPage.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';

class PageTab {
  String image;
  String title;
  String subTitle;

  PageTab(
      {@required this.image, @required this.title, @required this.subTitle});
}

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final pageController = PageController();
  int currentPage = 0;

  List<PageTab> get pageItems => [
        PageTab(
            image: "bg.png",
            title: "Get Started",
            subTitle: "Explore the AccessVerse and win prizes"),
        PageTab(
            image: "bg.png",
            title: "Get Started",
            subTitle: "Explore the AccessVerse and win prizes"),
        PageTab(
            image: "bg.png",
            title: "Get Started",
            subTitle: "Explore the AccessVerse and win prizes"),
        PageTab(
            image: "bg.png",
            title: "Get Started",
            subTitle: "Explore the AccessVerse and win prizes"),
      ];

  PageTab get currentItem {
    return pageItems[currentPage];
  }

  bool get isLastPage {
    return pageItems.length - 1 == currentPage;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      resizeToAvoidBottomInset: false,
      body: Container(
        // padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  Container(
                    child: Builder(
                      builder: (c) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: getScreenHeight(context) * 0.68,
                              child: Stack(
                                children: [
                                  Align(
                                      alignment: Alignment.topCenter,
                                      child:
                                          Image.asset("assets/icons/rec1.png")),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      // height: 350,
                                      // width: 350,
                                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                      alignment: Alignment.center,
                                      child: Image.asset(
                                        "assets/icons/${currentItem.image}",
                                        scale: 1.5,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.bottomCenter,
                                      child:
                                          Image.asset("assets/icons/rec0.png")),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    currentItem.title,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  addSpace(5),
                                  Text(
                                    currentItem.subTitle,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  PageView(
                    controller: pageController,
                    onPageChanged: (p) => setState(() {
                      currentPage = p;
                    }),
                    children: List.generate(
                        pageItems.length,
                        (p) => Container(
                              color: transparent,
                            )),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
              padding: EdgeInsets.all(10),
              color: white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: List.generate(pageItems.length, (p) {
                      bool active = currentPage == p;
                      return Container(
                        height: 12,
                        width: active ? 18 : 12,
                        margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
                        decoration: BoxDecoration(
                            color:
                                active ? btnGreenColor : black.withOpacity(.09),
                            border: Border.all(
                              color: black.withOpacity(.09),
                            ),
                            borderRadius: BorderRadius.circular(10)),
                      );
                    }),
                  ),
                  addSpace(5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 45,
                        width: 150,
                        alignment: Alignment.centerLeft,
                        child: TextButton(
                          onPressed: () {
                            pushAndResult(
                              context,
                              LoginPage(),
                              fade: true,
                            );
                          },
                          child: Text(
                            "Skip",
                            style: TextStyle(fontSize: 18),
                          ),
                          style: TextButton.styleFrom(
                            primary: black,
                            backgroundColor: Colors.white,
                            // shape: RoundedRectangleBorder(
                            //     borderRadius: BorderRadius.circular(30))
                          ),
                        ),
                      ),
                      Container(
                        height: 45,
                        width: 150,
                        child: TextButton(
                          onPressed: () {
                            if (isLastPage) {
                              pushAndResult(
                                context,
                                LoginPage(),
                                fade: true,
                              );
                              return;
                            }
                            pageController.nextPage(
                                duration: Duration(milliseconds: 200),
                                curve: Curves.ease);
                          },
                          child: Center(
                              child: Text(
                            isLastPage ? "Continue" : "Next",
                            style: TextStyle(fontSize: 16),
                          )),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: appColor1,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
